/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Web.Http;
using Swashbuckle.Swagger.Annotations;
using DerethForever.Api.Common;
using DerethForever.Api.Models;
using DerethForever.Entity;
using DerethForever.Database;
using DerethForever.Entity.Enum;

namespace DerethForever.Api.Controllers
{
    /// <summary>
    /// allows for authenticating directly to the server instead of the login server.  warning, this is
    /// subject to deprecation without warning.
    /// </summary>
    public class AccountController : BaseController
    {
        /// <summary>
        /// Used to get an authorization token for use with the API and/or launching of the client when connecting to a gameserver
        /// </summary>
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, "Auth successful", typeof(AuthResponse))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "Incorrect username or password")]
        public HttpResponseMessage Authenticate([FromBody] AuthRequest request)
        {
            var account = CheckUser(request.Username, request.Password);
            if (account != null)
            {
                var subscriptions = DatabaseManager.Authentication.GetSubscriptionsByAccount(account.AccountGuid);
                return Request.CreateResponse(HttpStatusCode.OK, new AuthResponse() { AuthToken = JwtManager.GenerateToken(account, (subscriptions.Count > 0) ? subscriptions[0].AccessLevel : Entity.Enum.AccessLevel.Player, JwtManager.HmacSigning) });
            }
            return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }

        private Account CheckUser(string username, string password)
        {
            var account = DatabaseManager.Authentication.GetAccountByName(username);
            if (account?.Name.Length > 0)
            {
                if (!account.PasswordMatches(password))
                    account = null;
            }
            return account;
        }

        /// <summary>
        /// Registers a new account and subscription with Player permissions
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        [SwaggerResponse(HttpStatusCode.OK, "Auth successful", typeof(RegisterResponse))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "There was an error in the request", typeof(SimpleMessage))]
        public HttpResponseMessage Register(RegisterRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Username))
                return Request.CreateResponse(HttpStatusCode.BadRequest, new SimpleMessage("username is required."));

            if (string.IsNullOrWhiteSpace(request.Password))
                return Request.CreateResponse(HttpStatusCode.BadRequest, new SimpleMessage("password is required."));

            // create an account
            Account newAccount = new Account();
            newAccount.Name = request.Username;
            newAccount.DisplayName = request.DisplayName ?? request.Username;
            newAccount.SetPassword(request.Password);
            newAccount.Email = request.EmailAddress;
            DatabaseManager.Authentication.CreateAccount(newAccount);

            // create a default subscription
            Subscription s = new Subscription();
            s.AccessLevel = AccessLevel.Player;
            s.AccountGuid = newAccount.AccountGuid;
            s.Name = "default";
            DatabaseManager.Authentication.CreateSubscription(s);

            var response = new RegisterResponse()
            {
                AccountGuid = newAccount.AccountGuid,
                AccountId = newAccount.AccountId,
                Username = newAccount.Name
            };

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        /// <summary>
        /// gets all the accounts in the database.  Admin AccessLevel required.
        /// </summary>
        [HttpGet]
        [ApiAuthorize(AccessLevel.Admin)]
        [SwaggerResponse(HttpStatusCode.OK, "account list successful.", typeof(List<Account>))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "you are not authorized for this action.")]
        public HttpResponseMessage GetAll()
        {
            var all = DatabaseManager.Authentication.GetAll();
            return Request.CreateResponse(HttpStatusCode.OK, all);
        }

        /// <summary>
        /// gets the requested account.  Admin AccessLevel required.
        /// </summary>
        [HttpGet]
        [ApiAuthorize(AccessLevel.Admin)]
        [SwaggerResponse(HttpStatusCode.OK, "account get successful.", typeof(Account))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "you are not authorized for this action.")]
        [SwaggerResponse(HttpStatusCode.NotFound, "specified account not found")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "request was invalid", typeof(SimpleMessage))]
        public HttpResponseMessage Get(string accountGuid)
        {
            Guid actualGuid;
            if (!Guid.TryParse(accountGuid, out actualGuid))
                return Request.CreateResponse(HttpStatusCode.BadRequest, new SimpleMessage("Invalid account guid."));

            var account = DatabaseManager.Authentication.GetAccountByGuid(actualGuid);
            if (account == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);

            return Request.CreateResponse(HttpStatusCode.OK, account);
        }

        /// <summary>
        /// gets the requested account.  Admin AccessLevel required.
        /// </summary>
        [HttpGet]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, "account get successful.", typeof(Account))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "you are not authorized for this action.")]
        [SwaggerResponse(HttpStatusCode.NotFound, "specified account not found")]
        public HttpResponseMessage GetMyAccount()
        {
            var p = (GenericPrincipal)RequestContext.Principal;

            Guid actualGuid;
            if (!Guid.TryParse(p.Identity.Name, out actualGuid))
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { message = "Invalid account guid." });

            var account = DatabaseManager.Authentication.GetAccountByGuid(actualGuid);
            if (account == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);

            return Request.CreateResponse(HttpStatusCode.OK, account);
        }

        /// <summary>
        /// adds a managed world to the account used for authentication
        /// </summary>
        [HttpPost]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, "managed world added.", typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "you are not authorized for this action.")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "invalid Managed World data", typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "unexpected error", typeof(SimpleMessage))]
        public HttpResponseMessage AddManagedWorld(ManagedWorld world)
        {
            var p = (GenericPrincipal)RequestContext.Principal;

            if (world == null || string.IsNullOrWhiteSpace(world.ServerName) || string.IsNullOrWhiteSpace(world.Address))
                return Request.CreateResponse(HttpStatusCode.BadRequest, new SimpleMessage("Invalid Managed World Data"));

            if (!Guid.TryParse(p.Identity.Name, out var actualGuid))
                return Request.CreateResponse(HttpStatusCode.BadRequest, new SimpleMessage("Invalid account guid."));

            world.AccountGuid = actualGuid;
            world.WorldGuid = Guid.NewGuid();

            try
            {
                if (DatabaseManager.Authentication.CreateManagedWorld(world))
                    return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                log.Error("Error during AccountController.AddMangedWorld", ex);
                if (p.IsInRole("Admin"))
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new SimpleMessage("There was an unexpected error processing your request:\r\n\r\n" + ex.ToString()));
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, new SimpleMessage("There was an unexpected error processing your request."));
        }

        /// <summary>
        /// updates the specified account.  the account must match the ticket being used for authentication.
        /// </summary>
        [HttpPost]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, "account updated.", typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "you are not authorized for this action.", typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "invalid request", typeof(SimpleMessage))]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "unexpected error", typeof(SimpleMessage))]
        public HttpResponseMessage UpdateAccount(Account account)
        {
            var p = (GenericPrincipal)RequestContext.Principal;

            if (account == null || string.IsNullOrWhiteSpace(account.DisplayName) || string.IsNullOrWhiteSpace(account.Name))
                return Request.CreateResponse(HttpStatusCode.BadRequest, new SimpleMessage("Invalid account"));

            if (account.ManagedWorlds != null)
            {
                account.ManagedWorlds.RemoveAll(w => w == null);

                foreach (var world in account.ManagedWorlds)
                    if (world != null && (string.IsNullOrWhiteSpace(world.Address) || string.IsNullOrWhiteSpace(world.ServerName)))
                        return Request.CreateResponse(HttpStatusCode.BadRequest, new SimpleMessage("invalid managed server data."));
            }

            if (!Guid.TryParse(p.Identity.Name, out var actualGuid))
                return Request.CreateResponse(HttpStatusCode.BadRequest, new SimpleMessage("Invalid account guid."));

            if (actualGuid != account.AccountGuid)
                return Request.CreateResponse(HttpStatusCode.Unauthorized, new SimpleMessage("you may only update your own account."));

            try
            {
                DatabaseManager.Authentication.UpdateAccount(account);
                return Request.CreateResponse(HttpStatusCode.OK, new SimpleMessage("account updated"));
            }
            catch (Exception ex)
            {
                log.Error("Error during AccountController.UpdateAccount", ex);
                if (p.IsInRole("Admin"))
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new SimpleMessage("There was an unexpected error processing your request:\r\n\r\n" + ex.ToString()));
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, new SimpleMessage("There was an unexpected error processing your request."));
        }

        /// <summary>
        /// Validates the bearer token and returns the associated account.  this method is used
        /// by servers in distributed auth mode.
        /// </summary>
        [HttpGet]
        [ApiAuthorize]
        [SwaggerResponse(HttpStatusCode.OK, "ticket is valid")]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "ticket is invalid")]
        public HttpResponseMessage Validate()
        {
            // ApiAuthorize bit handles the 401s.  if we made it here it must be ok.
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
