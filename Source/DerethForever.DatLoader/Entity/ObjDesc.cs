/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;

namespace DerethForever.DatLoader.Entity
{
    public class ObjDesc
    {
        public List<SubPalette> SubPalettes { get; set; } = new List<SubPalette>();
        public List<TextureMapChange> TextureChanges { get; set; } = new List<TextureMapChange>();
        public List<AnimationPartChange> AnimPartChanges { get; set; } = new List<AnimationPartChange>();

        public static ObjDesc ReadFromDat(ref DatReader datReader)
        {
            ObjDesc od = new ObjDesc();
            datReader.AlignBoundary(); // Align to the DWORD boundary before and after the ObjDesc
            datReader.ReadByte(); // ObjDesc always starts with 11.
            int numPalettes = datReader.ReadByte();
            int numTextureMapChanges = datReader.ReadByte();
            int numAnimPartChanges = datReader.ReadByte();
            for (int k = 0; k < numPalettes; k++)
            {
                // TODO - This isn't actually used anywhere in the CharGen system, so let's find a test care to make sure this is accurate!
                SubPalette subpalette = new SubPalette();
                subpalette.SubID = datReader.ReadUInt16();
                subpalette.SubID = datReader.ReadUInt16();
                subpalette.NumColors = Convert.ToUInt16(datReader.ReadByte());
                od.SubPalettes.Add(subpalette);
            }
            for (int k = 0; k < numTextureMapChanges; k++)
            {
                TextureMapChange texturechange = new TextureMapChange();
                texturechange.PartIndex = datReader.ReadByte();
                texturechange.OldTexture = datReader.ReadUInt16();
                texturechange.NewTexture = datReader.ReadUInt16();
                od.TextureChanges.Add(texturechange);
            }
            for (int k = 0; k < numAnimPartChanges; k++)
            {
                AnimationPartChange apchange = new AnimationPartChange();
                apchange.PartIndex = datReader.ReadByte();

                // behemoth: this is a packed dword.  if the MSB is set, read 2 more bytes.
                apchange.PartID = datReader.ReadUInt16();
                if (apchange.PartID == 0x8000) // To be honest, I'm not quite sure WHAT this is/means, but the math works out
                    apchange.PartID = datReader.ReadUInt16();
                apchange.PartID += 0x01000000u; // convert to full uint value
                od.AnimPartChanges.Add(apchange);
            }
            datReader.AlignBoundary(); // Align to the DWORD boundary before and after the ObjDesc

            return od;
        }
    }
}
