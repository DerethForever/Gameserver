using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using DerethForever.DatLoader.Entity;

namespace DerethForever.DatLoader.Entity
{
    public static class Vector3Extentions
    {
        public static void Normalize(this Vector3 vector)
        {
            float mag = 1 / vector.Magnitude();
            vector.X *= mag;
            vector.Y *= mag;
            vector.Z *= mag;
        }

        public static bool IsInsignificant(this Vector3 normal)
        {
            float mag = normal.Magnitude();
            return (mag < Loc.TOLERANCE);
        }

        public static float Magnitude(this Vector3 vector)
        {
            return (float)Math.Sqrt(vector.X * vector.X + vector.Y * vector.Y + vector.Z * vector.Z);
        }

        public static float GetHeading(this Vector3 vector)
        {
            Vector3 heading = new Vector3(vector.X, vector.Y, 0);

            if (heading.IsInsignificant())
                return 0f;

            heading.Normalize();

            double radians = ((450d - Math.Atan2(heading.X, heading.Y) * Loc.DEGREES_PER_RADIAN) % 360d) / Loc.DEGREES_PER_RADIAN;
            return (float)radians;
        }

        public static Vector3 CreateVelocity(this Vector3 vector, float timeToUse)
        {
            if (timeToUse < Loc.TOLERANCE)
                throw new ArgumentException("can not scale a vector by 0.");

            Vector3 result;

            result.X = vector.X / timeToUse;
            result.Y = vector.Y / timeToUse;
            result.Z = vector.Z / timeToUse;

            return result;
        }
    }
}
