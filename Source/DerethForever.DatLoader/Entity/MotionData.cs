﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

using DerethForever.Entity;

namespace DerethForever.DatLoader.Entity
{
    public class MotionData
    {
        public byte Bitfield { get; set; }
        public byte Bitfield2 { get; set; }
        public List<AnimData> Anims { get; set; } = new List<AnimData>();
        public DFVector3 Velocity { get; set; }
        public DFVector3 Omega { get; set; }

        public static MotionData Read(DatReader datReader)
        {
            MotionData md = new Entity.MotionData();

            byte numAnims = datReader.ReadByte();
            md.Bitfield = datReader.ReadByte();
            md.Bitfield2 = datReader.ReadByte();
            datReader.AlignBoundary();

            for (byte i = 0; i < numAnims; i++) {
                AnimData animData = new AnimData();
                animData.AnimId = datReader.ReadUInt32();
                animData.LowFrame = datReader.ReadUInt32();
                animData.HighFrame = datReader.ReadUInt32();
                animData.Framerate = datReader.ReadSingle();
                md.Anims.Add(animData);
            }

            if ((md.Bitfield2 & 1) > 0)
                md.Velocity = new DFVector3(datReader.ReadSingle(), datReader.ReadSingle(), datReader.ReadSingle());

            if ((md.Bitfield2 & 2) > 0)
                md.Omega = new DFVector3(datReader.ReadSingle(), datReader.ReadSingle(), datReader.ReadSingle());

            return md;
        }
    }
}
