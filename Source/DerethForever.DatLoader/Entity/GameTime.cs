﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;

namespace DerethForever.DatLoader.Entity
{
    public class GameTime
    {
        public UInt64 ZeroTimeOfYear { get; set; }
        public uint ZeroYear { get; set; } // Year "0" is really "P.Y. 10" in the calendar.
        public uint DayLength { get; set; }
        public uint DaysPerYear { get; set; } // 360. Likely for easier math so each month is same length
        public string YearSpec { get; set; } // "P.Y."
        public List<TimeOfDay> TimesOfDay { get; set; } = new List<TimeOfDay>();
        public List<string> DaysOfTheWeek { get; set; } = new List<string>();
        public List<Season> Seasons { get; set; } = new List<Season>();

        public static GameTime Read(DatReader datReader)
        {
            GameTime obj = new GameTime();
            obj.ZeroTimeOfYear = datReader.ReadUInt64();
            obj.ZeroYear = datReader.ReadUInt32();
            obj.DayLength = datReader.ReadUInt32();
            obj.DaysPerYear = datReader.ReadUInt32();
            obj.YearSpec = datReader.ReadPString();
            datReader.AlignBoundary();

            uint numTimesOfDay = datReader.ReadUInt32();
            for (uint i = 0; i < numTimesOfDay; i++)
            {
                obj.TimesOfDay.Add(TimeOfDay.Read(datReader));
            }

            uint numDaysOfTheWeek = datReader.ReadUInt32();
            for (uint i = 0; i < numDaysOfTheWeek; i++)
            {
                obj.DaysOfTheWeek.Add(datReader.ReadPString());
                datReader.AlignBoundary();
            }

            uint numSeasons = datReader.ReadUInt32();
            for (uint i = 0; i < numSeasons; i++)
            {
                obj.Seasons.Add(Season.Read(datReader));
            }

            return obj;
        }
    }
}
