/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;

using DerethForever.Entity.Enum;

namespace DerethForever.DatLoader
{
    public class PortalDatDatabase : DatDatabase
    {
        private static volatile PortalDatDatabase _portalDatDatabase;

        private static object _portalDatDatabaseLock = new object();

        public PortalDatDatabase(string filename) : base(filename, DatDatabaseType.Portal)
        {
            if (_portalDatDatabase == null)
            {
                lock (_portalDatDatabaseLock)
                {
                    if (_portalDatDatabase == null)
                    {
                        ReadDat();
                        _portalDatDatabase = this;
                    }
                }
            }
            else
            {
                AllFiles = _portalDatDatabase.AllFiles;
                RootDirectory = _portalDatDatabase.RootDirectory;
                SectorSize = _portalDatDatabase.SectorSize;
                FilePath = filename;
            }
        }

        public void ExtractCategorizedContents(string path)
        {
            foreach (KeyValuePair<uint, DatFile> entry in AllFiles)
            {
                string thisFolder = Path.Combine(path, entry.Value.GetFileType() != null ? entry.Value.GetFileType().ToString() : "UnknownType");

                if (!Directory.Exists(thisFolder))
                {
                    Directory.CreateDirectory(thisFolder);
                }

                string hex = entry.Value.ObjectId.ToString("X8");
                string thisFile = Path.Combine(thisFolder, hex + ".bin");

                // Use the DatReader to get the file data
                DatReader dr = GetReaderForFile(entry.Value.ObjectId);

                File.WriteAllBytes(thisFile, dr.Buffer);
            }
        }

        /// <summary>
        /// Reads a byte array and returns a base64 string used for the API or Web projects.
        /// </summary>
        public string ConvertPortalDatIconPngToBase64(byte[] imageData, byte[] backgroundData)
        {
            List<Bitmap> images = new List<Bitmap>();
            Bitmap iconImage = new Bitmap(32, 32, PixelFormat.Format32bppArgb);

            Bitmap backgroundBitmapFile = null;
            if (backgroundData != null)
            {
                using (var ms = new MemoryStream(backgroundData))
                {
                    BinaryReader reader = new BinaryReader(ms);
                    var resourceId = reader.ReadInt32();
                    var unknown1 = reader.ReadInt32();
                    var tex_w = reader.ReadInt32();
                    var tex_h = reader.ReadInt32();
                    var ac_pixfmt = reader.ReadInt32();
                    var cbDataSize = reader.ReadInt32();
                    byte[] pixelData = reader.ReadBytes(cbDataSize);
                    ResetMemoryStream(ms);
                    backgroundBitmapFile = BitmapFromRawPixelData(pixelData, tex_w, tex_h);
                    backgroundBitmapFile.Save(ms, ImageFormat.Png);
                }
            }

            Bitmap iconBitmapFile = null;
            using (var ms = new MemoryStream(imageData))
            {
                BinaryReader reader = new BinaryReader(ms);
                var resourceId = reader.ReadInt32();
                var unknown1 = reader.ReadInt32();
                var tex_w = reader.ReadInt32();
                var tex_h = reader.ReadInt32();
                var ac_pixfmt = reader.ReadInt32();
                var cbDataSize = reader.ReadInt32();
                byte[] pixelData = reader.ReadBytes(cbDataSize);
                ResetMemoryStream(ms);
                iconBitmapFile = BitmapFromRawPixelData(pixelData, tex_w, tex_h);
                iconBitmapFile.Save(ms, ImageFormat.Png);
            }

            using (Graphics g = Graphics.FromImage(iconImage))
            {
                // set background transparent
                g.Clear(Color.Transparent);
                // Allow composting ontop?
                g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;

                if (backgroundBitmapFile != null)
                {
                    // add the background
                    g.DrawImage(backgroundBitmapFile, 0, 0);
                    backgroundBitmapFile.Dispose();
                }

                // add the icon ontop? of the background
                g.DrawImage(iconBitmapFile, 0, 0);
                iconBitmapFile.Dispose();
            }

            return ConvertImageToBase64String(iconImage);
        }

        /// <summary>
        /// Converts an image to a string.
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static string ConvertImageToBase64String(Image image)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, ImageFormat.Png);
                return Convert.ToBase64String(ms.ToArray());
            }
        }

        /// <summary>
        /// Converted from unknown/raw pixel data.
        /// </summary>
        private static Bitmap BitmapFromRawPixelData(byte[] rawPixelData, int width, int heigth)
        {
            var returnImage = new Bitmap(width, heigth, PixelFormat.Format32bppArgb);
            returnImage.MakeTransparent();
            var scene = new Rectangle(0, 0, width, heigth);
            var imageData = returnImage.LockBits(scene, ImageLockMode.ReadWrite, returnImage.PixelFormat);

            var len = width * Image.GetPixelFormatSize(returnImage.PixelFormat) / 8;
            var ptr = imageData.Scan0;
            for (var i = 0; i < heigth; i++)
            {
                Marshal.Copy(rawPixelData, i * len, ptr, len);
                ptr += imageData.Stride;
            }
            returnImage.UnlockBits(imageData);

            Color back = returnImage.GetPixel(1, 1);

            returnImage.MakeTransparent(back);

            return returnImage;
        }

        private void ResetMemoryStream(MemoryStream stream)
        {
            byte[] buffer = stream.ToArray();
            Array.Clear(buffer, 0, buffer.Length);
            stream.Position = 0;
            stream.SetLength(0);
        }
    }
}
