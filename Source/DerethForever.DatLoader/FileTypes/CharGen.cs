/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Numerics;
using DerethForever.DatLoader.Entity;

namespace DerethForever.DatLoader.FileTypes
{
    public class CharGen
    {
        public int Did { get; set; }
        public List<List<Loc>> StarterAreas { get; set; } = new List<List<Loc>>();
        public Dictionary<int, HeritageGroupCG> HeritageGroups { get; set; } = new Dictionary<int, HeritageGroupCG>();

        public static CharGen ReadFromDat()
        {
            // Check the FileCache so we don't need to hit the FileSystem repeatedly
            if (DatManager.PortalDat.FileCache.ContainsKey(0x0E000002))
            {
                return (CharGen)DatManager.PortalDat.FileCache[0x0E000002];
            }
            else
            {
                // Create the datReader for the proper file
                DatReader datReader = DatManager.PortalDat.GetReaderForFile(0x0E000002);
                CharGen cg = new CharGen();

                cg.Did = datReader.ReadInt32();
                datReader.Offset = 8;

                /// STARTER AREAS. There are 5 dungeons per starting city, and one landscape span for Olthoi.
                int numStarterAreas = datReader.ReadByte();
                for (int i = 0; i < numStarterAreas; i++)
                {
                    string starterAreaName = datReader.ReadPStringBase();

                    int numAreas = datReader.ReadByte();
                    List<Loc> starterAreas = new List<Loc>();
                    for (int j = 0; j < numAreas; j++)
                    {
                        uint cell = datReader.ReadUInt32();
                        Loc starterArea = Loc.Unpack(cell, datReader);
                        starterAreas.Add(starterArea);
                    }

                    cg.StarterAreas.Add(starterAreas);
                }

                /// HERITAGE GROUPS -- 11 standard player races and 2 Olthoi.
                datReader.Offset++; // Not sure what this byte 0x01 is indicating, but we'll skip it because we can.
                int heritageGroupCount = datReader.ReadByte();
                for (int i = 0; i < heritageGroupCount; i++)
                {
                    HeritageGroupCG heritage = new HeritageGroupCG();
                    int heritageIndex = datReader.ReadInt32();
                    heritage.Name = datReader.ReadPStringBase();
                    heritage.IconImage = datReader.ReadUInt32();
                    heritage.SetupID = datReader.ReadUInt32();
                    heritage.EnvironmentSetupID = datReader.ReadUInt32();
                    heritage.AttributeCredits = datReader.ReadUInt32();
                    heritage.SkillCredits = datReader.ReadUInt32();

                    // Start Areas correspond go the CG.StarterAreas List.
                    int numPrimaryStartAreas = datReader.ReadByte();
                    for (int j = 0; j < numPrimaryStartAreas; j++)
                        heritage.PrimaryStartAreaList.Add(datReader.ReadInt32());

                    int numSecondaryStartAreas = datReader.ReadByte();
                    for (int j = 0; j < numSecondaryStartAreas; j++)
                        heritage.SecondaryStartAreaList.Add(datReader.ReadInt32());

                    // Racial Skills
                    int skillCount = datReader.ReadByte();
                    for (int j = 0; j < skillCount; j++)
                    {
                        SkillCG skill = new SkillCG();
                        skill.SkillNum = datReader.ReadUInt32();
                        skill.NormalCost = datReader.ReadUInt32();
                        skill.PrimaryCost = datReader.ReadUInt32();
                        heritage.SkillList.Add(skill);
                    }

                    // Adventurer, Bow Hunter, etc.
                    int templateCount = datReader.ReadByte();
                    for (int j = 0; j < templateCount; j++)
                    {
                        TemplateCG template = new TemplateCG();
                        template.Name = datReader.ReadPStringBase();
                        template.IconImage = datReader.ReadUInt32();
                        template.Title = datReader.ReadUInt32();
                        // Attributes
                        template.Strength = datReader.ReadUInt32();
                        template.Endurance = datReader.ReadUInt32();
                        template.Coordination = datReader.ReadUInt32();
                        template.Quickness = datReader.ReadUInt32();
                        template.Focus = datReader.ReadUInt32();
                        template.Self = datReader.ReadUInt32();

                        skillCount = datReader.ReadByte();
                        for (int k = 0; k < skillCount; k++)
                        {
                            template.NormalSkillsList.Add(datReader.ReadUInt32());
                        }
                        skillCount = datReader.ReadByte();
                        for (int k = 0; k < skillCount; k++)
                        {
                            template.PrimarySkillsList.Add(datReader.ReadUInt32());
                        }

                        heritage.TemplateList.Add(template);
                    }

                    datReader.Offset++; // 0x01 byte here. Not sure what/why, so skip it!
                    int numGenders = datReader.ReadByte(); // this is always 2, but let's read it anyways...
                    for (int j = 0; j < numGenders; j++)
                    {
                        SexCG sex = new SexCG();
                        int genderID = datReader.ReadInt32();
                        sex.Name = datReader.ReadPStringBase();
                        sex.Scale = datReader.ReadUInt32();
                        sex.SetupID = datReader.ReadUInt32();
                        sex.SoundTable = datReader.ReadUInt32();
                        sex.IconImage = datReader.ReadUInt32();
                        sex.BasePalette = datReader.ReadUInt32();
                        sex.SkinPalSet = datReader.ReadUInt32();
                        sex.PhysicsTable = datReader.ReadUInt32();
                        sex.MotionTable = datReader.ReadUInt32();
                        sex.CombatTable = datReader.ReadUInt32();

                        sex.BaseObjDesc = ObjDesc.ReadFromDat(ref datReader);

                        int numHairColors = datReader.ReadByte();
                        for (int k = 0; k < numHairColors; k++)
                        {
                            sex.HairColorList.Add(datReader.ReadUInt32());
                        }

                        int numHairStyles = datReader.ReadByte();
                        for (int k = 0; k < numHairStyles; k++)
                        {
                            HairStyleCG hairstyle = new HairStyleCG();
                            hairstyle.IconImage = datReader.ReadUInt32();
                            hairstyle.Bald = Convert.ToBoolean(datReader.ReadByte());
                            hairstyle.AlternateSetup = datReader.ReadUInt32();
                            hairstyle.ObjDesc = ObjDesc.ReadFromDat(ref datReader);
                            sex.HairStyleList.Add(hairstyle);
                        }

                        int numEyeColors = datReader.ReadByte();
                        for (int k = 0; k < numEyeColors; k++)
                            sex.EyeColorList.Add(datReader.ReadUInt32());

                        int numEyeStrips = datReader.ReadByte();
                        for (int k = 0; k < numEyeStrips; k++)
                        {
                            EyeStripCG eyestrip = new EyeStripCG();
                            eyestrip.IconImage = datReader.ReadUInt32();
                            eyestrip.IconImageBald = datReader.ReadUInt32();
                            eyestrip.ObjDesc = ObjDesc.ReadFromDat(ref datReader);
                            eyestrip.ObjDescBald = ObjDesc.ReadFromDat(ref datReader);
                            sex.EyeStripList.Add(eyestrip);
                        }

                        int numNoseStrips = datReader.ReadByte(); // Breathe Right?
                        for (int k = 0; k < numNoseStrips; k++)
                        {
                            FaceStripCG nosestrip = new FaceStripCG();
                            nosestrip.IconImage = datReader.ReadUInt32();
                            nosestrip.ObjDesc = ObjDesc.ReadFromDat(ref datReader);
                            sex.NoseStripList.Add(nosestrip);
                        }

                        int numMouthStrips = datReader.ReadByte(); // Breathe Right?
                        for (int k = 0; k < numMouthStrips; k++)
                        {
                            FaceStripCG mouthstrip = new FaceStripCG();
                            mouthstrip.IconImage = datReader.ReadUInt32();
                            mouthstrip.ObjDesc = ObjDesc.ReadFromDat(ref datReader);
                            sex.MouthStripList.Add(mouthstrip);
                        }

                        int numHeadGear = datReader.ReadByte();
                        for (int k = 0; k < numHeadGear; k++)
                        {
                            GearCG headgear = new GearCG();
                            headgear.Name = datReader.ReadPStringBase();
                            headgear.ClothingTable = datReader.ReadUInt32();
                            headgear.WeenieDefault = datReader.ReadUInt32();
                            sex.HeadgearList.Add(headgear);
                        }

                        int numShirts = datReader.ReadByte();
                        for (int k = 0; k < numShirts; k++)
                        {
                            GearCG shirt = new GearCG();
                            shirt.Name = datReader.ReadPStringBase();
                            shirt.ClothingTable = datReader.ReadUInt32();
                            shirt.WeenieDefault = datReader.ReadUInt32();
                            sex.ShirtList.Add(shirt);
                        }

                        int numPants = datReader.ReadByte();
                        for (int k = 0; k < numPants; k++)
                        {
                            GearCG pants = new GearCG();
                            pants.Name = datReader.ReadPStringBase();
                            pants.ClothingTable = datReader.ReadUInt32();
                            pants.WeenieDefault = datReader.ReadUInt32();
                            sex.PantsList.Add(pants);
                        }

                        int numFootwear = datReader.ReadByte();
                        for (int k = 0; k < numFootwear; k++)
                        {
                            GearCG footwear = new GearCG();
                            footwear.Name = datReader.ReadPStringBase();
                            footwear.ClothingTable = datReader.ReadUInt32();
                            footwear.WeenieDefault = datReader.ReadUInt32();
                            sex.FootwearList.Add(footwear);
                        }

                        int numClothingColors = datReader.ReadByte();
                        for (int k = 0; k < numClothingColors; k++)
                            sex.ClothingColorsList.Add(datReader.ReadUInt32());

                        heritage.SexList.Add(genderID, sex);
                    }

                    cg.HeritageGroups.Add(heritageIndex, heritage);
                }
                // Store this object in the FileCache
                DatManager.PortalDat.FileCache[0x0E000002] = cg;
                return cg;
            }
        }
    }
}
