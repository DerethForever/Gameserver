/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;

/// <summary>
/// Very special thanks to David Simpson for his early work on reading the cell.dat. Even bigger thanks for his documentation of it!
/// </summary>
namespace DerethForever.DatLoader.FileTypes
{
    public class CellLandblock
    {
        // A landblock is divided into 8 x 8 tiles, which means 9 x 9 vertices reporesenting those tiles. 
        // (Draw a grid of 9x9 dots; connect those dots to form squares; you'll have 8x8 squares)
        // It is also divided in 192x192 units (this is the x and the y)
        // 
        // 0,0 is the bottom left corner of the landblock. 
        // 
        // Height 0-9 is Western most edge. 10-18 is S-to-N strip just to the East. And so on.
        // Places in the inland sea, for example, are false. Should denote presence of xxxxFFFE (where xxxx is the cell).

        /// <summary>
        /// better called "HasExtendedInfo", or whether or not it has an FFFE file associated with it.  aka dungeon blocks.
        /// </summary>
        public bool HasObjects { get; set; }

        public ushort[] Terrain { get; set; } = new ushort[81];

        // Z value in-game is double this height.
        public ushort[] HeightMap { get; set; } = new ushort[81];

        /// <summary>
        /// Simple class to help calulate the Z point.
        /// TODO: Convert to DFVector3 after proof of concept complete.
        /// </summary>
        private class Point3d
        {
            public float X { get; set; }
            public float Y { get; set; }
            public float Z { get; set; }
        }

        /// <summary>
        /// Loads the structure of a CellLandblock from the client_cell.dat
        /// </summary>
        /// <param name="landblockId">Either a full int of the landblock or just the short of the cell itself</param>
        [Obsolete("Thread unsafe static method with high risk of being used in a multithreaded manner.")]
        public static CellLandblock ReadFromDat(uint landblockId)
        {
            CellLandblock c = new CellLandblock();

            // Check if landblockId is a full dword. We just need the hiword for the landblockId
            if ((landblockId >> 16) != 0)
                landblockId = landblockId >> 16;

            // The file index is CELL + 0xFFFF. e.g. a cell of 1234, the file index would be 0x1234FFFF.
            uint landblockFileIndex = (landblockId << 16) + 0xFFFF;

            // Check the FileCache so we don't need to hit the FileSystem repeatedly
            if (DatManager.CellDat.FileCache.ContainsKey(landblockFileIndex))
                return (CellLandblock)DatManager.CellDat.FileCache[landblockFileIndex];

            DatReader datReader = DatManager.CellDat.GetReaderForFile(landblockFileIndex);

            // should match the landblockFileIndex value.  could validate if we wanted to
            uint cellId = datReader.ReadUInt32();
                    
            c.HasObjects = (datReader.ReadUInt32() != 0);

            // Read in the terrain. 9x9 so 81 records.
            for (int i = 0; i < 81; i++)
                c.Terrain[i] = datReader.ReadUInt16();

            // Read in the height. 9x9 so 81 records
            for (int i = 0; i < 81; i++)
                c.HeightMap[i] = datReader.ReadByte();

            // Store this object in the FileCache
            DatManager.CellDat.FileCache[landblockFileIndex] = c;

            return c;
        }

        /// <summary>
        /// Calculates the z value on the CellLandblock plane at coordinate x,y
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>The Z value for a given X/Y in the CellLandblock</returns>
        public float GetZ(float x, float y)
        {
            // Find the exact tile in the 8x8 square grid. The cell is 192x192, so each tile is 24x24
            uint tileX = (uint)Math.Ceiling(x / 24) - 1; // Subract 1 to 0-index these
            uint tileY = (uint)Math.Ceiling(y / 24) - 1; // Subract 1 to 0-index these

            uint v1 = tileX * 9 + tileY;
            uint v2 = tileX * 9 + tileY + 1;
            uint v3 = (tileX + 1) * 9 + tileY;

            Point3d p1 = new Point3d();
            p1.X = tileX * 24;
            p1.Y = tileY * 24;
            p1.Z = HeightMap[(int)v1] * 2;

            Point3d p2 = new Point3d();
            p2.X = tileX * 24;
            p2.Y = (tileY + 1) * 24;
            p2.Z = HeightMap[(int)v2] * 2;

            Point3d p3 = new Point3d();
            p3.X = (tileX + 1) * 24;
            p3.Y = tileY * 24;
            p3.Z = HeightMap[(int)v3] * 2;

            float z = GetPointOnPlane(p1, p2, p3, x, y);
            return z;
        }

        /// <summary>
        /// Note that we only need 3 unique points to calculate our plane.
        /// https://social.msdn.microsoft.com/Forums/en-US/1b32dc40-f84d-4365-a677-b59e49d41eb0/how-to-calculate-a-point-on-a-plane-based-on-a-plane-from-3-points?forum=vbgeneral 
        /// </summary>
        private float GetPointOnPlane(Point3d p1, Point3d p2, Point3d p3, float x, float y)
        {
            Point3d v1 = new Point3d();
            Point3d v2 = new Point3d();
            Point3d abc = new Point3d();

            v1.X = p1.X - p3.X;
            v1.Y = p1.Y - p3.Y;
            v1.Z = p1.Z - p3.Z;

            v2.X = p2.X - p3.X;
            v2.Y = p2.Y - p3.Y;
            v2.Z = p2.Z - p3.Z;

            abc.X = (v1.Y * v2.Z) - (v1.Z * v2.Y);
            abc.Y = (v1.Z * v2.X) - (v1.X * v2.Z);
            abc.Z = (v1.X * v2.Y) - (v1.Y * v2.X);

            float d = (abc.X * p3.X) + (abc.Y * p3.Y) + (abc.Z * p3.Z);

            float z = (d - (abc.X * x) - (abc.Y * y)) / abc.Z;

            return z;
        }
    }
}
