﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

using DerethForever.DatLoader.Entity;

namespace DerethForever.DatLoader.FileTypes
{
    public class SpellComponentsTable
    {
        private uint FileId { get; set; }
        public Dictionary<uint, SpellComponentBase> SpellComponents { get; set; } = new Dictionary<uint, SpellComponentBase>();

        public static SpellComponentsTable ReadFromDat()
        {
            // Check the FileCache so we don't need to hit the FileSystem repeatedly
            if (DatManager.PortalDat.FileCache.ContainsKey(0x0E00000F))
            {
                return (SpellComponentsTable)DatManager.PortalDat.FileCache[0x0E00000F];
            }
            else
            {
                // Create the datReader for the proper file
                DatReader datReader = DatManager.PortalDat.GetReaderForFile(0x0E00000F);
                SpellComponentsTable comps = new SpellComponentsTable();

                comps.FileId = datReader.ReadUInt32();
                uint numComps = datReader.ReadUInt16(); // Should be 163 or 0xA3
                datReader.AlignBoundary();
                // loop through the entire file...
                for (uint i = 0; i < numComps; i++)
                {
                    SpellComponentBase newComp = new SpellComponentBase();
                    uint compId = datReader.ReadUInt32();
                    newComp.Name = datReader.ReadObfuscatedString();
                    datReader.AlignBoundary();
                    newComp.Category = datReader.ReadUInt32();
                    newComp.Icon = datReader.ReadUInt32();
                    newComp.Type = datReader.ReadUInt32();
                    newComp.Gesture = datReader.ReadUInt32();
                    newComp.Time = datReader.ReadSingle();
                    newComp.Text = datReader.ReadObfuscatedString();
                    datReader.AlignBoundary();
                    newComp.CDM = datReader.ReadSingle();
                    comps.SpellComponents.Add(compId, newComp);
                }

                DatManager.PortalDat.FileCache[0x0E00000F] = comps;
                return comps;
            }
        }

        // TODO - Complete this function.
        public static string GetSpellWords(List<uint> comps)
        {
            string result = "";
            return result;
        }
    }
}
