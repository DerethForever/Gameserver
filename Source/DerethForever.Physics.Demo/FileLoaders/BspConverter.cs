/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    Bullet Collision Detection and Physics Library
    Copyright (c) 2012 Advanced Micro Devices, Inc.  http://bulletphysics.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

using BulletSharp;
using System.Collections.Generic;

namespace DerethForever.Physics.Demo.FileLoaders
{
    public abstract class BspConverter
    {
        public void ConvertBsp(BspLoader bspLoader, float scaling)
        {
            Vector3 playerStart = GetPlayerPosition(bspLoader);
            playerStart.Z += 20.0f; //start a bit higher
            playerStart *= scaling;

            foreach (BspLeaf leaf in bspLoader.Leaves)
            {
                bool isValidBrush = false;

                for (int b = 0; b < leaf.NumLeafBrushes; b++)
                {
                    int brushID = bspLoader.LeafBrushes[leaf.FirstLeafBrush + b];
                    BspBrush brush = bspLoader.Brushes[brushID];

                    if (brush.ShaderNum == -1) continue;

                    ContentFlags flags = bspLoader.IsVbsp
                        ? (ContentFlags)brush.ShaderNum
                        : bspLoader.Shaders[brush.ShaderNum].ContentFlags;

                    if ((flags & ContentFlags.Solid) == 0) continue;

                    var planeEquations = new List<Vector4>();
                    brush.ShaderNum = -1;

                    for (int p = 0; p < brush.NumSides; p++)
                    {
                        int sideId = brush.FirstSide + p;

                        BspBrushSide brushside = bspLoader.BrushSides[sideId];
                        BspPlane plane = bspLoader.Planes[brushside.PlaneNum];
                        Vector4 planeEquation = new Vector4(plane.Normal, scaling * -plane.Distance);
                        planeEquations.Add(planeEquation);
                        isValidBrush = true;
                    }
                    if (isValidBrush)
                    {
                        List<Vector3> vertices = GeometryUtil.GetVerticesFromPlaneEquations(planeEquations);
                        const bool isEntity = false;
                        Vector3 entityTarget = Vector3.Zero;
                        AddConvexVerticesCollider(vertices, isEntity, entityTarget);
                    }
                }
            }
            /*
            foreach (BspEntity entity in bspLoader.Entities)
            {
                if (entity.ClassName == "trigger_push")
                {
                }
            }
            */
        }

        private Vector3 GetPlayerPosition(BspLoader bspLoader)
        {
            BspEntity player;
            if (bspLoader.Entities.TryGetValue("info_player_start", out player))
            {
                return player.Origin;
            }
            else if (bspLoader.Entities.TryGetValue("info_player_deathmatch", out player))
            {
                return player.Origin;
            }
            return new Vector3(0, 0, 100);
        }

        public abstract void AddConvexVerticesCollider(List<Vector3> vertices, bool isEntity, Vector3 entityTargetLocation);
    }
}
