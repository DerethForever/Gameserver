/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

    Bullet Collision Detection and Physics Library
    Copyright (c) 2012 Advanced Micro Devices, Inc.  http://bulletphysics.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

namespace DerethForever.Physics.Demo.Meshes
{
    public static class Taru
    {
        public static float[] Vertices =
        {
            1.08664f,-1.99237f,0.0f,
            0.768369f,-1.99237f,-0.768369f,
            1.28852f,1.34412e-007f,-1.28852f,
            1.82224f,1.90735e-007f,0.0f,
            0.0f,-1.99237f,-1.08664f,
            0.0f,0.0f,-1.82224f,
            0.0f,-1.99237f,-1.08664f,
            -0.768369f,-1.99237f,-0.768369f,
            -1.28852f,1.34412e-007f,-1.28852f,
            0.0f,0.0f,-1.82224f,
            -1.08664f,-1.99237f,1.82086e-007f,
            -1.82224f,1.90735e-007f,1.59305e-007f,
            -0.768369f,-1.99237f,0.76837f,
            -1.28852f,2.47058e-007f,1.28852f,
            1.42495e-007f,-1.99237f,1.08664f,
            2.38958e-007f,2.70388e-007f,1.82224f,
            0.768369f,-1.99237f,0.768369f,
            1.28852f,2.47058e-007f,1.28852f,
            0.768369f,1.99237f,-0.768369f,
            1.08664f,1.99237f,0.0f,
            0.0f,1.99237f,-1.08664f,
            -0.768369f,1.99237f,-0.768369f,
            0.0f,1.99237f,-1.08664f,
            -1.08664f,1.99237f,0.0f,
            -0.768369f,1.99237f,0.768369f,
            1.42495e-007f,1.99237f,1.08664f,
            0.768369f,1.99237f,0.768369f,
            1.42495e-007f,-1.99237f,1.08664f,
            -0.768369f,-1.99237f,0.76837f,
            -1.08664f,-1.99237f,1.82086e-007f,
            -0.768369f,-1.99237f,-0.768369f,
            0.0f,-1.99237f,-1.08664f,
            0.768369f,-1.99237f,-0.768369f,
            1.08664f,-1.99237f,0.0f,
            0.768369f,-1.99237f,0.768369f,
            0.768369f,1.99237f,-0.768369f,
            0.0f,1.99237f,-1.08664f,
            -0.768369f,1.99237f,-0.768369f,
            -1.08664f,1.99237f,0.0f,
            -0.768369f,1.99237f,0.768369f,
            1.42495e-007f,1.99237f,1.08664f,
            0.768369f,1.99237f,0.768369f,
            1.08664f,1.99237f,0.0f
        };
    }
}
