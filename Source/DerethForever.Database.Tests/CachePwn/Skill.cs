/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using Newtonsoft.Json;

namespace DerethForever.Database.Tests.CachePwn
{
    public class Skill
    {
        [JsonProperty("init_level")]
        public uint? Base { get; set; }

        [JsonProperty("last_used_time")]
        public float? LastUsed { get; set; }

        [JsonProperty("level_from_pp")]
        public uint? Ranks { get; set; }

        [JsonProperty("pp")]
        public uint? XpInvested { get; set; }

        [JsonProperty("resistance_of_last_check")]
        public uint? ResistanceOfLastCheck { get; set; }

        [JsonProperty("sac")]
        public int? TrainedLevel { get; set; }
    }
}
