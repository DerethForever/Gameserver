/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Common;

using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace DerethForever.Entity
{
    [DbTable("ace_static_spawn")]
    public class WeenieObjectInstance
    {
        // not needed to be loaded into object
        // [DbField("instanceId", (int)MySqlDbType.UInt32, IsCriteria = true, ListGet = true)]
        // public uint InstanceId { get; set; }

        /// <summary>
        /// this is just the high-word landblock x/y.  it does not contain cell data.
        /// </summary>
        [JsonIgnore]
        [DbField("landblock", (int)MySqlDbType.Int32, IsCriteria = true, ListGet = true)]
        public int Landblock { get; set; }

        [JsonProperty("weenieClassId")]
        [DbField("weenieClassId", (int)MySqlDbType.UInt32)]
        public uint WeenieClassId { get; set; }

        [JsonProperty("instanceId")]
        [DbField("preassignedGuid", (int)MySqlDbType.UInt32)]
        public uint PreassignedGuid { get; set; }

        /// <summary>
        /// full landblock x/y + cell data
        /// </summary>
        [JsonProperty("landblock")]
        [DbField("landblockRaw", (int)MySqlDbType.UInt32)]
        public uint LandblockRaw { get; set; }

        [JsonProperty("x")]
        [DbField("posX", (int)MySqlDbType.Float)]
        public float PositionX { get; set; }

        [JsonProperty("y")]
        [DbField("posY", (int)MySqlDbType.Float)]
        public float PositionY { get; set; }

        [JsonProperty("z")]
        [DbField("posZ", (int)MySqlDbType.Float)]
        public float PositionZ { get; set; }

        [JsonProperty("qw")]
        [DbField("qW", (int)MySqlDbType.Float)]
        public float RotationW { get; set; }

        [JsonProperty("qx")]
        [DbField("qX", (int)MySqlDbType.Float)]
        public float RotationX { get; set; }

        [JsonProperty("qy")]
        [DbField("qY", (int)MySqlDbType.Float)]
        public float RotationY { get; set; }

        [JsonProperty("qz")]
        [DbField("qZ", (int)MySqlDbType.Float)]
        public float RotationZ { get; set; }
    }
}
