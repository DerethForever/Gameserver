/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.IO;
using System.Numerics;

namespace DerethForever.Entity
{
    /// <summary>
    /// Vector3 with Dereth Forever Serialize
    /// </summary>
    public class DFVector3
    {
        private Vector3 vector;

        public DFVector3(float x, float y, float z)
        {
            vector.X = x; vector.Y = y; vector.Z = z;
        }

        public void Update(float x, float y, float z)
        {
            vector.X = x; vector.Y = y; vector.Z = z;
        }

        public Vector3 Forward()
        {
            // todo figure out how to calculate vector forward for AC.
            return vector;
        }

        public Vector3 RawVector
        {
            get { return vector; }
        }

        /// <summary>
        /// converts this vector into a bulletsharp vector.  NOTE: also does Y/Z inversion
        /// </summary>
        public BulletSharp.Vector3 GetBulletVector()
        {
            return new BulletSharp.Vector3(vector.X, vector.Z, vector.Y);
        }

        public void Serialize(BinaryWriter payload)
        {
            payload.Write(vector.X);
            payload.Write(vector.Y);
            payload.Write(vector.Z);
        }
    }
}
