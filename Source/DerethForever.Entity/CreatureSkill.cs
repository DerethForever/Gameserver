/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

using DerethForever.Entity.Enum;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DerethForever.Entity
{
    public class CreatureSkill : ICloneable
    {
        private DataObjectPropertiesSkill _backer;

        // because skill values are determined from stats, we need a reference to the character
        // so we can calculate.  this could be refactored into a better pattern, but it will
        // do for now.
        private ICreatureStats character;
        
        [JsonProperty("skillId")]
        public Skill Skill
        {
            get { return (Skill)_backer.SkillId; }
            set
            {
                _backer.SkillId = (ushort)value;
            }
        }

        [JsonProperty("skillStatus")]
        public SkillStatus Status
        {
            get { return (SkillStatus)_backer.SkillStatus; }
            set
            {
                _backer.SkillStatus = (ushort)value;
                _backer.IsDirty = true;
            }
        }

        [JsonProperty("ranks")]
        public uint Ranks
        {
            get { return _backer.SkillPoints; }
            set
            {
                _backer.SkillPoints = (ushort)value;
                _backer.IsDirty = true;
            }
        }

        [JsonProperty("baseValue")]
        public uint ValueFromAttributes
        {
            get
            {
                var formula = this.Skill.GetFormula();

                uint skillTotal = 0;

                if (formula != null)
                {
                    if ((Status == SkillStatus.Untrained && Skill.GetUsability().UsableUntrained) ||
                        Status == SkillStatus.Trained ||
                        Status == SkillStatus.Specialized)
                    {
                        skillTotal = formula.CalcBase(character);
                    }
                }

                return skillTotal;
            }
            set
            {
            }
        }

        [JsonIgnore]
        public uint UnbuffedValue
        {
            get
            {
                // TODO: buffs? Augs? not sure where they will go
                uint skillTotal = ValueFromAttributes;
                skillTotal += this.Ranks;

                return skillTotal;
            }
        }

        [JsonProperty("experienceSpent")]
        public uint ExperienceSpent
        {
            get { return _backer.SkillXpSpent; }
            set
            {
                _backer.SkillXpSpent = value;
                _backer.IsDirty = true;
            }
        }

        [JsonIgnore]
        public uint ActiveValue
        {
            // FIXME(ddevec) -- buffs?:
            get { return UnbuffedValue; }
        }

        /// <summary>
        /// used for compatibility with the API only.  skills made with this constructor will not function
        /// propery if loaded into a creature or player
        /// </summary>
        public CreatureSkill()
        {
            _backer = new DataObjectPropertiesSkill();
        }

        public CreatureSkill(ICreatureStats character, Skill skill, SkillStatus status, uint ranks, uint xpSpent)
        {
            this.character = character;
            _backer = new DataObjectPropertiesSkill();
            _backer.DataObjectId = character.DataObjectId;
            Skill = skill;
            Status = status;
            Ranks = ranks;
            ExperienceSpent = xpSpent;
        }

        public CreatureSkill(ICreatureStats character, DataObjectPropertiesSkill skill)
        {
            this.character = character;
            _backer = skill;
        }

        public DataObjectPropertiesSkill GetDataObjectSkill()
        {
            return _backer;
        }

        public void ClearDirtyFlags()
        {
            _backer.IsDirty = false;
            _backer.HasEverBeenSavedToDatabase = true;
        }

        public void SetDirtyFlags()
        {
            _backer.IsDirty = true;
            _backer.HasEverBeenSavedToDatabase = false;
        }

        public object Clone()
        {
            return new CreatureSkill(this.character, (DataObjectPropertiesSkill)_backer.Clone());
        }

        public double GetPercentSuccess(uint difficulty)
        {
            return GetPercentSuccess(ActiveValue, difficulty);
        }

        public static double GetPercentSuccess(uint skillLevel, uint difficulty)
        {
            int delta = (int)(skillLevel - difficulty);
            var scalar = 1d + Math.Pow(Math.E, 0.03 * delta);
            var percentSuccess = 1d - (1d / scalar);
            return percentSuccess;
        }
    }
}
