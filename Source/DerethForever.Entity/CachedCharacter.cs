﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Common;

using MySql.Data.MySqlClient;

namespace DerethForever.Entity
{
    [DbTable("vw_ace_character")]
    [DbGetAggregate("vw_ace_character", 106, "MAX", "guid")]
    public class CachedCharacter
    {
        [DbField("guid", (int)MySqlDbType.UInt32)]
        public uint FullGuid { get; set; }

        public ObjectGuid Guid
        {
            get
            {
                return new ObjectGuid(FullGuid);
            }
            set
            {
                FullGuid = value.Full;
            }
        }

        public byte SlotId { get; set; }

        [DbField("subscriptionId", (int)MySqlDbType.UInt32, ListGet = true)]
        public uint SubscriptionId { get; set; }

        [DbField("name", (int)MySqlDbType.VarChar, IsCriteria = true)]
        public string Name { get; set; }

        [DbField("deleted", (int)MySqlDbType.Bit, ListGet = true)]
        public bool Deleted { get; set; }

        [DbField("deleteTime", (int)MySqlDbType.UInt64)]
        public ulong DeleteTime { get; set; }

        [DbField("loginTimestamp", (int)MySqlDbType.Double)]
        public double LoginTimestamp { get; set; }

        public CachedCharacter() { }

        public CachedCharacter(ObjectGuid guid, byte slotId, string name, ulong deleteTime)
        {
            Guid = guid;
            SlotId = slotId;
            Name = name;
            DeleteTime = deleteTime;
        }
    }
}
