/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

using DerethForever.Entity.Enum;

namespace DerethForever.Entity
{
    public struct LandblockId
    {
        private uint rawValue;

        public LandblockId(uint raw)
        {
            rawValue = raw;
        }

        public LandblockId(byte x, byte y)
        {
            rawValue = (uint)x << 24 | (uint)y << 16;
        }

        public LandblockId East
        {
            get { return new LandblockId(Convert.ToByte(LandblockX + 1), LandblockY); }
        }

        public LandblockId West
        {
            get { return new LandblockId(Convert.ToByte(LandblockX - 1), LandblockY); }
        }

        public LandblockId North
        {
            get { return new LandblockId(LandblockX, Convert.ToByte(LandblockY + 1)); }
        }

        public LandblockId South
        {
            get { return new LandblockId(LandblockX, Convert.ToByte(LandblockY - 1)); }
        }

        public LandblockId NorthEast
        {
            get { return new LandblockId(Convert.ToByte(LandblockX + 1), Convert.ToByte(LandblockY + 1)); }
        }

        public LandblockId NorthWest
        {
            get { return new LandblockId(Convert.ToByte(LandblockX - 1), Convert.ToByte(LandblockY + 1)); }
        }

        public LandblockId SouthEast
        {
            get { return new LandblockId(Convert.ToByte(LandblockX + 1), Convert.ToByte(LandblockY - 1)); }
        }

        public LandblockId SouthWest
        {
            get { return new LandblockId(Convert.ToByte(LandblockX - 1), Convert.ToByte(LandblockY - 1)); }
        }

        public uint Raw
        {
            get { return rawValue; }
        }

        public ushort Landblock
        {
            get { return (ushort)((rawValue >> 16) & 0xFFFF); }
        }

        public byte LandblockX
        {
            get { return (byte)((rawValue >> 24) & 0xFF); }
        }

        public byte LandblockY
        {
            get { return (byte)((rawValue >> 16) & 0xFF); }
        }
        /// <summary>
        /// This is only used to calclate LandcellX and LandcellY - it has no other function.
        /// </summary>
        private ushort Landcell
        {
            get { return (byte)((rawValue & 0x3F) - 1); }
        }

        public byte LandcellX
        {
            get { return Convert.ToByte((Landcell >> 3) & 0x7); }
        }

        public byte LandcellY
        {
            get { return Convert.ToByte(Landcell & 0x7); }
        }

        public MapScope MapScope
        {
            get { return (MapScope)((rawValue & 0x0F00) >> 8); }
        }

        public static bool operator ==(LandblockId c1, LandblockId c2)
        {
            return c1.Landblock == c2.Landblock;
        }

        public static bool operator !=(LandblockId c1, LandblockId c2)
        {
            return c1.Landblock != c2.Landblock;
        }

        public bool IsAdjacentTo(LandblockId block)
        {
            return (Math.Abs(this.LandblockX - block.LandblockX) <= 1 && Math.Abs(this.LandblockY - block.LandblockY) <= 1);
        }
        public override bool Equals(object obj)
        {
            if (obj is LandblockId)
                return ((LandblockId)obj) == this;
            else
                return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
