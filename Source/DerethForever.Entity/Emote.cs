/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Common;
using DerethForever.Entity.Enum;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;

namespace DerethForever.Entity
{
    [DbTable("emote")]
    public class Emote
    {
        /// <summary>
        /// Table field Primary Key
        /// </summary>
        [JsonProperty("emoteGuid")]
        public Guid? EmoteGuid { get; set; }

        [JsonIgnore]
        [DbField("emoteGuid", (int)MySqlDbType.Binary, Update = false, IsCriteria = true)]
        public byte[] EmoteGuid_Binder
        {
            get { return EmoteGuid.Value.ToByteArray(); }
            set { EmoteGuid = new Guid(value); }
        }

        /// <summary>
        /// guid of the emote set
        /// </summary>
        [JsonProperty("emoteSetGuid")]
        public Guid? EmoteSetGuid { get; set; }

        [JsonIgnore]
        [DbField("emoteSetGuid", (int)MySqlDbType.Binary, ListGet = true, ListDelete = true)]
        public byte[] EmoteSetGuid_Binder
        {
            get { return EmoteSetGuid.Value.ToByteArray(); }
            set { EmoteSetGuid = new Guid(value); }
        }

        /// <summary>
        /// Emote Type Id - these are found in the code emotetype.cs
        /// </summary>
        [JsonProperty("emoteTypeId")]
        [DbField("emoteTypeId", (int)MySqlDbType.UInt32)]
        public uint EmoteTypeId { get; set; }

        [JsonIgnore]
        public EmoteType EmoteType
        {
            get { return (EmoteType)EmoteTypeId; }
            set { EmoteTypeId = (uint)value; }
        }

        [JsonProperty("delay")]
        [DbField("delay", (int)MySqlDbType.Float)]
        public float Delay { get; set; }

        [JsonProperty("extent")]
        [DbField("extent", (int)MySqlDbType.Float)]
        public float Extent { get; set; }

        [JsonProperty("message")]
        [DbField("message", (int)MySqlDbType.VarChar)]
        public string Message { get; set; } = "";

        [JsonProperty("amount")]
        [DbField("amount", (int)MySqlDbType.UInt32)]
        public uint? Amount { get; set; }

        [JsonProperty("stat")]
        [DbField("stat", (int)MySqlDbType.UInt32)]
        public uint? Stat { get; set; }

        /// <summary>
        /// Emote Types:
        ///   ( / 0x31)
        ///   ( / 0x32)
        ///   ( / 0x76)
        /// </summary>
        [JsonProperty("percent")]
        [DbField("percent", (int)MySqlDbType.Double)]
        public double? Percent { get; set; }

        [JsonProperty("minimum")]
        [DbField("minimum", (int)MySqlDbType.UInt32)]
        public uint? Minimum { get; set; }

        [JsonProperty("maximum")]
        [DbField("maximum", (int)MySqlDbType.UInt32)]
        public uint? Maximum { get; set; }

        [JsonProperty("amount64")]
        [DbField("amount64", (int)MySqlDbType.UInt64)]
        public ulong? Amount64 { get; set; }

        [JsonProperty("heroXp64")]
        [DbField("heroXp64", (int)MySqlDbType.UInt64)]
        public ulong? HeroXp64 { get; set; }

        /// <summary>
        /// this object is misleading.  it only exists for the purposes of json (de)serialization.  the data
        /// is saved via the CP_* properties even though the CreationProfile class's properties are also
        /// decorated.  those properties are used when the creation profile exists as a property entity.
        ///
        /// Emote Types:
        ///   Give (3 / 0x03)
        ///   InqOwnsItems (76 / 0x4C)
        ///   TakeItems (74 / 0x4A)
        /// </summary>
        [JsonProperty("item")]
        public CreationProfile CreationProfile { get; set; } = new CreationProfile();

        /// <summary>
        /// CreationProfile.WeenieClassId for database binding only
        /// </summary>
        [JsonIgnore]
        [DbField("cpWeenieClassId", (int)MySqlDbType.UInt32)]
        public uint? CP_WeenieClassId
        {
            get { return CreationProfile?.WeenieClassId; }
            set { CreationProfile.WeenieClassId = value; }
        }

        [JsonIgnore]
        [DbField("cpPalette", (int)MySqlDbType.UInt32)]
        public uint? CP_Palette
        {
            get { return CreationProfile?.Palette; }
            set { CreationProfile.Palette = value; }
        }

        [JsonIgnore]
        [DbField("cpShade", (int)MySqlDbType.Double)]
        public double? CP_Shade
        {
            get { return CreationProfile?.Shade; }
            set { CreationProfile.Shade = value; }
        }

        [JsonIgnore]
        [DbField("cpDestination", (int)MySqlDbType.UInt32)]
        public uint? CP_Destination
        {
            get { return CreationProfile?.Destination; }
            set { CreationProfile.Destination = value; }
        }

        [JsonIgnore]
        [DbField("cpStackSize", (int)MySqlDbType.Int32)]
        public int? CP_StackSize
        {
            get { return CreationProfile?.StackSize; }
            set { CreationProfile.StackSize = value; }
        }

        [JsonIgnore]
        public bool? CP_TryToBond
        {
            get { return CreationProfile?.TryToBond; }
            set { CreationProfile.TryToBond = value; }
        }

        [JsonIgnore]
        [DbField("cpTryToBond", (int)MySqlDbType.Byte)]
        public object CP_TryToBond_Binder
        {
            get { return CP_TryToBond == null ? (byte?)null : (CP_TryToBond.Value ? (byte)1 : (byte)0); }
            set
            {
                if (value is byte?)
                {
                    byte? v = (byte?)value;
                    CP_TryToBond = (v == null ? (bool?)null : (v.Value != 0));
                }
                else if (value is bool?)
                    CP_TryToBond = (bool?)value;
                else if (value == null)
                    CP_TryToBond = null;
                else if (value is SByte) // no idea wtf this is, but that's how it comes in
                    CP_TryToBond = ((SByte)value != 0);
                else
                    throw new ArgumentException("wtf");
            }
        }

        /// <summary>
        /// Emote Types:
        ///   CreateTreasure (56 / 0x38)
        /// </summary>
        [JsonProperty("wealthRating")]
        [DbField("wealthRating", (int)MySqlDbType.Int32)]
        public uint? WealthRating { get; set; }

        /// <summary>
        /// Emote Types:
        ///   CreateTreasure (56 / 0x38)
        /// </summary>
        [JsonProperty("treasureClass")]
        [DbField("treasureClass", (int)MySqlDbType.Int32)]
        public uint? TreasureClass { get; set; }

        /// <summary>
        /// Emote Types:
        ///   CreateTreasure (56 / 0x38)
        /// </summary>
        [JsonProperty("treasureType")]
        [DbField("treasureType", (int)MySqlDbType.Int32)]
        public uint? TreasureType { get; set; }

        /// <summary>
        /// Emote Types:
        ///   Motion (5 / 0x05)
        ///   ForceMotion (52 / 0x34)
        /// </summary>
        [JsonProperty("motion")]
        [DbField("motion", (int)MySqlDbType.Int64)]
        public long? Motion { get; set; }

        /// <summary>
        /// Emote Types:
        ///   MoveHome (4 / 0x04)
        ///   Move (6 / 0x06)
        ///   Turn (11 / 0x0B)
        ///   MoveToPos (87 / 0x57)
        ///
        /// These also use a Position
        ///   ( / 0x3F)
        ///   ( / 0x63)
        ///   ( / 0x64)
        /// </summary>
        [JsonIgnore]
        public Position Frame
        {
            get
            {
                if (PositionLandBlockId != null && PositionX != null && PositionY != null && PositionZ != null && RotationX != null && RotationY != null && RotationZ != null && RotationW != null)
                    return new Position((uint)PositionLandBlockId, (float)PositionX, (float)PositionY, (float)PositionZ, (float)RotationX, (float)RotationY, (float)RotationZ, (float)RotationW);
                return null;
            }
        }

        /// <summary>
        /// Emote Types:
        ///   PhysScript (7 / 0x07)
        /// </summary>

        [JsonProperty("physicsScript")]
        [DbField("physicsScript", (int)MySqlDbType.UInt32)]
        public uint? PhysicsScript { get; set; }

        /// <summary>
        /// Emote Types:
        ///   Sound (9 / 0x09)
        /// </summary>
        [JsonProperty("sound")]
        [DbField("sound", (int)MySqlDbType.UInt32)]
        public uint? Sound { get; set; }

        /// <summary>
        /// Emote Types:
        ///   InqStringStat (38 / 0x26)
        ///   InqYesNo (75 / 0x4B)
        /// </summary>
        [JsonProperty("testString")]
        [DbField("testString", (int)MySqlDbType.VarChar)]
        public string TestString { get; set; }

        /// <summary>
        /// Emote Types:
        ///   ( / 0x72)
        ///   ( / 0x31)
        /// </summary>
        [JsonProperty("minimum64")]
        [DbField("minimum64", (int)MySqlDbType.UInt64)]
        public long? Minimum64 { get; set; }

        /// <summary>
        /// Emote Types:
        ///   ( / 0x72)
        ///   ( / 0x31)
        /// </summary>
        [JsonProperty("maximum64")]
        [DbField("maximum64", (int)MySqlDbType.UInt64)]
        public long? Maximum64 { get; set; }

        /// <summary>
        /// Emote Types:
        ///   ( / 0x25)
        /// </summary>
        [JsonProperty("minimumFloat")]
        [DbField("minimumFloat", (int)MySqlDbType.Float)]
        public float? MinimumFloat { get; set; }

        /// <summary>
        /// Emote Types:
        ///   ( / 0x25)
        /// </summary>
        [JsonProperty("maximumFloat")]
        [DbField("maximumFloat", (int)MySqlDbType.Float)]
        public float? MaximumFloat { get; set; }

        /// <summary>
        /// Emote Types:
        ///   ( / 0x32)
        /// </summary>
        [JsonProperty("display")]
        public bool? Display { get; set; }

        [JsonIgnore]
        [DbField("display", (int)MySqlDbType.Byte)]
        public object Display_Binder
        {
            get { return Display == null ? (byte?)null : (Display.Value ? (byte)1 : (byte)0); }
            set
            {
                if (value is byte?)
                {
                    byte? v = (byte?)value;
                    Display = (v == null ? (bool?)null : (v.Value != 0));
                }
                else if (value is bool?)
                    Display = (bool?)value;
                else if (value == null)
                    Display = null;
                else if (value is SByte) // no idea wtf this is, but that's how it comes in
                    Display = ((SByte)value != 0);
                else
                    throw new ArgumentException("wtf");
            }
        }

        [JsonProperty("positionLandblockId")]
        [DbField("positionLandblockId", (int)MySqlDbType.Int32)]
        public uint? PositionLandBlockId { get; set; }

        [JsonProperty("positionX")]
        [DbField("positionX", (int)MySqlDbType.Float)]
        public float? PositionX { get; set; }

        [JsonProperty("positionY")]
        [DbField("positionY", (int)MySqlDbType.Float)]
        public float? PositionY { get; set; }

        [JsonProperty("positionZ")]
        [DbField("positionZ", (int)MySqlDbType.Float)]
        public float? PositionZ { get; set; }

        [JsonProperty("rotationW")]
        [DbField("rotationW", (int)MySqlDbType.Float)]
        public float? RotationW { get; set; }

        [JsonProperty("rotationX")]
        [DbField("rotationX", (int)MySqlDbType.Float)]
        public float? RotationX { get; set; }

        [JsonProperty("rotationY")]
        [DbField("rotationY", (int)MySqlDbType.Float)]
        public float? RotationY { get; set; }

        [JsonProperty("rotationZ")]
        [DbField("rotationZ", (int)MySqlDbType.Float)]
        public float? RotationZ { get; set; }

        [JsonProperty("sortOrder")]
        [DbField("sortOrder", (int)MySqlDbType.UInt32)]
        public uint? SortOrder { get; set; }

        [JsonProperty("spellId")]
        [DbField("spellId", (int)MySqlDbType.UInt32)]
        public uint? SpellId { get; set; }
    }
}
