﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using DerethForever.Common;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace DerethForever.Entity
{
    [DbTable("ace_object_properties_book")]
    public class DataObjectPropertiesBook : BaseDataProperty, ICloneable
    {
        [JsonProperty("page")]
        [DbField("page", (int)MySqlDbType.UInt16, IsCriteria = true, Update = false)]
        public uint Page { get; set; }

        [JsonProperty("authorName")]
        [DbField("authorName", (int)MySqlDbType.VarChar)]
        public string AuthorName { get; set; }

        [JsonProperty("authorAccount")]
        [DbField("authorAccount", (int)MySqlDbType.VarChar)]
        public string AuthorAccount { get; set; }

        [JsonProperty("authorId")]
        [DbField("authorId", (int)MySqlDbType.UInt32)]
        public uint AuthorId { get; set; } = 0xFFFFFFFF;

        [JsonProperty("ignoreAuthor")]
        [DbField("ignoreAuthor", (int)MySqlDbType.UInt32)]
        public uint IgnoreAuthor { get; set; } = 0;

        [JsonProperty("pageText")]
        [DbField("pageText", (int)MySqlDbType.VarChar)]
        public string PageText { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
