/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

using DerethForever.Entity.Enum;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DerethForever.Entity
{
    public class CreatureAbility : ICloneable, ICreatureXpSpendableStat
    {
        private DataObjectPropertiesAttribute _backer;

        [JsonProperty("ability")]
        public Ability Ability
        {
            get { return (Ability)_backer.AttributeId; }
            set { _backer.AttributeId = (ushort)value; }
        }

        /// <summary>
        /// Returns the Base Value for a Creature's Ability, for Players this is set durring Character Creation 
        /// </summary>
        [JsonProperty("baseValue")]
        public uint Base
        {
            get { return _backer.AttributeBase; }
            set
            {
                _backer.AttributeBase = (ushort)value;
                _backer.IsDirty = true;
            }
        }

        /// <summary>
        /// Returns the Current Rank for a Creature's Ability
        /// </summary>
        [JsonProperty("ranks")]
        public uint Ranks
        {
            get { return _backer.AttributeRanks; }
            set
            {
                _backer.AttributeRanks = (ushort)value;
                _backer.IsDirty = true;
            }
        }

        [JsonIgnore]
        public uint Current
        {
            get { return UnbuffedValue; }
        }

        /// <summary>
        /// For Primary Abilities, Returns the Base Value Plus the Ranked Value
        /// For Secondary Abilities, Returns the adjusted Value depending on the current Abiliy formula
        /// </summary>
        [JsonIgnore]
        public uint UnbuffedValue
        {
            get
            {
                // TODO: buffs?  not sure where they will go
                return this.Ranks + this.Base;
            }
        }

        /// <summary>
        /// Returns the MaxValue of an ability, UnbuffedValue + Additional
        /// </summary>
        [JsonIgnore]
        public uint MaxValue
        {
            get
            {
                // TODO: once buffs are implemented, make sure we have a max Value wich calculates the buffs in, 
                // as it's needed for the UpdateHealth GameMessage. For now this is just the unbuffed value.

                return UnbuffedValue;
            }
        }

        /// <summary>
        /// Total Experience Spent on an ability
        /// </summary>
        [JsonProperty("experienceSpent")]
        public uint ExperienceSpent
        {
            get { return _backer.AttributeXpSpent; }
            set
            {
                _backer.AttributeXpSpent = value;
                _backer.IsDirty = true;
            }
        }

        /// <summary>
        /// DO NOT USE.  this is only for use by the API.
        /// </summary>
        public CreatureAbility()
        {
            _backer = new DataObjectPropertiesAttribute();
        }

        public CreatureAbility(Ability ability)
        {
            _backer = new DataObjectPropertiesAttribute();

            Ability = ability;
            Base = 10;
        }

        public CreatureAbility(DataObjectPropertiesAttribute attrib)
        {
            _backer = attrib;
        }

        public DataObjectPropertiesAttribute GetAttribute()
        {
            return _backer;
        }

        public void ClearDirtyFlags()
        {
            _backer.IsDirty = false;
            _backer.HasEverBeenSavedToDatabase = true;
        }

        public void SetDirtyFlags()
        {
            _backer.IsDirty = true;
            _backer.HasEverBeenSavedToDatabase = false;
        }

        public object Clone()
        {
            return new CreatureAbility((DataObjectPropertiesAttribute)_backer.Clone());
        }
    }
}
