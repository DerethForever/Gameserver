/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using MySql.Data.MySqlClient;
using System;

using DerethForever.Common;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DerethForever.Entity
{
    [DbTable("ace_recipe")]
    public class Recipe
    {
        [JsonProperty("recipeGuid")]
        public Guid? RecipeGuid { get; set; }

        [JsonIgnore]
        [DbField("recipeGuid", (int)MySqlDbType.Binary, IsCriteria = true, Update = false)]
        public byte[] RecipeGuid_Binder
        {
            get { return RecipeGuid.Value.ToByteArray(); }
            set { RecipeGuid = new Guid(value); }
        }

        /// <summary>
        /// This is a mocked property that will set a flag in the database any time this object is altered.  this flag
        /// will allow us to detect objects that have changed post-installation and generate changesetss
        /// </summary>
        [JsonIgnore]
        [DbField("userModified", (int)MySqlDbType.Bit)]
        public virtual bool UserModified
        {
            get { return true; }
            set { } // method intentionally not implemented
        }

        /// <summary>
        /// enum RecipeType
        /// </summary>
        [JsonProperty("recipeType")]
        [DbField("recipeType", (int)MySqlDbType.Byte)]
        public byte RecipeType { get; set; }

        [JsonProperty("sourceWcid")]
        [DbField("sourceWcid", (int)MySqlDbType.UInt32)]
        public uint? SourceWcid { get; set; }

        [JsonProperty("targetWcid")]
        [DbField("targetWcid", (int)MySqlDbType.UInt32)]
        public uint? TargetWcid { get; set; }

        [JsonProperty("skillId")]
        [DbField("skillId", (int)MySqlDbType.UInt16)]
        public ushort? SkillId { get; set; }

        [JsonProperty("skillDifficulty")]
        [DbField("skillDifficulty", (int)MySqlDbType.UInt16)]
        public ushort? SkillDifficulty { get; set; }

        [JsonProperty("successMessage")]
        [DbField("successMessage", (int)MySqlDbType.Text)]
        public string SuccessMessage { get; set; }

        [JsonProperty("failMessage")]
        [DbField("failMessage", (int)MySqlDbType.Text)]
        public string FailMessage { get; set; }

        /// <summary>
        /// used by dyeing for the alt-colors
        /// </summary>
        [JsonProperty("alternateMessage")]
        [DbField("alternateMessage", (int)MySqlDbType.Text)]
        public string AlternateMessage { get; set; }

        /// <summary>
        /// enum RecipeResult
        /// </summary>
        [JsonProperty("resultFlags")]
        [DbField("resultFlags", (int)MySqlDbType.UInt32)]
        public uint? ResultFlags { get; set; }

        [JsonProperty("successItem1Wcid")]
        [DbField("successItem1Wcid", (int)MySqlDbType.UInt32)]
        public uint? SuccessItem1Wcid { get; set; }

        [JsonProperty("successItem1Quantity")]
        [DbField("successItem1Qty", (int)MySqlDbType.UInt32)]
        public uint? SuccessItem1Quantity { get; set; }

        [JsonProperty("successItem2Wcid")]
        [DbField("successItem2Wcid", (int)MySqlDbType.UInt32)]
        public uint? SuccessItem2Wcid { get; set; }

        [JsonProperty("successItem2Quantity")]
        [DbField("successItem2Qty", (int)MySqlDbType.UInt32)]
        public uint? SuccessItem2Quantity { get; set; }

        [JsonProperty("failureItem1Wcid")]
        [DbField("failureItem1Wcid", (int)MySqlDbType.UInt32)]
        public uint? FailureItem1Wcid { get; set; }

        [JsonProperty("failureItem1Quantity")]
        [DbField("failureItem1Qty", (int)MySqlDbType.UInt32)]
        public uint? FailureItem1Quantity { get; set; }

        [JsonProperty("failureItem2Wcid")]
        [DbField("failureItem2Wcid", (int)MySqlDbType.UInt32)]
        public uint? FailureItem2Wcid { get; set; }

        [JsonProperty("failureItem2Quantity")]
        [DbField("failureItem2Qty", (int)MySqlDbType.UInt32)]
        public uint? FailureItem2Quantity { get; set; }

        /// <summary>
        /// enum source: DerethForever.Entity.Enum.Properties.PropertyAttribute
        /// </summary>
        [JsonProperty("healingAttribute")]
        [DbField("healingAttribute", (int)MySqlDbType.UInt16)]
        public ushort? HealingAttribute { get; set; }

        private DateTime? _lastModified;

        [JsonProperty("lastModified")]
        public DateTime? LastModified
        {
            get { return _lastModified; }
            set
            {
                _lastModified = value;
                IsDirty = true;
            }
        }

        [JsonIgnore]
        [DbField("lastModifiedDate", (int)MySqlDbType.Int64)]
        public long? LastModified_Binder
        {
            get { return LastModified == null ? (long?)null : LastModified.Value.Ticks; }
            set { LastModified = value == null ? (DateTime?)null : new DateTime(value.Value); }
        }

        [JsonProperty("modifiedBy")]
        [DbField("modifiedBy", (int)MySqlDbType.Text)]
        public string ModifiedBy { get; set; }

        [JsonIgnore]
        public bool IsDirty { get; set; }

        [JsonProperty("changelog")]
        public List<ChangelogEntry> Changelog { get; set; } = new List<ChangelogEntry>();

        [JsonProperty("comments")]
        public string Comments { get; set; }

        /// <summary>
        /// flag to indicate whether or not this instance came from the database
        /// or was created by the game engine.  use case: when calling "SaveObject"
        /// in the database, we need to know whether to insert or update.  There's
        /// really no other way to tell at present.
        /// </summary>
        [JsonIgnore]
        public bool HasEverBeenSavedToDatabase { get; set; } = false;

        public virtual void ClearDirtyFlags()
        {
            this.IsDirty = false;
            this.HasEverBeenSavedToDatabase = true;
        }

        public virtual void SetDirtyFlags()
        {
            this.IsDirty = true;
            this.HasEverBeenSavedToDatabase = false;
        }
    }
}
