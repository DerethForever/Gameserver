/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;
using DerethForever.Common;
using DerethForever.Entity.Enum;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;

namespace DerethForever.Entity
{
    [DbTable("emote_set")]
    public class EmoteSet
    {
        [JsonProperty("emoteSetGuid")]
        public Guid? EmoteSetGuid { get; set; }

        [JsonIgnore]
        [DbField("emoteSetGuid", (int)MySqlDbType.Binary, Update = false, IsCriteria = true)]
        public byte[] EmoteSetGuid_Binder
        {
            get { return EmoteSetGuid.Value.ToByteArray(); }
            set { EmoteSetGuid = new Guid(value); }
        }

        [JsonProperty("weenieClassId")]
        [DbField("weenieClassId", (int)MySqlDbType.UInt32,  ListGet = true, ListDelete = true)]
        public uint WeenieClassId { get; set; }

        [JsonProperty("sortOrder")]
        [DbField("sortOrder", (int)MySqlDbType.UInt32)]
        public uint SortOrder { get; set; }

        [JsonIgnore]
        public EmoteCategory Category
        {
            get { return (EmoteCategory)EmoteCategoryId; }
            set { EmoteCategoryId = (uint)value; }
        }

        [JsonProperty("emoteCategoryId")]
        [DbField("emoteCategoryId", (int)MySqlDbType.UInt32)]
        public uint EmoteCategoryId { get; set; }

        [JsonProperty("probability")]
        [DbField("probability", (int)MySqlDbType.Double)]
        public double Probability { get; set; }

        [JsonProperty("emotes")]
        public List<Emote> Emotes { get; set; } = new List<Emote>();

        /// <summary>
        /// Used for Categories Refuse == 1, Give == 6
        /// </summary>
        [JsonProperty("classId")]
        [DbField("classId", (int)MySqlDbType.UInt32)]
        public uint? ClassId { get; set; }

        /// <summary>
        /// Used for Category HeartBeat == 5
        /// </summary>
        [JsonProperty("style")]
        [DbField("style", (int)MySqlDbType.UInt32)]
        public uint? Style { get; set; }

        /// <summary>
        /// Used for Category HeartBeat == 5
        /// </summary>
        [JsonProperty("subStyle")]
        [DbField("subStyle", (int)MySqlDbType.UInt32)]
        public uint? SubStyle { get; set; }

        /// <summary>
        /// Used for Categories QuestSuccess, QuestFailure, TestSuccess, TestFailure, EventSuccess, EventFailure, TestNoQuality, QuestNoFellow, TestNoFellow,
        /// GotoSet, NumFellowsSuccess, NumFellowsFailure, NumCharacterTitlesSuccess, NumCharacterTitlesFailure, ReceiveLocalSignal, ReceiveTalkDirect
        /// </summary>
        [JsonProperty("quest")]
        [DbField("quest", (int)MySqlDbType.VarChar)]
        public string Quest { get; set; }

        /// <summary>
        /// used for Category == Vendor
        /// </summary>
        [JsonProperty("vendorType")]
        [DbField("vendorType", (int)MySqlDbType.UInt32)]
        public uint? VendorType { get; set; }

        /// <summary>
        /// used for Category == WoundedTaunt
        /// </summary>
        [JsonProperty("minHealth")]
        [DbField("minHealth", (int)MySqlDbType.Float)]
        public float? MinHealth { get; set; }

        /// <summary>
        /// used for Category == WoundedTaunt
        /// </summary>
        [JsonProperty("maxHealth")]
        [DbField("maxHealth", (int)MySqlDbType.Float)]
        public float? MaxHealth { get; set; }
    }
}
