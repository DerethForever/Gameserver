﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;

using DerethForever.Entity.Enum;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DerethForever.Entity
{
    public class SearchWeeniesCriteria
    {
        public SearchWeeniesCriteria()
        {
        }

        [JsonProperty("partialName")]
        public string PartialName { get; set; }

        [JsonProperty("weenieClassId")]
        public uint? WeenieClassId { get; set; }

        [JsonProperty("weenieType")]
        [JsonConverter(typeof(StringEnumConverter))]
        public WeenieType? WeenieType { get; set; }

        [JsonProperty("itemType")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ItemType? ItemType { get; set; }

        [JsonProperty("contentGuid")]
        public Guid? ContentGuid { get; set; }

        [JsonProperty("userModified")]
        public bool? UserModified { get; set; }

        [JsonProperty("criteria")]
        public List<SearchWeenieProperty> PropertyCriteria { get; set; } = new List<SearchWeenieProperty>();
    }
}
