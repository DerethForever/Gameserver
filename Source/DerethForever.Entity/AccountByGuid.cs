/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.Common;
using MySql.Data.MySqlClient;

namespace DerethForever.Entity
{
    [DbTable("vw_account_by_name")]
    public class AccountByGuid
    {
        /// <summary>
        /// creates a new account object and pre-creates a new, random salt
        /// </summary>
        public AccountByGuid()
        {
        }

        [DbField("accountId", (int)MySqlDbType.UInt32, Insert = false, Update = false)]
        public uint AccountId { get; set; }

        public Guid AccountGuid { get; set; }

        [DbField("accountGuid", (int)MySqlDbType.Binary, IsCriteria = true, Update = false)]
        public byte[] AccountGuid_Binder
        {
            get { return AccountGuid.ToByteArray(); }
            set { AccountGuid = new Guid(value); }
        }
    }
}
