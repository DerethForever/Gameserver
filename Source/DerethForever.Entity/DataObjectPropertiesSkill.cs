﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using DerethForever.Common;
using DerethForever.Entity.Enum;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DerethForever.Entity
{
    [DbTable("ace_object_properties_skill")]
    public class DataObjectPropertiesSkill : BaseDataProperty, ICloneable
    {
        private uint _xpSpent = 0;
        private ushort _skillPoints = 0;
        private ushort _skillStatus = 0;

        [JsonIgnore]
        [DbField("skillId", (int)MySqlDbType.UInt16, IsCriteria = true, Update = false)]
        public ushort SkillId { get; set; }

        /// <summary>
        /// typed property for exposure in the API
        /// </summary>
        [JsonProperty("skill")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Skill Skill_Typed
        {
            get { return (Skill)SkillId; }
            set { SkillId = (ushort)value; }
        }

        [JsonIgnore]
        [DbField("skillStatus", (int)MySqlDbType.UInt16)]
        public ushort SkillStatus
        {
            get
            {
                return _skillStatus;
            }
            set
            {
                _skillStatus = value;
                IsDirty = true;
            }
        }

        /// <summary>
        /// typed property for exposure in the API
        /// </summary>
        [JsonProperty("skillStatus")]
        [JsonConverter(typeof(StringEnumConverter))]
        public SkillStatus SkillStatus_Typed
        {
            get { return (SkillStatus)_skillStatus; }
            set { _skillStatus = (ushort)value; }
        }

        [JsonProperty("ranks")]
        [DbField("skillPoints", (int)MySqlDbType.UInt16)]
        public ushort SkillPoints
        {
            get
            {
                return _skillPoints;
            }
            set
            {
                _skillPoints = value;
                IsDirty = true;
            }
        }

        [JsonProperty("experienceSpent")]
        [DbField("skillXpSpent", (int)MySqlDbType.UInt32)]
        public uint SkillXpSpent
        {
            get
            {
                return _xpSpent;
            }
            set
            {
                _xpSpent = value;
                IsDirty = true;
            }
        }
        
        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
