/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Entity.Enum
{
    public enum HumanParts
    {
        Abdomen       = 0,
        LeftUpperLeg  = 1,
        LeftLowerLeg  = 2,
        LeftFoot      = 3,
        LeftToe       = 4,
        RightUpperLeg = 5,
        RightLowerLeg = 6,
        RightFoot     = 7,
        RightToe      = 8,
        Chest         = 9,
        LeftUpperArm  = 10,
        LeftLowerArm  = 11,
        LeftHand      = 12,
        RightUpperArm = 13,
        RightLowerArm = 14,
        RightHand     = 15,
        Head          = 16,
        TailSeg1      = 17,
        TailSeg2      = 18,
        TailSeg3      = 19,
        TailSeg4      = 20,
        HeadHair      = 21,
        HeadHelmet    = 22,
        ShoulderLeft  = 23,
        ShoulderRight = 24,
        KneeLeft      = 25,
        KneeRight     = 26,
        ElbowLeft     = 27,
        ElbowRight    = 28,
        CloakSeg1     = 29,
        CloakSeg2     = 30,
        CloakSeg3     = 31,
        CloakSeg4     = 32,
        CloakSeg5     = 33,
    }
}
