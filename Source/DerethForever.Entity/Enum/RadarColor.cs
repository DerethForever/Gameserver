﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Entity.Enum
{
    public enum RadarColor : byte
    {
        Default             = 0x00,
        Blue                = 0x01,
        Gold                = 0x02,
        White               = 0x03,
        Purple              = 0x04,
        Red                 = 0x05,
        Pink                = 0x06,
        Green               = 0x07,
        Yellow              = 0x08,
        Cyan                = 0x09,
        BrightGreen         = 0x10,
        Admin               = Cyan,
        Advocate            = Pink,
        Creature            = Gold,
        LifeStone           = Blue,
        NPC                 = Yellow,
        PlayerKiller        = Red,
        Portal              = Purple,
        Sentinel            = Cyan,
        Vendor              = Yellow,
        Fellowship          = BrightGreen,
        FellowshipLeader    = BrightGreen,
        PKLite              = Pink
    }
}
