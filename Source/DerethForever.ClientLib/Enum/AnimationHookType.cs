/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
namespace DerethForever.ClientLib.Enum
{
    public enum AnimationHookType
    {
        Unknown                 = -1,
        NoOp                    = 0,
        Sound                   = 1,
        SoundTable              = 2,
        Attack                  = 3,
        AnimationDone           = 4,
        ReplaceObject           = 5,
        Ethereal                = 6,
        TransparentPart         = 7,
        Luminous                = 8,
        LuminousPart            = 9,
        Diffuse                 = 10,
        DiffusePart             = 11,
        Scale                   = 12,
        CreateParticle          = 13,
        DestroyParticle         = 14,
        StopParticle            = 15,
        NoDraw                  = 16,
        DefaultScript           = 17,
        DefaultScriptPart       = 18,
        CallPES                 = 19, // Particle Emitter System
        Transparent             = 20,
        SoundTweaked            = 21,
        SetOmega                = 22,
        TextureVelocity         = 23,
        TextureVelocityPart     = 24,
        SetLight                = 25,
        CreateBlockingParticle  = 26,
        ForceAnimationHook32Bit = 2147483647
    }
}
