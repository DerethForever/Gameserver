/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib
{
    /// <summary>
    /// Psuedo-random number generator
    /// </summary>
    public static class Prng
    {
        /// <summary>
        /// this function doesn't exist in the client, but rather is inlined all over the place.  This
        /// is probably a compiler optimization.  Seed values are set as constants in the Seeds subclass.
        /// 
        /// examples: acclient.c 352640, 352664
        /// </summary>
        public static double Generate(uint a, uint b, uint seed)
        {
            //          1813693831       1109124029               1360117743           1888038839
            uint ival = 0x6C1AC587 * b - 0x421BE3BD * a - seed * (0x5111BFEF * b * a + 0x70892FB7);

            return (ival / (double)uint.MaxValue);
        }

        public static double NeSwCut(uint a, uint b)
        {
            uint val = b * (214614067 * a + 1813693831) - 1109124029 * a - 1369149221;

            return (val / (double)uint.MaxValue);
        }

        public static class Seeds
        {
            public const uint InvertCellTriangles = 0x00000003;

            public const uint SceneSubIndex = 0x00002bf9;

            public const uint Frequency = 0x00005b67;

            public const uint DisplacementX = 0x0000b2cd;

            public const uint DisplacementY = 0x00011c0f;

            public const uint ObjectScale = 0x00007f51;

            public const uint AltObjectScale = 0x000096a7;

            public const uint SceneRotation = 0x0000e7eb;
            
            public const uint ObjectRotation = 0x0000f697;
            
            public const uint LandblockRotation = 0x0000000d;

            public const uint RND_LAND_TEX = 0x00000011;
            public const uint RND_LAND_ALPHA2 = 0x00012071;
            public const uint RND_LAND_ALPHA1 = 0x000002db;
            public const uint RND_SKILL_APPRAISE = 0x00000013;
            public const uint RND_TIME_WEATH1 = 0x0000a883;
            public const uint RND_TIME_WEATH2 = 0x00013255;
        }
    }
}
