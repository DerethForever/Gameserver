/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CRegionDesc in the client
    /// </summary>
    public class RegionDescription : DatabaseObject
    {
        /// <summary>
        /// +0x038 region_number    : Uint4B
        /// </summary>
        public uint RegionNumber;

        /// <summary>
        /// +0x03c region_name      : AC1Legacy::PStringBase(char)
        /// </summary>
        public string RegionName;

        /// <summary>
        /// +0x040 version          : Uint4B
        /// </summary>
        public uint Version;

        /// <summary>
        /// +0x044 minimize_pal     : Int4B
        /// </summary>
        public int MinimizePalette;

        /// <summary>
        /// +0x048 parts_mask       : Uint4B
        /// </summary>
        public RegionPartsMask PartsMask;

        /// <summary>
        /// client source uknown
        /// </summary>
        public uint FileId { get; set; }

        /// <summary>
        /// +0x04c file_info        : Ptr32 FileNameDesc
        /// </summary>
        public FileNameDescription FileInfo;

        /// <summary>
        /// +0x050 sky_info         : Ptr32 SkyDesc
        /// </summary>
        public SkyDescription SkyInfo;

        /// <summary>
        /// +0x054 sound_info       : Ptr32 CSoundDesc
        /// </summary>
        public SoundDescription SoundInfo;

        /// <summary>
        /// +0x058 scene_info       : Ptr32 CSceneDesc
        /// </summary>
        public SceneDescription SceneInfo;

        /// <summary>
        /// +0x05c terrain_info     : Ptr32 CTerrainDesc
        /// </summary>
        public TerrainDescription TerrainInfo { get; set; }

        /// <summary>
        /// +0x060 encounter_info   : Ptr32 CEncounterDesc
        /// </summary>
        public EncounterDescription EncounterInfo;

        /// <summary>
        /// +0x064 water_info       : Ptr32 WaterDesc
        /// </summary>
        public WaterDescription WaterInfo;

        /// <summary>
        /// +0x068 fog_info         : Ptr32 FogDesc
        /// </summary>
        public FogDescription FogInfo;

        /// <summary>
        /// +0x06c dist_fog_info    : Ptr32 DistanceFogDesc
        /// </summary>
        public DistanceFogDescription DistanceFogInfo;

        /// <summary>
        /// +0x070 region_map_info  : Ptr32 RegionMapDesc
        /// </summary>
        public RegionMapDescription RegionMapInfo;

        /// <summary>
        /// +0x074 region_misc      : Ptr32 RegionMisc
        /// </summary>
        public RegionMiscellaneous RegionMiscellaneous;
        
        /// <summary>
        /// //----- (004FF440) --------------------------------------------------------
        /// void **__thiscall CRegionDesc::UnPack(CRegionDesc *this, void **addr, unsigned int size)
        /// acclient.c 299776
        /// </summary>
        public static RegionDescription Unpack(BinaryReader reader)
        {
            RegionDescription reg = new RegionDescription();
            reg.FileId = reader.ReadUInt32();
            reg.RegionNumber = reader.ReadUInt32();
            reg.Version = reader.ReadUInt32();
            reg.RegionName = reader.ReadPString();
            
            // next 8 dwords (32 bytes) are (sorta) throw-away information.  they contain static information
            // that we presume to be constant for our purposes, so we're going to skip over them.
            var garbage = reader.ReadBytes(32);

            // not going to double buffer this, just assign to the static collection directly
            for (int i = 0; i < 256; i++)
                LandDefs.LandHeightTable[i] = reader.ReadSingle();
            
            GameTime.CurrentGameTime = GameTime.Unpack(reader);

            // client loads this into m_pNext, but it's clearly a mask for what elements to read next
            // plus, mp_Next is part of the base database object linked list structure.  probably decompiler
            // issues, as this function is littered with them.
            reg.PartsMask = (RegionPartsMask)reader.ReadUInt32();

            if (reg.PartsMask.HasFlag(RegionPartsMask.Sky))
                reg.SkyInfo = SkyDescription.Unpack(reader);

            if (reg.PartsMask.HasFlag(RegionPartsMask.Sound))
                reg.SoundInfo = SoundDescription.Unpack(reader);

            if (reg.PartsMask.HasFlag(RegionPartsMask.Scene))
            {
                reg.SceneInfo = new SceneDescription();

                uint numSceneTypes = reader.ReadUInt32();
                for (uint i = 0; i < numSceneTypes; i++)
                {
                    SceneType st = new SceneType();
                    int soundIndex = reader.ReadInt32();

                    if (soundIndex != -1)
                        st.AmbientSoundTable = reg.SoundInfo.SoundTables[soundIndex];

                    uint numScenes = reader.ReadUInt32();
                    for (int j = 0; j < numScenes; j++)
                        st.Scenes.Add(reader.ReadUInt32());

                    reg.SceneInfo.SceneTypes.Add(st);
                }
            }
            
            reg.TerrainInfo = new TerrainDescription();

            uint numTerrains = reader.ReadUInt32();
            for (int i = 0; i < numTerrains; i++)
            {
                TerrainType tt = new TerrainType();
                tt.TerrainName = reader.ReadPString();
                tt.TerrainColor = reader.ReadUInt32();

                uint numSceneTypes = reader.ReadUInt32();
                for (int j = 0; j < numSceneTypes; j++)
                {
                    int sceneIndex = reader.ReadInt32();
                    if (sceneIndex != -1)
                        tt.SceneTypes.Add(reg.SceneInfo.SceneTypes[sceneIndex]);
                }

                reg.TerrainInfo.TerrainTypes.Add(tt);
            }

            reg.TerrainInfo.LandSurface = LandSurface.Unpack(reader);

            if (reg.PartsMask.HasFlag(RegionPartsMask.Misc))
                reg.RegionMiscellaneous = RegionMiscellaneous.Unpack(reader);

            return reg;
        }
    }
}
