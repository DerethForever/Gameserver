/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Numerics;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// not exactly a class in the client, but more of a collection of static data
    /// </summary>
    public class LandDefs
    {
        static LandDefs()
        {
            // default the LandHeightTable.  will be overridden when Region data is loaded
            for (int i = 0; i < 256; i++)
                LandHeightTable[i] = i * 2;
        }

        public static float[] LandHeightTable { get; set; } = new float[256];

        /// <summary>
        /// LandDefs::Direction in the client.  values extracted by Iron Golem.
        /// Values are radian header directions but in no logical order or we'd just math it.
        ///
        /// What's significant here is that the values indicate clockwise heading progression, which
        /// violates the right-hand-rule of modern physics engines.  this explains a lot of the places
        /// in the client where Z (upwards) values get inverted otherwise inexplicably.
        /// </summary>
        public static double[] Direction = new double[]
        {
            0d,                 // "IN_VIEWER_BLOCK" in the client, but the functions that do the translations never differentiate 0 from 1.
            0d,                 // North
            Math.PI,            // South
            Math.PI * 0.5d,     // East
            Math.PI * 1.5d,     // West
            Math.PI * 1.75d,    // North-West
            Math.PI * 1.25d,    // South-West
            Math.PI * 0.25d,    // North-East
            Math.PI * 0.75d     // South-East
        };

        /*
          *_height = 255;
          *_width = 255;
          *_cell_size = 24.0;
          *_cells_per_block = 8;
          *_cell_sub_divide = 1;
          *_max_obj_height = 200.0;
          *_sky_height = 1000.0;
          *_road_width = 5.0;
         */
        public static int SideVertexCount    = 9;

        public static float HalfSquareLength = 12.0f;
        public static uint NumBlockLength    = 255;
        public static uint NumBlockWidth     = 255;
        public static float CellSize         = 24.0f;
        public static uint CellsPerBlock     = 8;
        public static uint CellSubDivide     = 1;
        public static float MaxiObjectHeight = 200.0f;
        public static float SkyHeight        = 1000.0f;
        public static float RoadWidth        = 5.0f;
        public static int BlockPartShift     = 16;
        public static uint SquareLength      = 24;

        /// <summary>
        /// //----- (005A9BC0) --------------------------------------------------------
        /// int __cdecl LandDefs::adjust_to_outside(unsigned int *cell_id, AC1Legacy::Vector3 *loc)
        /// acclient.c 467434
        /// </summary>
        public static bool AdjustToOutside(ref uint cellId, ref Vector3 loc)
        {
            uint lower = cellId & 0xFFFF;

            if ((lower < 1 || lower > 0x40) && (lower < 0x100 || lower > 0xFFFD) && lower != 0xFFFF)
                return false;

            if (Math.Abs(loc.X) < Constants.TOLERANCE)
                loc.X = 0f;

            if (Math.Abs(loc.Y) < Constants.TOLERANCE)
                loc.Y = 0f;

            int x, y;

            if (GetOutsideLCoord(cellId, loc, out x, out y))
            {
                cellId = LCoordToGid(x, y);
                loc.X -= (float)(Math.Floor(loc.X / 192f) * 192f);
                loc.Y -= (float)(Math.Floor(loc.Y / 192f) * 192f);
                return true;
            }

            return false;
        }

        /// <summary>
        /// //----- (005A9B00) --------------------------------------------------------
        /// int __cdecl LandDefs::get_outside_lcoord(unsigned int cell_id, AC1Legacy::Vector3 *loc, int *x, int *y)
        /// acclient.c 467411
        /// </summary>
        public static bool GetOutsideLCoord(uint cellId, Vector3 loc, out int x, out int y)
        {
            x = 0;
            y = 0;

            uint lower = cellId & 0xFFFF;

            if ((lower < 1 || lower > 0x40) && (lower < 0x100 || lower > 0xFFFD) && lower != 0xFFFF)
                return false;

            BlockIdToLCoord(cellId, out x, out y);
            x += (int)Math.Floor(loc.X / 24);
            y += (int)Math.Floor(loc.Y / 24);

            return !(x < 0 || y < 0 || x >= 0x7F8 || y >= 0x7F8);
        }

        /// <summary>
        /// warning: x is logically left shifted 3 coming out of this (block #3 comes out as 0x18)
        /// //----- (0043D680) --------------------------------------------------------
        /// int __cdecl LandDefs::blockid_to_lcoord(unsigned int block_id, int *x, int *y)
        /// acclient.c 122260
        /// </summary>
        public static bool BlockIdToLCoord(uint blockid, out int x, out int y)
        {
            if (blockid == 0)
            {
                x = 0;
                y = 0;
                return false;
            }

            x = (int)(((blockid >> 16) & 0xFF00) >> 8 << 3);
            y = (int)((blockid >> 16) & 0x00FF) << 3;

            return !(x < 0 || y < 0 || x >= 0x7F8 || y >= 0x7F8);
        }

        /// <summary>
        /// //----- (004A19A0) --------------------------------------------------------
        /// unsigned int __cdecl LandDefs::lcoord_to_gid(int x, int y)
        /// acclient.c 218182
        /// </summary>
        public static uint LCoordToGid(int x, int y)
        {
            if (x < 0 || y < 0 || x >= 0x7F8 || y >= 0x7F8)
                return 0;

            // client is 1 giant inline bitwise mess here
            int block = ((x >> 3 << 8) | (y >> 3)) << 16;
            int cell = 1 + ((x & 0x7) << 3) + (y & 0x7);
            return (uint)block | (uint)cell;
        }

        /// <summary>
        /// //----- (00497A90) --------------------------------------------------------
        /// int __cdecl LandDefs::gid_to_lcoord(unsigned int cell_id, int *x, int *y)
        /// acclient.c 209521
        /// </summary>
        public static bool GidToLCoord(uint cellId, ref int x, ref int y)
        {
            ushort cellShort = (ushort)(cellId & 0xFFFF);

            if (InboundValidCellId(cellId))
            {
                if (cellShort < 0x100)
                {
                    x = (int)(cellId >> 21) & 0x7F8;
                    x += (cellShort - 1) >> 3;

                    y = ((int)(cellId >> 16) & 0xFF) << 3;
                    y += ((cellShort - 1) & 7);

                    return x >= 0 && y >= 0 && x < 0x7F8 && y < 0x7F8;
                }
            }

            return false;
        }

        /// <summary>
        /// //----- (004979A0) --------------------------------------------------------
        /// int __cdecl LandDefs::inbound_valid_cellid(unsigned int cell_id)
        /// acclient.c 209468
        /// </summary>
        public static bool InboundValidCellId(uint cellId)
        {
            ushort cellShort = (ushort)(cellId & 0xFFFF);
            if ((cellShort >= 1 && cellShort <= 0x40)
                || (cellShort >= 0x100 && cellShort < 0xFFFD)
                || cellShort == 0xFFFF)
            {
                uint x = (cellId >> 21) & 0x78F;
                uint y = (cellId >> 13) & 0x78F;

                return (x < 0x78F && y < 0x78F);
            }

            return false;
        }

        /// <summary>
        /// //----- (0050E800) --------------------------------------------------------
        /// int __cdecl LandDefs::within_block(AC1Legacy::Vector3 *vc, float radius)
        /// acclient.c 315583
        /// </summary>
        public static bool IsInBlock(Vector3 loc, float radius)
        {
            float max = 192.0f - radius;
            return !(loc.X < radius || loc.Y < radius || loc.X > max || loc.Y > max);
        }

        /// <summary>
        /// ----- (0043E630) --------------------------------------------------------
        /// AC1Legacy::Vector3* __cdecl LandDefs::get_block_offset(AC1Legacy::Vector3* result, unsigned int cell_from, unsigned int cell_to)
        /// acclient.c 123110
        /// </summary>
        public static Vector3 GetBlockOffset(uint cellFrom, uint cellTo)
        {
            if (cellFrom >> BlockPartShift == cellTo >> BlockPartShift)
                return Vector3.Zero;

            BlockIdToLCoord(cellFrom, out int fromX, out int fromY);
            BlockIdToLCoord(cellFrom, out int toX, out int toY);

            return new Vector3((toX - fromX) * SquareLength, (toY - fromY) * SquareLength, 0.0f);
        }
    }
}
