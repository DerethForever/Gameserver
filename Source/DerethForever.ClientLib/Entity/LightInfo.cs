/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    public class LightInfo
    {
        /// <summary>
        ///  +0x000 type             : Int4B
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// +0x044 viewerspace_location : AC1Legacy::Vector3
        /// </summary>
        public Frame ViewerspaceLocation { get; set; }

        /// <summary>
        /// +0x050 color            : RGBColor
        /// </summary>
        public uint Color { get; set; } // _RGB Color. Red is bytes 3-4, Green is bytes 5-6, Blue is bytes 7-8. Bytes 1-2 are always FF (?)

        /// <summary>
        /// +0x060 falloff          : Float
        /// </summary>
        public float Intensity { get; set; }

        /// <summary>
        /// +0x060 falloff          : Float
        /// </summary>
        public float Falloff { get; set; }

        /// <summary>
        /// +0x064 cone_angle       : Float
        /// </summary>
        public float ConeAngle { get; set; }

        public static LightInfo Unpack(BinaryReader reader)
        {
            LightInfo obj = new LightInfo();

            obj.Type = reader.ReadInt32();
            obj.ViewerspaceLocation = Frame.Unpack(reader);
            obj.Color = reader.ReadUInt32();
            obj.Intensity = reader.ReadSingle();
            obj.Falloff = reader.ReadSingle();
            obj.ConeAngle = reader.ReadSingle();
            return obj;
        }
    }
}
