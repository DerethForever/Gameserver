/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.Numerics;
using DerethForever.ClientLib.DatUtil;
using DerethForever.ClientLib.Enum;
using DerethForever.ClientLib.Managers;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CPartArray in the client
    /// </summary>
    public class PartArray
    {
        /// <summary>
        /// 0x000 pa_state : Uint4B
        /// </summary>
        public PhysicsState State { get; set; }

        /// <summary>
        /// 0x004 owner : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject Owner { get; set; }

        /// <summary>
        /// 0x008 sequence : CSequence
        /// </summary>
        public Sequence Sequence { get; set; } = new Sequence();

        /// <summary>
        /// 0x050 motion_table_manager : Ptr32 MotionTableManager
        /// </summary>
        public MotionTableManager MotionTableManager { get; set; }

        /// <summary>
        /// 0x054 setup : Ptr32 CSetup
        /// </summary>
        public SetupModel Setup { get; private set; }

        /// <summary>
        /// 0x058 num_parts : Uint4B
        /// </summary>
        public uint NumParts { get; set; }

        /// <summary>
        /// 0x05c parts : Ptr32 Ptr32 CPhysicsPart
        /// </summary>
        public List<PhysicsPart> Parts { get; set; } = new List<PhysicsPart>();

        /// <summary>
        /// 0x060 scale : AC1Legacy::Vector3
        /// </summary>
        public Vector3 Scale = Vector3.One;

        /// <summary>
        /// 0x06c pals : Ptr32 Ptr32 Palette
        /// </summary>
        public List<Palette> Palettes = new List<Palette>();

        /// <summary>
        /// 0x070 lights : Ptr32 LIGHTLIST
        /// </summary>
        public List<object> Lights = new List<object>();

        /// <summary>
        /// 0x074 last_animframe : Ptr32 AnimFrame
        /// </summary>
        public PhysicsAnimationFrame LastAnimationFrame;

        /// <summary>
        /// default sorting sphere for when there is none or there is no setup model
        /// </summary>
        private static Sphere DefaultSortingSphere = new Sphere(Vector3.Zero, 0f);

        /// <summary>
        /// ----- (00518060) --------------------------------------------------------
        /// unsigned int __thiscall CPartArray::GetNumSphere(CPartArray*this)
        /// acclient.c 325358
        /// </summary>
        public int GetNumSpheres()
        {
            return Setup.Spheres.Count;
        }

        /// <summary>
        /// //----- (00517DB0) --------------------------------------------------------
        /// void __thiscall CPartArray::Update(CPartArray *this, float quantum, Frame* offset_frame)
        /// acclient.c 325140
        /// </summary>
        public void Update(double quantum, Frame position)
        {
            Sequence.Update(quantum, position);
        }

        /// <summary>
        /// //----- (00519310) --------------------------------------------------------
        /// void __thiscall CPartArray::SetFrame(CPartArray *this, Frame *frame)
        /// acclient.c 326766
        /// </summary>
        public void SetFrame(Frame loc)
        {
            UpdateParts(loc);

            // update lights in client.  skip it for server.
        }

        /// <summary>
        /// //----- (005193D0) --------------------------------------------------------
        /// int __thiscall CPartArray::SetPlacementFrame(CPartArray *this, unsigned int placement_id)
        /// acclient.c 326818
        /// </summary>
        public bool SetPlacementFrame(Placement placement)
        {
            if (Setup.PlacementFrames.ContainsKey(placement))
            {
                var frame = Setup.PlacementFrames[placement];
                Sequence.SetPlacementFrame(frame.AnimFrame, placement);
                return true;
            }

            Sequence.SetPlacementFrame(null, Placement.Default);
            return false;
        }

        /// <summary>
        /// //----- (005190F0) --------------------------------------------------------
        /// void __thiscall CPartArray::UpdateParts(CPartArray *this, Frame *frame)
        /// acclient.c 326601
        /// </summary>
        public void UpdateParts(Frame frame)
        {
            var animFrame = Sequence.GetCurrentAnimationFrame();

            if (animFrame != null)
            {
                for (int i = 0; i < Parts.Count; i++)
                {
                    Parts[i].Position.Frame = Frame.Combine(frame, animFrame.Frames[i], Scale);
                }
            }
        }

        /// <summary>
        /// //----- (00517D60) --------------------------------------------------------
        /// void __thiscall CPartArray::HandleMovement(CPartArray *this)
        /// acclient.c 325106
        /// </summary>
        public void HandleMovement()
        {
            MotionTableManager.UseTime();
        }

        /// <summary>
        /// //----- (00518070) --------------------------------------------------------
        /// CSphere* __thiscall CPartArray::GetSphere(CPartArray*this)
        /// acclient.c 325364
        /// </summary>
        public List<Sphere> GetSpheres()
        {
            return Setup.Spheres;
        }

        /// <summary>
        /// //----- (005180C0) --------------------------------------------------------
        /// int __thiscall CPartArray::AllowsFreeHeading(CPartArray *this)
        /// acclient.c 325394
        /// </summary>
        public bool AllowsFreeHeading()
        {
            return Setup.Bitfield.HasFlag(SetupModelFlags.AllowFreeHeading);
        }

        /// <summary>
        /// //----- (005180D0) --------------------------------------------------------
        /// double __thiscall CPartArray::GetStepUpHeight(CPartArray *this)
        /// acclient.c 325400
        /// </summary>
        public float GetStepUpHeight()
        {
            return (Setup?.StepUpHeight ?? 0) * Scale.Z;
        }

        /// <summary>
        /// //----- (005180F0) --------------------------------------------------------
        /// double __thiscall CPartArray::GetStepDownHeight(CPartArray*this)
        /// acclient.c 325414
        /// </summary>
        public float GetStepDownHeight()
        {
            return (Setup?.StepDownHeight ?? 0) * Scale.Z;
        }

        /// <summary>
        /// //----- (00519150) --------------------------------------------------------
        /// int __thiscall CPartArray::SetMeshID(CPartArray *this, IDClass mesh_did)
        /// acclient.c 326635
        /// </summary>
        public bool SetMeshId(uint meshDid)
        {
            if (meshDid == 0)
                return false;

            Setup = SetupModel.MakeSimpleSetup(meshDid);
            if (Setup == null)
                return false;

            InitializeParts();
            return true;
        }

        /// <summary>
        /// //----- (00519640) --------------------------------------------------------
        /// CPartArray *__cdecl CPartArray::CreateMesh(CPhysicsObj *_owner, IDClass setup_did)
        /// acclient.c 326984
        /// </summary>
        public static PartArray CreateMesh(PhysicsObject owner, uint setupDid)
        {
            PartArray pa = new PartArray();
            pa.Owner = owner;
            pa.Sequence.SetObject(owner);

            if (!pa.SetMeshId(setupDid))
                return null;

            pa.SetPlacementFrame(Placement.Resting);

            return pa;
        }

        /// <summary>
        /// //----- (005195A0) --------------------------------------------------------
        /// CPartArray *__cdecl CPartArray::CreateSetup(CPhysicsObj *_owner, IDClass setup_did, int bCreateParts)
        /// acclient.c 326947
        /// </summary>
        public static PartArray CreateSetup(PhysicsObject owner, uint setupId, bool createParts)
        {
            PartArray pa = new PartArray();
            pa.State = 0;
            pa.Owner = owner;
            pa.Sequence = new Sequence();
            pa.Sequence.SetObject(owner);

            if (!pa.SetSetupId(setupId, createParts))
                return null;

            pa.SetPlacementFrame(Placement.Resting);

            return pa;
        }

        /// <summary>
        /// //----- (00519330) --------------------------------------------------------
        /// int __thiscall CPartArray::SetSetupID(CPartArray *this, IDClass setup_id, int bCreateParts)
        /// acclient.c 326779
        /// </summary>
        private bool SetSetupId(uint setupId, bool createParts)
        {
            if (Setup != null && Setup.Id == setupId)
                return true;

            // clear dependent data
            Palettes = new List<Palette>();
            Parts = new List<PhysicsPart>();
            Lights = new List<object>();

            // fetch the info from the dat
            Setup = PortalDatReader.Current.Unpack<SetupModel>(setupId);

            if (Setup == null)
                return false;

            // initialize dependent data
            if (createParts)
            {
                InitializeParts();
            }

            InitializeLights();
            InitializeDefaults();

            return true;
        }

        /// <summary>
        /// //----- (00517F40) --------------------------------------------------------
        /// unsigned int __thiscall CPartArray::InitParts(CPartArray *this)
        /// acclient.c 325274
        /// </summary>
        private void InitializeParts()
        {
            Parts = new List<PhysicsPart>();

            Setup.PartIds.ForEach(pId => Parts.Add(PhysicsPart.MakePhysicsPart(pId)));

            for (int i = 0; i < Parts.Count; i++)
            {
                Parts[i].PhysicsObject = Owner;
                Parts[i].Index = i;

                if (i < Setup.DefaultScale.Count)
                    Parts[i].GraphicsObjectScale = Setup.DefaultScale[i];
            }
        }

        /// <summary>
        /// //----- (00518C00) --------------------------------------------------------
        /// int __thiscall CPartArray::InitLights(CPartArray *this)
        /// acclient.c 326321
        /// </summary>
        public void InitializeLights()
        {
            // probably don't care about lights for a server
        }

        /// <summary>
        /// //----- (00517DD0) --------------------------------------------------------
        /// void __thiscall CPartArray::SetCellID(CPartArray *this, const unsigned int cell_id)
        /// acclient.c 325146
        /// </summary>
        public void SetCellId(uint cellId)
        {
            Parts.ForEach(p => p.Position.CellId = cellId);
        }

        /// <summary>
        /// //----- (00518980) --------------------------------------------------------
        /// void __thiscall CPartArray::InitDefaults(CPartArray *this)
        /// acclient.c 326158
        /// </summary>
        private void InitializeDefaults()
        {
            if (Setup.DefaultAnimation != 0)
            {
                Sequence.ClearAnimations();
                AnimationData defaultAnim = new AnimationData();
                defaultAnim.AnimationId = Setup.DefaultAnimation;
                defaultAnim.LowFrame = 0;
                defaultAnim.HighFrame = -1;
                defaultAnim.FrameRate = 30.0f;
                Sequence.AppendAnimation(defaultAnim);
            }

            Owner?.InitializeDefaults(Setup);
        }

        /// <summary>
        /// //----- (00518340) --------------------------------------------------------
        /// void __thiscall CPartArray::SetTranslucencyInternal(CPartArray *this, float _translucency)
        /// acclient.c 325638
        /// </summary>
        internal void SetTranslucencyInternal(float value)
        {
            if (Setup != null)
                Parts?.ForEach(p => p.SetTranslucency(value));
        }

        /// <summary>
        /// //----- (00518110) --------------------------------------------------------
        /// int __thiscall CPartArray::CacheHasPhysicsBSP(CPartArray *this)
        /// acclient.c 325428
        /// </summary>
        internal bool CacheHasPhysicsBsp()
        {
            foreach (PhysicsPart p in Parts)
            {
                if (p.GraphicsObjects[0].PhysicsBsp == null)
                    continue;
                State |= PhysicsState.HasPhysicsBsp;
                return true;
            }

            State &= ~PhysicsState.HasPhysicsBsp;
            return false;
        }

        /// <summary>
        /// ----- (005187F0) --------------------------------------------------------
        /// signed int __thiscall CPartArray::StopInterpretedMotion(CPartArray*this, unsigned int motion, MovementParameters*params)
        /// acclient.c 326054
        /// </summary>
        public Sequence StopInterpretedMotion(MotionCommand motion, MovementParameters movementParameters)
        {
            if (MotionTableManager == null)
                return new Sequence(7);
            MovementStructure mvs = new MovementStructure(MovementTypes.StopInterpretedCommand);
            mvs.Motion = motion;
            mvs.Params = movementParameters;
            return MotionTableManager.PerformMovement(mvs, Sequence);
        }

        /// <summary>
        /// ----- (00517D70) --------------------------------------------------------
        /// void __thiscall CPartArray::HandleEnterWorld(CPartArray*this)
        /// acclient.c 325116
        /// </summary>
        public void HandleEnterWorld()
        {
            MotionTableManager?.HandleEnterWorld(Sequence);
        }

        /// <summary>
        /// ----- (00518750) --------------------------------------------------------
        /// signed int __thiscall CPartArray::DoInterpretedMotion(CPartArray*this, unsigned int motion, MovementParameters*params)
        /// acclient.c 326018
        /// </summary>
        public Sequence DoInterpretedMotion(MotionCommand motion, MovementParameters movementParameters)
        {
            return MotionTableManager == null ? new Sequence(7) : MotionTableManager.PerformMovement(new MovementStructure(MovementTypes.InterpretedCommand, motion, movementParameters), Sequence);
        }

        /// <summary>
        /// ----- (00518890) --------------------------------------------------------
        /// signed int __thiscall CPartArray::StopCompletelyInternal(CPartArray*this)
        /// acclient.c 326090
        /// </summary>
        public Sequence StopCompletelyInternal()
        {
            if (MotionTableManager == null)
                return new Sequence(7);
            MovementStructure movementStructure = new MovementStructure(MovementTypes.StopCompletely);
            return MotionTableManager.PerformMovement(movementStructure, Sequence);
        }

        /// <summary>
        /// ----- (00517D50) --------------------------------------------------------
        /// void __thiscall CPartArray::CheckForCompletedMotions(CPartArray*this)
        /// acclient.c 325096
        /// </summary>
        public void CheckForCompletedMotions()
        {
            MotionTableManager?.CheckForCompletedMotions();
        }

        /// <summary>
        /// //----- (00518B00) --------------------------------------------------------
        /// CSphere *__thiscall CPartArray::GetSortingSphere(CPartArray *this)
        /// acclient.c 326268
        /// </summary>
        public Sphere GetSortingSphere()
        {
            if (Setup != null)
                return Setup.SortingSphere;

            return DefaultSortingSphere;
        }

        /// <summary>
        /// //----- (00518080) --------------------------------------------------------
        /// unsigned int __thiscall CPartArray::GetNumCylsphere(CPartArray *this)
        /// acclient.c 325370
        /// </summary>
        public uint GetNumCylinderSpheres()
        {
            return Setup?.NumClySpheres ?? 0;
        }

        /// <summary>
        /// //----- (00518090) --------------------------------------------------------
        /// CCylSphere *__thiscall CPartArray::GetCylsphere(CPartArray *this)
        /// acclient.c 325376
        /// </summary>
        public List<CylinderSphere> GetCylinderSpheres()
        {
            return Setup?.CylSpheres;
        }

        /// <summary>
        /// //----- (00518A00) --------------------------------------------------------
        /// int __thiscall CPartArray::SetScaleInternal(CPartArray *this, AC1Legacy::Vector3 *new_scale)
        /// acclient.c 326182
        /// </summary>
        public void SetScaleInternal(Vector3 scaleVector)
        {
            Scale = scaleVector;

            for (int i = 0; i < NumParts; i++)
            {
                PhysicsPart part = Parts[i];
                Vector3 defaultScale = new Vector3(1.0f, 1.0f, 0.0f); // things don't scale on Z by default apparently (line 326223)

                if (Setup.DefaultScale?.Count > 0)
                    defaultScale = Setup.DefaultScale[i];

                defaultScale = defaultScale * scaleVector;
                part.GraphicsObjectScale = defaultScale;
            }
        }

        /// <summary>
        /// ----- (00517D10) --------------------------------------------------------
        /// void __thiscall CPartArray::InitializeMotionTables(CPartArray*this)
        /// accilent.c 325068
        /// </summary>
        public void InitializeMotionTables()
        {
            if (MotionTableManager != null)
                MotionTableManager.InitializeState(Sequence);
        }
    }
}
