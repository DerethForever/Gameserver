/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// PalShiftTex in the client.  currently not used.
    /// </summary>
    public class PaletteShiftTexture
    {
        /// <summary>
        /// 0x000 tex_guid : IDClass
        /// </summary>
        public uint Id;

        /// <summary>
        /// 0x004 sub_pal : AC1Legacy::SmartArray(PalShiftSubPal *)
        /// </summary>
        public List<PaletteShiftSubPalette> SubPalettes = new List<PaletteShiftSubPalette>();

        /// <summary>
        /// 0x010 road_code : AC1Legacy::SmartArray(PalShiftRoadCode *)
        /// </summary>
        public List<PaletteShiftRoadCode> RoadCodes = new List<PaletteShiftRoadCode>();

        /// <summary>
        /// 0x01c terrain_pal : AC1Legacy::SmartArray(PalShiftTerrainPal *)
        /// </summary>
        public List<PaletteShiftTerrainPalette> TerrainPalette = new List<PaletteShiftTerrainPalette>();

        /// <summary>
        /// private because all valid instances come from Unpack
        /// </summary>
        private PaletteShiftTexture()
        {
        }

        /// <summary>
        /// //----- (005003F0) --------------------------------------------------------
        /// int __thiscall PalShiftTex::UnPack(PalShiftTex *this, void **addr, unsigned int *size)
        /// acclient.c 300987
        /// </summary>
        public static PaletteShiftTexture Unpack(BinaryReader reader)
        {
            throw new NotImplementedException();
        }
    }
}
