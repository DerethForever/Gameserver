/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// AnimData in the client
    /// </summary>
    public class AnimationData
    {
        /// <summary>
        /// +0x004 anim_id          : IDClass
        /// </summary>
        public uint AnimationId { get; set; }

        /// <summary>
        /// +0x008 low_frame        : Int4B
        /// </summary>
        public int LowFrame { get; set; }

        /// <summary>
        /// +0x00c high_frame       : Int4B
        /// </summary>
        public int HighFrame { get; set; }

        /// <summary>
        /// +0x010 framerate        : Float
        /// </summary>
        public float FrameRate { get; set; }

        public AnimationData()
        {

        }

        /// <summary>
        /// ----- (00525E50) --------------------------------------------------------
        /// int __thiscall AnimData::UnPack(AnimData*this, void** addr, unsigned int size)
        /// acclient.c 341179
        /// </summary>
        public static AnimationData Unpack(BinaryReader reader)
        {
            AnimationData ad = new AnimationData();
            ad.AnimationId = reader.ReadUInt32();
            ad.LowFrame = reader.ReadInt32();
            ad.HighFrame = reader.ReadInt32();
            ad.FrameRate = reader.ReadSingle();
            return ad;
        }

        public AnimationData(AnimationData animationData, float speed = Constants.DefaultSpeed)
        {
            AnimationId = animationData.AnimationId;
            LowFrame = animationData.LowFrame;
            HighFrame = animationData.HighFrame;
            FrameRate = animationData.FrameRate + speed;
        }
    }
}
