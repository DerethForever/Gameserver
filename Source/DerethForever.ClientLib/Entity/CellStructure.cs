/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CCellStruct in the client
    /// </summary>
    public class CellStructure
    {
        /// <summary>
        /// +0x000 cellstruct_id    : Uint4B
        /// </summary>
        public uint CellStructureId { get; set; }

        /// <summary>
        /// +0x004 vertex_array     : CVertexArray
        /// </summary>
        public VertexArray VertexArray { get; set; }

        /// <summary>
        /// +0x02c num_portals      : Uint4B
        /// </summary>
        public uint NumberPortals
        {
            get { return (uint)Portals.Count; }
        }

        /// <summary>
        /// +0x030 portals          : Ptr32 Ptr32 CPolygon
        /// </summary>
        public List<Polygon> Portals { get; set; }

        /// <summary>
        /// +0x034 num_surface_strips : Uint4B
        /// </summary>
        public uint NumberSurfaceStrips
        {
            get { return (uint)SurfaceStrips.Count; }
        }

        /// <summary>
        /// +0x038 surface_strips   : Ptr32 CSurfaceTriStrips
        /// </summary>
        public List<uint> SurfaceStrips; // Fix me

        /// <summary>
        /// +0x03c num_polygons     : Uint4B
        /// </summary>
        public uint NumberPolygons
        {
            get { return (uint)Polygons.Count; }
        }

        /// <summary>
        /// +0x040 polygons         : Ptr32 CPolygon
        /// </summary>
        public Dictionary<ushort, Polygon> Polygons { get; set; }

        /// <summary>
        /// +0x044 drawing_bsp      : Ptr32 BSPTREE
        /// </summary>
        public BspTree DrawingBsp { get; set; }

        /// <summary>
        /// +0x048 num_physics_polygons : Uint4B
        /// </summary>
        public uint NumberPhysicsPolygons
        {
            get { return (uint)PhysicsPolygons.Count; }
        }

        /// <summary>
        /// +0x04c physics_polygons : Ptr32 CPolygon
        /// </summary>
        public Dictionary<ushort, Polygon> PhysicsPolygons { get; set; }

        /// <summary>
        /// +0x050 physics_bsp      : Ptr32 BSPTREE
        /// </summary>
        public BspTree PhysicsBsp { get; set; }

        /// <summary>
        /// +0x054 cell_bsp         : Ptr32 BSPTREE
        /// </summary>
        public BspTree CellBsp { get; set; }

        /// <summary>
        /// ----- (00533D00) --------------------------------------------------------
        /// int __thiscall CCellStruct::UnPack(CCellStruct*this, void** addr, unsigned int size)
        /// acclient.c 355823
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static CellStructure Unpack(BinaryReader reader)
        {
            CellStructure obj = new CellStructure();

            uint numPolygons = reader.ReadUInt32();
            uint numPhysicsPolygons = reader.ReadUInt32();
            uint numPortals = reader.ReadUInt32();

            obj.VertexArray = VertexArray.Unpack(reader);

            for (uint i = 0; i < numPolygons; i++)
            {
                ushort polyId = reader.ReadUInt16();
                obj.Polygons.Add(polyId, Polygon.Unpack(reader));
            }

            ////for (uint i = 0; i < numPortals; i++)
            ////    obj.Portals.Add(reader.ReadUInt16());

            ////reader. AlignBoundary();

            ////obj.CellBsp = BspTree.Unpack(reader, BspType.Cell);

            ////for (uint i = 0; i < numPhysicsPolygons; i++)
            ////{
            ////    ushort poly_id = reader.ReadUInt16();
            ////    obj.PhysicsPolygons.Add(poly_id, Polygon.Read(reader));
            ////}
            ////obj.PhysicsBSP = BSPTree.Read(reader, BSPType.Physics);

            ////uint hasDrawingBSP = reader.ReadUInt32();
            ////if (hasDrawingBSP != 0)
            ////    obj.DrawingBSP = BSPTree.Read(reader, BSPType.Drawing);

            ////reader.AlignBoundary();
            //TODO: Pick up here
            return obj;
        }
    }
}
