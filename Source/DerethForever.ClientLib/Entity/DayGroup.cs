/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// DayGroup in the client
    /// </summary>
    public class DayGroup
    {
        /// <summary>
        /// private because Unpack is the only valid source of DayGroups
        /// </summary>
        private DayGroup()
        {
        }

        /// <summary>
        /// +0x000 day_name         : AC1Legacy::PStringBase<char>
        /// </summary>
        public string DayName;

        /// <summary>
        /// +0x004 chance_of_occur  : Float
        /// </summary>
        public float ChanceOfOccurance;

        /// <summary>
        /// +0x008 sky_time         : AC1Legacy::SmartArray<SkyTimeOfDay *>
        /// </summary>
        public List<SkyObject> SkyObjects = new List<SkyObject>();

        /// <summary>
        /// +0x014 sky_objects      : AC1Legacy::SmartArray<SkyObject*>
        /// </summary>
        public List<SkyTimeOfDay> SkyTimes = new List<SkyTimeOfDay>();

        /// <summary>
        /// //----- (00501A70) --------------------------------------------------------
        /// int __thiscall DayGroup::UnPack(DayGroup *this, void **addr, unsigned int *size)
        /// acclient.c 302642
        /// </summary>
        public static DayGroup Unpack(BinaryReader reader)
        {
            DayGroup dg = new DayGroup
            {
                ChanceOfOccurance = reader.ReadSingle(),
                DayName = reader.ReadPString()
            };

            uint numSkyObj = reader.ReadUInt32();
            for (uint i = 0; i < numSkyObj; i++)
                dg.SkyObjects.Add(SkyObject.Unpack(reader));

            uint numSkyTimes = reader.ReadUInt32();
            for (uint i = 0; i < numSkyTimes; i++)
                dg.SkyTimes.Add(SkyTimeOfDay.Unpack(reader));

            return dg;
        }
    }
}
