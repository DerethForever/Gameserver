/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// GfxObjDegradeInfo in the client
    /// </summary>
    public class GraphicsObjectDegradeInfo : DatabaseObject
    {
        /// <summary>
        /// +0x004 m_dataCategory   : Uint4B
        /// </summary>
        public uint DataCategoryId { get; set; }

        /// <summary>
        /// 0x038 num_degrades : Uint4B
        /// </summary>
        public uint NumberDegrades => (uint)DegradeList.Count;

        /// <summary>
        /// 0x03c degrades : Ptr32 GfxObjInfo
        /// </summary>
        public List<GraphicsObjectInfo> DegradeList { get; set; } = new List<GraphicsObjectInfo>();

        /// <summary>
        /// ----- (0051E400) --------------------------------------------------------
        /// int __thiscall GfxObjDegradeInfo::UnPack(GfxObjDegradeInfo*this, void** addr, unsigned int size)
        /// acclient.c 332294
        /// </summary>
        public static GraphicsObjectDegradeInfo Unpack(BinaryReader reader)
        {
            GraphicsObjectDegradeInfo godi = new GraphicsObjectDegradeInfo();
            godi.DataCategoryId = reader.ReadUInt32();

            // Next read is the number of degraded so I just in-lined it
            for (int j = 0; j < reader.ReadUInt32(); j++)
            {
               godi.DegradeList.Add(GraphicsObjectInfo.Unpack(reader));
            }
            return godi;
        }
    }
}
