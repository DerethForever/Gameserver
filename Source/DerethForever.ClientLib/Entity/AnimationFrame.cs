/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using System.IO;
using System.Numerics;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// AnimFrame in the client
    /// </summary>
    public class AnimationFrame
    {
        /// <summary>
        /// 0x000 frame : Ptr32 AFrame
        /// </summary>
        public List<Frame> Frames { get; set; } = new List<Frame>();

        /// <summary>
        /// 0x004 num_frame_hooks : Uint4B
        /// </summary>
        public uint NumFrameHooks
        {
            get { return (uint) Frames.Count; }
        }

        /// <summary>
        /// 0x008 hooks : Ptr32 CAnimHook
        /// </summary>
        public List<AnimationHook> Hooks { get; set; } = new List<AnimationHook>();

        /// <summary>
        /// 0x00c num_parts : Uint4B
        /// </summary>
        public uint NumParts
        {
            get { return (uint) Hooks.Count; }
        }

        /// <summary>
        /// //----- (0051F8E0) --------------------------------------------------------
        /// int __thiscall AnimFrame::UnPack(AnimFrame *this, unsigned int _num_parts, void **addr, unsigned int size)
        /// acclient.c 333870
        /// </summary>
        public static AnimationFrame Unpack(uint numParts, BinaryReader reader)
        {
            AnimationFrame a = new AnimationFrame();

            for (uint i = 0; i < numParts; i++)
                a.Frames.Add(Frame.Unpack(reader));

            uint numHooks = reader.ReadUInt32();

            for (uint i = 0; i < numHooks; i++)
                a.Hooks.Add(AnimationHook.Unpack(reader));

            return a;
        }        
    }
}
