/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.Numerics;
using DerethForever.ClientLib.Enum;
using DerethForever.ClientLib.Integration;

namespace DerethForever.ClientLib.Entity
{
    public class MotionInterp
    {
        /// <summary>
        /// +0x000 initted          : Int4B
        /// </summary>
        public bool Initted { get; set; }

        /// <summary>
        /// +0x004 weenie_obj       : Ptr32 CWeenieObject
        /// </summary>
        public IWorldObject WeenieObject { get; set; }

        /// <summary>
        /// +0x008 physics_obj      : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject PhysicsObject { get; set; }

        /// <summary>
        /// +0x00c raw_state        : RawMotionState
        /// </summary>
        public RawMotionState RawState { get; set; }

        /// <summary>
        /// +0x044 interpreted_state : InterpretedMotionState
        /// </summary>
        public InterpretedMotionState InterpretedState { get; set; }

        /// <summary>
        /// +0x06c current_speed_factor : Float
        /// </summary>
        public float CurrentSpeedFactor { get; set; }

        /// <summary>
        /// +0x070 standing_longjump : Int4B
        /// </summary>
        public bool StandingLongJump { get; set; }

        /// <summary>
        /// +0x074 jump_extent      : Float
        /// </summary>
        public float JumpExtent { get; set; }

        /// <summary>
        /// +0x078 server_action_stamp : Uint4B
        /// </summary>
        public int ServerActionStamp { get; set; }

        /// <summary>
        /// +0x07c my_run_rate      : Float
        /// </summary>
        public float MyRunRate { get; set; }

        /// <summary>
        /// +0x080 pending_motions  : LList<CMotionInterp::MotionNode>
        /// </summary>
        public List<MotionNode> PendingMotions { get; set; }

        /// <summary>
        /// ----- (00528C00) --------------------------------------------------------
        /// CMotionInterp* __cdecl CMotionInterp::Create(CPhysicsObj* _physics_obj, CWeenieObject* _weenie_obj)
        /// acclient.c 344528 - just used normal constructor instead of the create wrapper in the client.
        /// </summary>
        public MotionInterp(PhysicsObject physicsObj, IWorldObject weenieObj)
        {
            CurrentSpeedFactor = Constants.DefaultSpeed;
            MyRunRate = Constants.DefaultSpeed;

            SetPhysicsObject(physicsObj);
            SetWeenieObject(weenieObj);
        }

        /// <summary>
        /// ----- (00528870) --------------------------------------------------------
        /// void __thiscall CMotionInterp::apply_current_movement(CMotionInterp*this, int cancel_moveto, int disallow_jump)
        /// acclient.c 344302
        /// </summary>
        public void ApplyCurrentMovement(bool cancelMoveTo, bool disallowJump)
        {
            if (PhysicsObject == null || !Initted)
                return;

            if (WeenieObject == null || WeenieObject.IsCreature() && PhysicsObject.MovmentIsAutonomous())
                ApplyRawMovement(cancelMoveTo, disallowJump);
            else
                ApplyInterpretedMovement(cancelMoveTo, disallowJump);
        }

        /// <summary>
        /// ----- (005287E0) --------------------------------------------------------
        /// void __thiscall CMotionInterp::apply_raw_movement(CMotionInterp*this, int cancel_moveto, int disallow_jump)
        /// acclient.c 344260
        /// </summary>
        public void ApplyRawMovement(bool cancelMoveTo, bool disallowJump)
        {
            if (PhysicsObject == null)
                return;

            InterpretedState.CurrentStyle = RawState.CurrentStyle;
            InterpretedState.ForwardCommand = RawState.ForwardCommand;
            InterpretedState.ForwardSpeed = RawState.ForwardSpeed;
            InterpretedState.SideStepCommand = RawState.SideStepCommand;
            InterpretedState.SideStepSpeed = RawState.SideStepSpeed;
            InterpretedState.TurnCommand = RawState.TurnCommand;
            InterpretedState.TurnSpeed = RawState.TurnSpeed;

            AdjustMotion(InterpretedState.ForwardCommand, InterpretedState.ForwardSpeed, RawState.ForwardHoldKey);
            AdjustMotion(InterpretedState.SideStepCommand, InterpretedState.SideStepSpeed, RawState.SideStepHoldKey);
            AdjustMotion(InterpretedState.TurnCommand, InterpretedState.TurnSpeed, RawState.TurnHoldKey);

            ApplyInterpretedMovement(cancelMoveTo, disallowJump);
        }

        /// <summary>
        /// ----- (00528600) --------------------------------------------------------
        /// void __thiscall CMotionInterp::apply_interpreted_movement(CMotionInterp*this, int cancel_moveto, int disallow_jump)
        /// acclient.c 344148
        /// </summary>
        public void ApplyInterpretedMovement(bool cancelMoveTo, bool disallowJump)
        {
            if (PhysicsObject == null)
                return;

            // Double check this - I simplified it and it needs a second look.  Coral Golem
            MovementParameters movementParams = new MovementParameters
            {
                CancelMoveTo = cancelMoveTo,
                DisableJumpDuringLink = disallowJump
            };

            if (InterpretedState.ForwardCommand == MotionCommand.RunForward)
                MyRunRate = InterpretedState.ForwardSpeed;

            DoInterpretedMotion(InterpretedState.CurrentStyle, movementParams);

            if (ContactAllowsMove(InterpretedState.ForwardCommand))
            {
                if (!StandingLongJump)
                {
                    movementParams.Speed = InterpretedState.ForwardSpeed;
                    DoInterpretedMotion(InterpretedState.ForwardCommand, movementParams);

                    if (InterpretedState.SideStepCommand != 0)
                    {
                        movementParams.Speed = InterpretedState.SideStepSpeed;
                        DoInterpretedMotion(InterpretedState.SideStepCommand, movementParams);
                    }
                    else
                        StopInterpretedMotion(MotionCommand.SideStepRight, movementParams);
                }
                else
                {
                    movementParams.Speed = Constants.DefaultSpeed;
                    DoInterpretedMotion(MotionCommand.Ready, movementParams);
                    StopInterpretedMotion(MotionCommand.SideStepRight, movementParams);
                }
            }
            else
            {
                movementParams.Speed = Constants.DefaultSpeed;
                DoInterpretedMotion(MotionCommand.Falling, movementParams);
            }

            if (InterpretedState.TurnCommand != 0)
            {
                movementParams.Speed = InterpretedState.TurnSpeed;
                DoInterpretedMotion(InterpretedState.TurnCommand, movementParams);
            }
            else
            {
                if (StopInterpretedMotion(MotionCommand.TurnRight, movementParams) != null)
                {
                    // AddToQueue in the client
                    PendingMotions.Add(new MotionNode(movementParams.ContextId, MotionCommand.Ready, 0));

                    if (movementParams.ModifyInterpretedState)
                        InterpretedState.RemoveMotion(MotionCommand.TurnRight);
                }

                if (PhysicsObject.Cell == null)
                    PhysicsObject.RemoveLinkAnimations();
            }
        }

        /// <summary>
        /// ----- (00528360) --------------------------------------------------------
        /// signed int __thiscall CMotionInterp::DoInterpretedMotion(CMotionInterp*this, unsigned int motion, MovementParameters*params)
        /// acclient.c 343976
        /// </summary>
        public Sequence DoInterpretedMotion(MotionCommand motion, MovementParameters movementParams)
        {
            if (PhysicsObject == null)
                return new Sequence(8);

            Sequence sequence = new Sequence();

            if (ContactAllowsMove(motion))
            {
                if (StandingLongJump && (motion == MotionCommand.WalkForward||
                    motion == MotionCommand.RunForward ||
                    motion == MotionCommand.SideStepRight))
                {
                    if (movementParams.ModifyInterpretedState)
                        InterpretedState.ApplyMotion(motion, movementParams);
                }
                else
                {
                    if (motion == MotionCommand.Dead)
                        PhysicsObject.RemoveLinkAnimations();

                    sequence = PhysicsObject.DoInterpretedMotion(motion, movementParams);

                    if (sequence == null)
                    {
                        uint jumpErrorCode = 0;

                        if (movementParams.DisableJumpDuringLink)
                            jumpErrorCode = 0x48;
                        else
                        {
                            jumpErrorCode = MotionAllowsJump(motion);

                            if (jumpErrorCode == 0 && (motion & MotionCommand.ActionMask) != 0)
                                jumpErrorCode = MotionAllowsJump(InterpretedState.ForwardCommand);
                        }

                        // AddToQueue in the client
                        PendingMotions.Add(new MotionNode(movementParams.ContextId, motion, jumpErrorCode));

                        if (movementParams.ModifyInterpretedState)
                            InterpretedState.ApplyMotion(motion, movementParams);
                    }
                }
            }
            else
            {
                if ((motion & MotionCommand.ActionMask) != 0)
                    sequence.Id = 0x24;

                else if (movementParams.ModifyInterpretedState)
                    InterpretedState.ApplyMotion(motion, movementParams);
            }

            if (PhysicsObject != null && PhysicsObject.Cell == null)
                PhysicsObject.RemoveLinkAnimations();

            return sequence;
        }

        /// <summary>
        /// ----- (00528010) --------------------------------------------------------
        /// void __thiscall CMotionInterp::adjust_motion(CMotionInterp*this, unsigned int* motion, float* speed, HoldKey key)
        /// acclient.c 343747
        /// </summary>
        public void AdjustMotion(MotionCommand motion, float speed, HoldKey holdKey)
        {
            if (WeenieObject != null && !WeenieObject.IsCreature())
                return;

            switch (motion)
            {
                case MotionCommand.RunForward:
                    return;

                case MotionCommand.WalkBackwards:
                    motion = MotionCommand.WalkForward;
                    speed *= -Constants.BackwardsFactor;
                    break;

                case MotionCommand.TurnLeft:
                    motion = MotionCommand.TurnRight;
                    speed *= -Constants.DefaultSpeed;
                    break;

                case MotionCommand.SideStepLeft:
                    motion = MotionCommand.SideStepRight;
                    speed *= -Constants.DefaultSpeed;
                    break;

                case MotionCommand.SideStepRight:
                    speed = speed * Constants.DefaultSideStepRightAdj;
                    break;
            }

            if (holdKey == HoldKey.Invalid)
                holdKey = RawState.CurrentHoldKey;

            if (holdKey == HoldKey.Run)
                ApplyRunToCommand(ref motion, ref speed);
        }

        /// <summary>
        /// ----- (00527BE0) --------------------------------------------------------
        /// void __thiscall CMotionInterp::apply_run_to_command(CMotionInterp*this, unsigned int* motion, float* speed)
        /// acclient.c 343440
        /// </summary>
        public void ApplyRunToCommand(ref MotionCommand motion, ref float speed)
        {
            float speedMod = 1.0f;

            if (WeenieObject != null)
            {
                float runFactor = 0.0f;
                speedMod = WeenieObject.InqRunRate(ref runFactor) ? runFactor : MyRunRate;
            }
            switch (motion)
            {
                case MotionCommand.WalkForward:
                    if (speed > 0.0f)
                        motion = MotionCommand.RunForward;

                    speed *= speedMod;
                    break;

                case MotionCommand.TurnRight:
                    speed *= Constants.RunTurnFactor;
                    break;

                case MotionCommand.SideStepRight:
                    speed *= speedMod;

                    if (Constants.MaxSidestepAnimRate < Math.Abs(speed))
                    {
                        if (speed > 0.0f)
                            speed = Constants.MaxSidestepAnimRate;
                        else
                            speed = -Constants.MaxSidestepAnimRate;
                    }
                    break;
            }
        }

        /// <summary>
        /// ----- (00528240) --------------------------------------------------------
        /// int __thiscall CMotionInterp::contact_allows_move(CMotionInterp*this, unsigned int motion)
        /// acclient.c 343883
        /// </summary>
        public bool ContactAllowsMove(MotionCommand motion)
        {
            if (PhysicsObject == null)
                return false;

            if (motion == MotionCommand.Dead || motion == MotionCommand.Falling || motion >= MotionCommand.TurnRight && motion <= MotionCommand.TurnLeft)
                return true;

            if (WeenieObject != null && !WeenieObject.IsCreature())
                return true;

            if (!PhysicsObject.State.HasFlag(PhysicsState.Gravity))
                return true;

            if (!PhysicsObject.TransientState.HasFlag(TransientState.Contact))
                return false;

            return PhysicsObject.TransientState.HasFlag(TransientState.Walkable);
        }

        /// <summary>
        /// ----- (00528470) --------------------------------------------------------
        /// signed int __thiscall CMotionInterp::StopInterpretedMotion(CMotionInterp*this, unsigned int motion, MovementParameters*params)
        /// acclient.c 344035
        /// </summary>
        public Sequence StopInterpretedMotion(MotionCommand motion, MovementParameters movementParams)
        {
            if (PhysicsObject == null)
            {
                // Magic number in the client - result = 8; acclient.c - 344076
                return new Sequence(8);
            }

            Sequence sequence = new Sequence();

            if (ContactAllowsMove(motion))
            {
                if (StandingLongJump && (motion == MotionCommand.WalkForward || motion == MotionCommand.RunForward || motion == MotionCommand.SideStepRight))
                {
                    if (movementParams.ModifyInterpretedState)
                        InterpretedState.RemoveMotion(motion);
                }
                else
                {
                    sequence = PhysicsObject.StopInterpretedMotion(motion, movementParams);

                    if (sequence == null)
                    {
                        // AddToQueue in the client
                        PendingMotions.Add(new MotionNode(movementParams.ContextId, MotionCommand.Ready, 0));

                        if (movementParams.ModifyInterpretedState)
                            InterpretedState.RemoveMotion(motion);
                    }
                }
            }
            else
            {
                if (movementParams.ModifyInterpretedState)
                    InterpretedState.RemoveMotion(motion);
            }

            if (PhysicsObject.Cell == null)
                PhysicsObject.RemoveLinkAnimations();

            return sequence;
        }

        /// <summary>
        /// ----- (005279E0) --------------------------------------------------------
        /// signed int __stdcall CMotionInterp::motion_allows_jump(unsigned int substate)
        /// acclient.c 343296
        /// </summary>
        public uint MotionAllowsJump(MotionCommand subState)
        {
            if (subState >= MotionCommand.Reload && subState <= MotionCommand.Pickup ||
                subState >= MotionCommand.TripleThrustLow && subState <= MotionCommand.MagicPowerUp07Purple||
                subState >= MotionCommand.MagicPowerUp01 && subState <= MotionCommand.MagicPowerUp10 ||
                subState >= MotionCommand.Crouch && subState <= MotionCommand.Sleeping ||
                subState >= MotionCommand.AimLevel && subState <= MotionCommand.MagicPray ||
                subState == MotionCommand.Fallen)
            {
                // magic number in the client 343315
                return 72;
            }
            return 0;
        }

        /// <summary>
        /// ----- (00528970) --------------------------------------------------------
        /// void __thiscall CMotionInterp::SetPhysicsObject(CMotionInterp*this, CPhysicsObj* _physics_obj)
        /// acclient.c 344355
        /// </summary>
        public void SetPhysicsObject(PhysicsObject obj)
        {
            PhysicsObject = obj;
            ApplyCurrentMovement(true, true);
        }

        /// <summary>
        /// ----- (00528920) --------------------------------------------------------
        /// void __thiscall CMotionInterp::SetWeenieObject(CMotionInterp*this, CWeenieObject* _weenie_obj)
        /// acclient.c 344336
        /// </summary>
        public void SetWeenieObject(IWorldObject wobj)
        {
            WeenieObject = wobj;
            ApplyCurrentMovement(true, true);
        }

        /// <summary>
        /// ----- (00528E80) --------------------------------------------------------
        /// signed int __thiscall CMotionInterp::PerformMovement(CMotionInterp*this, MovementStruct* mvs)
        /// acclient.c 344672
        /// </summary>
        public Sequence PerformMovement(MovementStructure movementStructure)
        {
            Sequence sequence = null;

            switch (movementStructure.Type)
            {
                case MovementTypes.RawCommand:
                    sequence = DoMotion(movementStructure.Motion, movementStructure.Params);
                    break;
                case MovementTypes.InterpretedCommand:
                    sequence = DoInterpretedMotion(movementStructure.Motion, movementStructure.Params);
                    break;
                case MovementTypes.StopRawCommand:
                    sequence = StopMotion(movementStructure.Motion, movementStructure.Params);
                    break;
                case MovementTypes.StopInterpretedCommand:
                    sequence = StopInterpretedMotion(movementStructure.Motion, movementStructure.Params);
                    break;
                case MovementTypes.StopCompletely:
                    sequence = StopCompletely();
                    break;
                default:
                    sequence.Id = 71;
                    return sequence;
            }
            PhysicsObject.CheckForCompletedMotions();
            return sequence;
        }

        /// <summary>
        /// ----- (00528D20) --------------------------------------------------------
        /// signed int __thiscall CMotionInterp::DoMotion(CMotionInterp*this, unsigned int motion, MovementParameters*params)
        /// acclient.c 344602
        /// </summary>
        public Sequence DoMotion(MotionCommand motion, MovementParameters movementParams)
        {
            if (PhysicsObject == null)
                return new Sequence(8);

            if (movementParams.CancelMoveTo)
                PhysicsObject.CancelMoveTo();

            if (movementParams.SetHoldKey)
                SetHoldKey(movementParams.HoldKeyToApply, movementParams.CancelMoveTo);

            Sequence sequence = new Sequence();

            if (InterpretedState.CurrentStyle != MotionCommand.NonCombat)
            {
                switch (motion)
                {
                    case MotionCommand.Crouch:
                        sequence.Id = 0x3F;
                        return sequence;
                    case MotionCommand.Sitting:
                        sequence.Id = 0x40;
                        return sequence;
                    case MotionCommand.Sleeping:
                        sequence.Id = 0x41;
                        return sequence;
                }

                if ((motion & (MotionCommand)0x2000000) != 0)
                {
                    sequence.Id = 0x42;
                    return sequence;
                }
            }

            if ((motion & MotionCommand.ActionMask) != 0)
            {
                if (InterpretedState.GetNumActions() >= 6)
                {
                    sequence.Id = 0x45;
                    return sequence;
                }
            }
            Sequence newMotion = DoInterpretedMotion(motion, movementParams);

            if (newMotion == null && movementParams.ModifyRawState)
                RawState.ApplyMotion(motion, movementParams);

            return newMotion;
        }

        /// <summary>
        /// ----- (00528530) --------------------------------------------------------
        /// signed int __thiscall CMotionInterp::StopMotion(CMotionInterp*this, unsigned int motion, MovementParameters*params)
        /// acclient.c 344083
        /// </summary>
        public Sequence StopMotion(MotionCommand motion, MovementParameters movementParams)
        {
            if (PhysicsObject == null)
                return new Sequence(8);

            if (movementParams.CancelMoveTo)
                PhysicsObject.CancelMoveTo();

            AdjustMotion(motion, movementParams.Speed, movementParams.HoldKeyToApply);

            Sequence newMotion = StopInterpretedMotion(motion, movementParams);

            if (newMotion == null && movementParams.ModifyRawState)
                RawState.RemoveMotion(motion);

            return newMotion;
        }

        /// <summary>
        /// ----- (00528BB0) --------------------------------------------------------
        /// void __thiscall CMotionInterp::SetHoldKey(CMotionInterp*this, HoldKey key, int cancel_moveto)
        /// acclient.c 344504
        /// </summary>
        public void SetHoldKey(HoldKey key, bool cancelMoveTo)
        {
            if ((key == RawState.CurrentHoldKey) || (key != HoldKey.None) || (RawState.CurrentHoldKey != HoldKey.Run))
                return;

            RawState.CurrentHoldKey = HoldKey.None;
            ApplyCurrentMovement(cancelMoveTo, true);
        }

        /// <summary>
        /// ----- (00527E40) --------------------------------------------------------
        /// signed int __thiscall CMotionInterp::StopCompletely(CMotionInterp*this)
        /// acclient.c 343599
        /// </summary>
        public Sequence StopCompletely()
        {
            if (PhysicsObject == null)
                return new Sequence(8);

            PhysicsObject.CancelMoveTo();

            uint jump = MotionAllowsJump(InterpretedState.ForwardCommand);

            RawState.ForwardCommand = MotionCommand.Ready;
            RawState.ForwardSpeed = Constants.DefaultSpeed;
            RawState.SideStepCommand = 0;
            RawState.TurnCommand = 0;

            InterpretedState.ForwardCommand = MotionCommand.Ready;
            InterpretedState.ForwardSpeed = Constants.DefaultSpeed;
            InterpretedState.SideStepCommand = 0;
            InterpretedState.TurnCommand = 0;

            PhysicsObject.StopCompletelyInternal();

            PendingMotions.Add(new MotionNode(0, MotionCommand.Ready, jump));

            if (PhysicsObject.Cell != null)
                PhysicsObject.RemoveLinkAnimations();

            return new Sequence(0);
        }

        /// <summary>
        /// ----- (00528C80) --------------------------------------------------------
        /// void __thiscall CMotionInterp::enter_default_state(CMotionInterp*this)
        /// acclient.c 344562
        /// </summary>
        public void EnterDefaultState()
        {
            RawState = new RawMotionState();
            InterpretedState = new InterpretedMotionState();

            PhysicsObject.InitializeMotionTables();
            PendingMotions = new List<MotionNode>();

            AddToQueue(0, MotionCommand.Ready, 0);

            Initted = true;
            LeaveGround();
        }

        /// <summary>
        /// ----- (00527B80) --------------------------------------------------------
        /// void __thiscall CMotionInterp::add_to_queue(CMotionInterp*this, unsigned int context_id, unsigned int motion, unsigned int jump_error_code)
        /// </summary>
        public void AddToQueue(uint contextId, MotionCommand motion, uint jumpErrorCode)
        {
            PendingMotions.Add(new MotionNode(contextId, motion, jumpErrorCode));
        }

        /// <summary>
        /// ----- (00528B00) --------------------------------------------------------
        /// void __thiscall CMotionInterp::LeaveGround(CMotionInterp*this)
        /// acclient.c 344457
        /// </summary>
        public void LeaveGround()
        {
            if (PhysicsObject == null)
                return;

            if (!PhysicsObject.State.HasFlag(PhysicsState.Gravity))
                return;

            if (WeenieObject != null && !WeenieObject.IsCreature())
                return;

            Vector3 velocity = GetLeaveGroundVelocity();
            PhysicsObject.SetLocalVelocity(velocity, true);

            StandingLongJump = false;
            JumpExtent = 0;

            PhysicsObject.RemoveLinkAnimations();
            ApplyCurrentMovement(false, true);
        }

        /// <summary>
        /// ----- (005280C0) --------------------------------------------------------
        /// void __thiscall CMotionInterp::get_leave_ground_velocity(CMotionInterp*this, AC1Legacy::Vector3* v)
        /// acclient.c 343806
        /// </summary>
        public Vector3 GetLeaveGroundVelocity()
        {
            Vector3 velocity = GetStateVelocity();
            velocity.Z = GetJumpVelocityZ();

            if (!velocity.Equals(Vector3.Zero))
                velocity = PhysicsObject.CurrentPosition.GlobalToLocalVector(velocity);

            return velocity;
        }

        /// <summary>
        /// ----- (00527D50) --------------------------------------------------------
        /// void __thiscall CMotionInterp::get_state_velocity(CMotionInterp*this, AC1Legacy::Vector3* v)
        /// acclient.c 343539
        /// </summary>
        public Vector3 GetStateVelocity()
        {
            Vector3 velocity = Vector3.Zero;

            if (InterpretedState.SideStepCommand == MotionCommand.SideStepRight)
                velocity.X = Constants.SidestepAnimSpeed * InterpretedState.SideStepSpeed;

            switch (InterpretedState.ForwardCommand)
            {
                case MotionCommand.WalkForward:
                    velocity.Y = Constants.WalkAnimSpeed * InterpretedState.ForwardSpeed;
                    break;
                case MotionCommand.RunForward:
                    velocity.Y = Constants.RunAnimSpeed * InterpretedState.ForwardSpeed;
                    break;
            }

            float rate = MyRunRate;

            if (WeenieObject != null)
                WeenieObject.InqRunRate(ref rate);

            float maxSpeed = Constants.RunAnimSpeed * rate;
            if (velocity.Length() > maxSpeed)
            {
                velocity = velocity.Normalize();
                velocity *= maxSpeed;
            }
            return velocity;
        }

        /// <summary>
        /// ----- (00527CB0) --------------------------------------------------------
        /// double __thiscall CMotionInterp::get_max_speed(CMotionInterp*this)
        /// acclient.c 343486
        /// </summary>
        public float GetMaxSpeed()
        {
            float rate = 1.0f;

            if (WeenieObject != null)
                if (!WeenieObject.InqRunRate(ref rate))
                    rate = MyRunRate;

            return Constants.RunAnimSpeed * rate;
        }

        /// <summary>
        /// ----- (00527AA0) --------------------------------------------------------
        /// double __thiscall CMotionInterp::get_jump_v_z(CMotionInterp*this)
        /// acclient.c 343343
        /// </summary>
        public float GetJumpVelocityZ()
        {
            if (JumpExtent < Constants.TOLERANCE)
                return 0.0f;

            float extent = JumpExtent;

            if (extent > Constants.DefaultSpeed)
                extent = Constants.DefaultSpeed;

            if (WeenieObject == null)
                return Constants.MaxJumpVelocityZ;

            float vz = extent;
            if (WeenieObject.InqJumpVelocity(extent, ref vz))
                return vz;

            return 0.0f;
        }
    }
}
