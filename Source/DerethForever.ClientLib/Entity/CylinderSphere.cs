/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.IO;
using System.Numerics;

namespace DerethForever.ClientLib.Entity
{
    public class CylinderSphere
    {
        /// <summary>
        /// +0x000 low_pt           : AC1Legacy::Vector3
        /// </summary>
        public Vector3 LowPoint;

        /// <summary>
        /// +0x00c height           : Float
        /// </summary>
        public float Height { get; set; }

        /// <summary>
        /// +0x010 radius           : Float
        /// </summary>
        public float Radius { get; set; }

        public CylinderSphere(Vector3 origin, float radius, float height)
        {
            this.LowPoint = origin;
            this.Radius = radius;
            this.Height = height;
        }

        /// <summary>
        /// //----- (0053A960) --------------------------------------------------------
        /// int __thiscall CCylSphere::UnPack(CCylSphere*this, void** addr, unsigned int size)
        /// acclient.c 361548
        /// </summary>
        public static CylinderSphere Unpack(BinaryReader reader)
        {
            CylinderSphere cs = new CylinderSphere(reader.ReadVector(), reader.ReadSingle(), reader.ReadSingle());
            return cs;
        }
    }
}
