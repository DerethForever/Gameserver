/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.Collections.Generic;
using DerethForever.ClientLib.Enum;


namespace DerethForever.ClientLib.Entity
{
    public class RawMotionState
    {
        /// <summary>
        /// +0x004 actions          : LList<ActionNode>
        /// </summary>
        public List<ActionNode> Actions { get; set; }

        /// <summary>
        ///  +0x00c current_holdkey  : HoldKey
        /// </summary>
        public HoldKey CurrentHoldKey { get; set; }

        /// <summary>
        /// +0x010 current_style    : Uint4B
        /// </summary>
        public MotionCommand CurrentStyle { get; set; }

        /// <summary>
        /// +0x014 forward_command  : Uint4B
        /// </summary>
        public MotionCommand ForwardCommand { get; set; }

        /// <summary>
        /// +0x018 forward_holdkey  : HoldKey
        /// </summary>
        public HoldKey ForwardHoldKey { get; set; }

        /// <summary>
        /// +0x01c forward_speed    : Float
        /// </summary>
        public float ForwardSpeed { get; set; }

        /// <summary>
        /// +0x020 sidestep_command : Uint4B
        /// </summary>
        public MotionCommand SideStepCommand { get; set; }

        /// <summary>
        /// +0x024 sidestep_holdkey : HoldKey
        /// </summary>
        public HoldKey SideStepHoldKey { get; set; }

        /// <summary>
        /// +0x028 sidestep_speed   : Float
        /// </summary>
        public float SideStepSpeed { get; set; }

        /// <summary>
        /// +0x02c turn_command     : Uint4B
        /// </summary>
        public MotionCommand TurnCommand { get; set; }

        /// <summary>
        /// +0x030 turn_holdkey     : HoldKey
        /// </summary>
        public HoldKey TurnHoldKey { get; set; }

        /// <summary>
        /// +0x030 turn_holdkey     : HoldKey
        /// </summary>
        public float TurnSpeed { get; set; }

        /// <summary>
        /// ----- (0051E840) --------------------------------------------------------
        /// void __thiscall RawMotionState::AddAction(RawMotionState*this, unsigned int action, float speed, unsigned int stamp, int autonomous)
        /// </summary>
        public void AddAction(MotionCommand action, float speed, uint stamp, bool autonomous)
        {
            Actions.Add(new ActionNode(action, speed, stamp, autonomous));
        }

        /// <summary>
        /// ----- (0051EB60) --------------------------------------------------------
        /// void __thiscall RawMotionState::ApplyMotion(RawMotionState*this, unsigned int motion, MovementParameters*params)
        /// </summary>
        public void ApplyMotion(MotionCommand motion, MovementParameters movementParams)
        {
            switch (motion)
            {
                case MotionCommand.SideStepRight:
                case MotionCommand.SideStepLeft:
                    SideStepCommand = motion;
                    if (movementParams.SetHoldKey)
                    {
                        SideStepHoldKey = HoldKey.Invalid;
                        SideStepSpeed = movementParams.Speed;
                    }
                    else
                    {
                        SideStepHoldKey = movementParams.HoldKeyToApply;
                        SideStepSpeed = movementParams.Speed;
                    }
                    break;

                case MotionCommand.TurnRight:
                case MotionCommand.TurnLeft:
                    TurnCommand = motion;
                    if (movementParams.SetHoldKey)
                    {
                        TurnHoldKey = HoldKey.Invalid;
                        TurnSpeed = movementParams.Speed;
                    }
                    else
                    {
                        TurnHoldKey = movementParams.HoldKeyToApply;
                        TurnSpeed = movementParams.Speed;
                    }
                    break;

                default:
                    if ((motion & MotionCommand.SubStateMask) != 0)
                    {
                        if (motion != MotionCommand.RunForward)
                        {
                            ForwardCommand = motion;
                            if (movementParams.SetHoldKey)
                            {
                                ForwardHoldKey = HoldKey.Invalid;
                                ForwardSpeed = movementParams.Speed;
                            }
                            else
                            {
                                ForwardHoldKey = movementParams.HoldKeyToApply;
                                ForwardSpeed = movementParams.Speed;
                            }
                        }
                    }
                    else if ((motion & MotionCommand.StyleMask) != 0)
                    {
                        if (CurrentStyle != motion)
                        {
                            ForwardCommand = MotionCommand.Ready;
                            CurrentStyle = motion;
                        }
                    }
                    else if ((motion & MotionCommand.ActionMask) != 0)
                    {
                        AddAction(motion, movementParams.Speed, movementParams.ActionStamp, movementParams.Autonomous);
                    }
                    break;
            }
        }

        /// <summary>
        /// ----- (0051E6E0) --------------------------------------------------------
        /// void __thiscall RawMotionState::RemoveMotion(RawMotionState*this, unsigned int motion)
        /// acclient.c 332495
        /// </summary>
        public void RemoveMotion(MotionCommand motion)
        {
            switch (motion)
            {
                case MotionCommand.SideStepRight:
                case MotionCommand.SideStepLeft:
                    SideStepCommand = MotionCommand.Undef;
                    break;

                case MotionCommand.TurnRight:
                case MotionCommand.TurnLeft:
                    TurnCommand = MotionCommand.Undef;
                    break;

                default:
                    if ((motion & MotionCommand.SubStateMask) != 0)
                    {
                        if (motion == ForwardCommand)
                        {
                            ForwardCommand = MotionCommand.Ready;
                            ForwardSpeed = Constants.DefaultSpeed;
                        }
                    }
                    else if ((motion & MotionCommand.StyleMask) != 0 && motion == CurrentStyle)
                    {
                        CurrentStyle = MotionCommand.NonCombat;
                    }
                    break;
            }
        }


    }
}
