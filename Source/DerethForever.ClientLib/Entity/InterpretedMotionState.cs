/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    public class InterpretedMotionState : ICloneable
    {
        /// <summary>
        /// +0x004 current_style    : Uint4B
        /// </summary>
        public MotionCommand CurrentStyle { get; set; }

        /// <summary>
        /// +0x008 forward_command  : Uint4B
        /// </summary>
        public MotionCommand ForwardCommand { get; set; }

        /// <summary>
        /// +0x00c forward_speed    : Float
        /// </summary>
        public float ForwardSpeed { get; set; }

        /// <summary>
        /// 0x010 sidestep_command : Uint4B
        /// </summary>
        public MotionCommand SideStepCommand { get; set; }

        /// <summary>
        /// +0x014 sidestep_speed   : Float
        /// </summary>
        public float SideStepSpeed { get; set; }

        /// <summary>
        /// +0x018 turn_command     : Uint4B
        /// </summary>
        public MotionCommand TurnCommand { get; set; }

        /// <summary>
        /// +0x01c turn_speed       : Float
        /// </summary>
        public float TurnSpeed { get; set; }

        /// <summary>
        /// +0x020 actions          : LList<ActionNode>
        /// </summary>
        public List<ActionNode> Actions { get; set; }

        /// <summary>
        /// ----- (0051E8D0) --------------------------------------------------------
        /// void __thiscall InterpretedMotionState::InterpretedMotionState(InterpretedMotionState*this)
        /// acclient.c 332642
        /// </summary>
        public InterpretedMotionState()
        {
            CurrentStyle = MotionCommand.NonCombat;
            ForwardCommand = MotionCommand.Ready;
            ForwardSpeed = Constants.DefaultSpeed;
            SideStepCommand = MotionCommand.Undef;
            SideStepSpeed = Constants.DefaultSpeed;
            TurnCommand = MotionCommand.Undef;
            TurnSpeed = Constants.DefaultSpeed;
            Actions = new List<ActionNode>();
        }

        /// <summary>
        /// ----- (0051E9E0) --------------------------------------------------------
        /// void __thiscall InterpretedMotionState::AddAction(InterpretedMotionState*this, unsigned int action, float speed, unsigned int stamp, int autonomous)
        /// acclient.c 332725
        /// </summary>
        public void AddAction(MotionCommand action, float speed, uint stamp, bool autonomous)
        {
            Actions.Add(new ActionNode(action, speed, stamp, autonomous));
        }

        /// <summary>
        /// ----- (0051EA40) --------------------------------------------------------
        /// void __thiscall InterpretedMotionState::ApplyMotion(InterpretedMotionState*this, unsigned int motion, MovementParameters*params)
        /// </summary>
        public void ApplyMotion(MotionCommand motion, MovementParameters movementParams)
        {
            switch (motion)
            {
                case MotionCommand.TurnRight:
                    TurnCommand = MotionCommand.TurnRight;
                    TurnSpeed = movementParams.Speed;
                    break;

                case MotionCommand.SideStepRight:
                    SideStepCommand = MotionCommand.SideStepRight;
                    SideStepSpeed = movementParams.Speed;
                    break;

                default:
                    if ((motion & MotionCommand.SubStateMask) != 0)
                    {
                        ForwardCommand = motion;
                        ForwardSpeed = movementParams.Speed;
                    }
                    else if ((motion & MotionCommand.StyleMask) != 0)
                    {
                        ForwardCommand = MotionCommand.Ready;
                        CurrentStyle = motion;
                    }
                    else if ((motion & MotionCommand.ActionMask) != 0)
                    {
                        AddAction(motion, movementParams.Speed, movementParams.ActionStamp, movementParams.Autonomous);
                    }
                    break;
            }
        }

        /// <summary>
        /// ----- (0051EB00) --------------------------------------------------------
        /// unsigned int __thiscall InterpretedMotionState::GetNumActions(InterpretedMotionState*this)
        /// acclient.c 332815
        /// </summary>
        public int GetNumActions()
        {
            return Actions.Count;
        }

        /// <summary>
        /// ----- (0051E750) --------------------------------------------------------
        /// void __thiscall InterpretedMotionState::copy_movement_from(InterpretedMotionState*this, InterpretedMotionState* rhs)
        /// </summary>
        public void CopyMovementFrom(out InterpretedMotionState target, InterpretedMotionState rhs)
        {
            target = (InterpretedMotionState)rhs.Clone();
        }

        /// <summary>
        /// ----- (0051EAD0) --------------------------------------------------------
        /// unsigned int __thiscall InterpretedMotionState::RemoveAction(InterpretedMotionState*this)
        /// acclient.c 332789
        /// </summary>
        public MotionCommand RemoveAction()
        {
            if (Actions.Count == 0)
                return MotionCommand.Undef;

            ActionNode action = Actions.First();
            Actions.RemoveAt(0);
            return action.Action;
        }

        /// <summary>
        /// ----- (0051E790) --------------------------------------------------------
        /// void __thiscall InterpretedMotionState::RemoveMotion(InterpretedMotionState*this, unsigned int motion)
        /// acclient.c 332537
        /// </summary>
        public void RemoveMotion(MotionCommand motion)
        {
            switch (motion)
            {
                case MotionCommand.TurnRight:
                    TurnCommand = 0;
                    break;
                case MotionCommand.SideStepRight:
                    SideStepCommand = 0;
                    break;

                default:
                    if ((motion & MotionCommand.SubStateMask) != 0)
                    {
                        if (ForwardCommand == motion)
                        {
                            ForwardCommand = MotionCommand.Ready;
                            ForwardSpeed = 1.0f;
                        }
                    }
                    else if ((motion & MotionCommand.StyleMask) != 0)
                    {
                        if (CurrentStyle == motion)
                            CurrentStyle = MotionCommand.NonCombat;
                    }
                    break;
            }
        }
        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
