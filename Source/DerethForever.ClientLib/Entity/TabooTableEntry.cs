/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    public class TabooTableEntry
    {
        /// <summary>
        /// private because all valid instances come from Unpack
        /// </summary>
        private TabooTableEntry()
        {
        }

        public ushort Unknown1 { get; set; }

        public ushort Unknown2 { get; set; }

        public ushort Unknown3 { get; set; }

        public ushort Unknown4 { get; set; }

        public ushort Unknown5 { get; set; }

        public List<string> WordList { get; set; } = new List<string>();

        /// <summary>
        /// no corresponding client function.  this one was just brute forced
        /// from data inspection
        /// </summary>
        public static TabooTableEntry Unpack(BinaryReader reader)
        {
            TabooTableEntry tte = new TabooTableEntry();
            tte.Unknown1 = reader.ReadUInt16();
            tte.Unknown2 = reader.ReadUInt16();
            tte.Unknown3 = reader.ReadUInt16();
            tte.Unknown4 = reader.ReadUInt16();
            tte.Unknown5 = reader.ReadUInt16();
            
            uint count = reader.ReadUInt32();

            for (int i = 0; i < count; i++)
                 tte.WordList.Add(reader.ReadTabooString());

            return tte;
        }
    }
}
