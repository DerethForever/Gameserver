/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CSortCell in the client
    /// </summary>
    public class SortCell : ObjectCell
    {
        /// <summary>
        /// 0x0f8 building : Ptr32 CBuildingObj
        /// </summary>
        public Building Building;

        /// <summary>
        /// //----- (005340A0) --------------------------------------------------------
        /// signed int __thiscall CSortCell::find_collisions(CSortCell *this, CTransition *transition)
        /// acclient.c 356107
        /// </summary>
        public override TransitionState FindCollisions(Transition trans)
        {
            // TODO implement SortCell.FindCollisions
            return TransitionState.Ok;
        }

        /// <summary>
        /// //----- (00534030) --------------------------------------------------------
        /// void __thiscall CSortCell::add_building(CSortCell *this, CBuildingObj *_object)
        /// acclient.c 356074
        /// </summary>
        public void AddBuilding(Building building)
        {
            if (Building == null)
                Building = building;
        }

        /// <summary>
        /// //----- (00534000) --------------------------------------------------------
        /// BOOL __thiscall CSortCell::has_building(CSortCell *this)
        /// acclient.c 356068
        /// </summary>
        public bool HasBuilding()
        {
            return (Building != null);
        }
    }
}
