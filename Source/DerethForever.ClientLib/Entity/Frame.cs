/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.IO;
using System.Numerics;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// Frame in the client
    /// </summary>

#pragma warning disable CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()
    public class Frame
#pragma warning restore CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()
    {
        /// <summary>
        /// +0x034 m_fOrigin        : AC1Legacy::Vector3
        /// </summary>
        public Vector3 Origin;

        /// <summary>
        /// +0x000 qw               : Float
        /// +0x004 qx               : Float
        /// +0x008 qy               : Float
        /// +0x00c qz               : Float
        /// </summary>
        public Quaternion Quaternion;

        public Frame()
        {
            Origin = Vector3.Zero;
            Quaternion = new Quaternion(0f, 0f, 0f, 1f);
        }

        /// <summary>
        /// ----- (00424A80) --------------------------------------------------------
        /// void __thiscall Frame::Frame(Frame*this)
        /// acclient.c 96598
        /// </summary>
        public Frame(Frame source)
        {
            Origin = source.Origin;
            Quaternion = source.Quaternion;
        }

        public Frame(Vector3 origin, Quaternion quat)
        {
            Origin = origin;
            Quaternion = quat;
        }

        /// <summary>
        /// ----- (005351A0) --------------------------------------------------------
        /// int __thiscall Frame::UnPack(Frame*this, void** addr, unsigned int size)
        /// acclient.c 357133
        /// </summary>
        public static Frame Unpack(BinaryReader reader)
        {
            Frame loc = new Frame { Origin = reader.ReadVector() };

            // packed quats in the dat are WXYZ, native constructor is XYZW
            float qw = reader.ReadSingle();
            float qx = reader.ReadSingle();
            float qy = reader.ReadSingle();
            float qz = reader.ReadSingle();

            loc.Quaternion = new Quaternion(qx, qy, qz, qw);

            return loc;
        }

        public static bool operator ==(Frame p1, Frame p2)
        {
            if (ReferenceEquals(p1, null))
                return ReferenceEquals(p2, null);

            if (Math.Abs(p1.Origin.X - p2.Origin.X) > Constants.TOLERANCE)
                return false;
            if (Math.Abs(p1.Origin.Y - p2.Origin.Y) > Constants.TOLERANCE)
                return false;
            if (Math.Abs(p1.Origin.Z - p2.Origin.Z) > Constants.TOLERANCE)
                return false;

            return true;
        }

        public static bool operator !=(Frame p1, Frame p2)
        {
            return !(p1 == p2);
        }

        /// <summary>
        /// BOOL __thiscall Frame::is_equal(Frame *this, Frame *rhs)
        /// acclient.c 96700
        /// </summary>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Frame p2 = (Frame)obj;

            return (this == p2);
        }

        /// <summary>
        /// //----- (00535DB0) --------------------------------------------------------
        /// void __thiscall Frame::set_vector_heading(Frame*this, AC1Legacy::Vector3* heading)
        /// acclient.c 357668
        /// </summary>
        public void SetVectorHeading(Vector3 heading)
        {
            if (heading.IsInsignificant())
                return;

            heading.Normalize();

            float zRotation = (-1 * (450f - (float)Math.Atan2(heading.X, heading.Y) * Constants.DEGREES_PER_RADIAN) % 360f) / Constants.DEGREES_PER_RADIAN;
            float xRotation = (float)Math.Asin(heading.Z);

            EulerSetRotation(zRotation, xRotation);
        }

        /// <summary>
        /// client optimizations have been removed from this function in favor of calling stock
        /// quaternion functions in the System.Numerics library.
        ///
        /// ----- (00525180) --------------------------------------------------------
        /// void __thiscall Frame::combine(Frame*this, Frame* _f1, AFrame* _f2)
        /// acclient.c 340420
        /// </summary>
        public static Frame Combine(Frame f1, Frame f2)
        {
            Frame result = new Frame();
            result.Origin = f1.Origin + Vector3.Transform(f2.Origin, f1.Quaternion);
            result.Quaternion = Quaternion.Multiply(f1.Quaternion, f2.Quaternion);
            return result;
        }

        /// <summary>
        /// client optimizations have been removed from this function in favor of calling stock
        /// quaternion functions in the System.Numerics library.
        ///
        /// ----- (00518FD0) --------------------------------------------------------
        /// void __thiscall Frame::combine(Frame*this, Frame* _f1, AFrame* _f2, AC1Legacy::Vector3* scale)
        /// acclient.c 326563
        /// </summary>
        public static Frame Combine(Frame f1, Frame f2, Vector3 scale)
        {
            Frame result = new Frame();
            result.Origin = f1.Origin + Vector3.Transform(f2.Origin * scale, f1.Quaternion);
            result.Quaternion = Quaternion.Multiply(f1.Quaternion, f2.Quaternion);
            return result;
        }

        /// <summary>
        /// ----- (00535E40) --------------------------------------------------------
        /// void __thiscall Frame::set_heading(Frame *this, float degrees)
        /// acclient.c 357692
        /// </summary>
        public void SetHeading(float degrees)
        {
            var radians = (degrees) * Constants.RADIANS_PER_DEGREE;
            var q = Quaternion.CreateFromAxisAngle(Vector3.UnitZ, -radians);
            var q2 = Quaternion * q;
            Quaternion = new Quaternion(0f, 0f, q2.Z, q2.W);
        }

        /// <summary>
        /// //----- (00535BF0) --------------------------------------------------------
        /// void __thiscall Frame::euler_set_rotate(Frame *this, float x, float y, float z, int _order)
        /// acclient.c 357589
        /// </summary>
        public void EulerSetRotation(float zRotation, float yRotation = 0, float xRotation = 0)
        {
            /*
            w = c1 c2 c3 - s1 s2 s3
            x = s1 s2 c3 + c1 c2 s3
            y = s1 c2 c3 + c1 s2 s3
            z = c1 s2 c3 - s1 c2 s3

            where:

            c1 = cos(heading / 2)
            c2 = cos(attitude / 2)
            c3 = cos(bank / 2)
            s1 = sin(heading / 2)
            s2 = sin(attitude / 2)
            s3 = sin(bank / 2)

            for us:

            heading = zRotation
            bank = 0
            attitude = xRotation

             */

            Quaternion = Quaternion.CreateFromYawPitchRoll(yRotation, xRotation, zRotation);
            return;

            double c1 = Math.Cos(zRotation / 2);
            double c2 = Math.Cos(xRotation / 2);
            double c3 = Math.Cos(yRotation / 2);
            double s1 = Math.Sin(zRotation / 2);
            double s2 = Math.Sin(xRotation / 2);
            double s3 = Math.Sin(yRotation / 2);

            Quaternion.W = (float)(c1 * c2 * c3 - s1 * s2 * s3);
            Quaternion.X = (float)(s1 * s2 * c3 + c1 * c2 * s3);
            Quaternion.Y = (float)(s1 * c2 * c3 + c1 * s2 * s3);
            Quaternion.Z = (float)(c1 * s2 * c3 - s1 * c2 * s3);
        }

        /// <summary>
        /// rotates the current frame by the XYZ angles specified in the vector.
        ///
        /// ----- (005357A0) --------------------------------------------------------
        /// void __thiscall Frame::grotate(Frame*this, AC1Legacy::Vector3* w)
        /// acclient.c 357422
        /// </summary>
        public void Rotate(Vector3 w)
        {
            float mag = w.Magnitude();
            if (mag < Constants.TOLERANCE)
                return;

            w *= 1 / mag; // normalize

            // yay system libraries!
            Quaternion q1 = Quaternion.CreateFromAxisAngle(w, mag * 0.5f);
            Quaternion q2 = Quaternion.Multiply(q1, this.Quaternion);
            this.Quaternion = Quaternion.Normalize(q2);
        }

        /// <summary>
        /// ----- (00534ED0) --------------------------------------------------------
        /// int __thiscall Frame::IsValid(Frame*this)
        /// acclient.c 357025
        /// </summary>
        public bool IsValid()
        {
            if (!IsValidExceptForHeading())
                return false;

            float sumSquares = Quaternion.LengthSquared();

            // why 5?  no clue, it's what the client used
            return float.IsNaN(sumSquares) || sumSquares > (5 * Constants.TOLERANCE);
        }

        /// <summary>
        /// ----- (00534FE0) --------------------------------------------------------
        /// BOOL __thiscall Frame::IsValidExceptForHeading(Frame*this)
        /// acclient.c 357051
        /// </summary>
        /// <returns></returns>
        public bool IsValidExceptForHeading()
        {
            bool anyNaN = float.IsNaN(Origin.X) || float.IsNaN(Origin.Y) || float.IsNaN(Origin.Z) || float.IsNaN(Quaternion.W) || float.IsNaN(Quaternion.X) || float.IsNaN(Quaternion.Y) || float.IsNaN(Quaternion.Z);
            return anyNaN;
        }

        /// <summary>
        /// //----- (00452620) --------------------------------------------------------
        /// AC1Legacy::Vector3 *__thiscall Frame::localtoglobal(Frame *this, AC1Legacy::Vector3 *result, AC1Legacy::Vector3 *in)
        /// acclient.c 143702
        /// </summary>
        public Vector3 LocalToGlobal(Vector3 point)
        {
            return Origin + Vector3.Transform(point, Quaternion);
        }

        /// <summary>
        /// ----- (00452550) --------------------------------------------------------
        /// AC1Legacy::Vector3* __thiscall Frame::globaltolocalvec(Frame*this, AC1Legacy::Vector3* result, AC1Legacy::Vector3*in)
        /// acclient.c 143675
        /// </summary>
        public Vector3 GlobalToLocalVector(Vector3 point)
        {
            Matrix4x4 rotate = Matrix4x4.Transpose(Matrix4x4.CreateFromQuaternion(Quaternion));
            return Vector3.Transform(point, rotate);
        }
    }
}
