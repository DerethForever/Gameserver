/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.IO;
using DerethForever.ClientLib.Entity.AnimationHooks;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// CAnimHook in the client
    /// </summary>
    public class AnimationHook : IHook
    {
        public AnimationHookType HookType { get; set; }

        /// <summary>
        /// 0x004 next_hook : Ptr32 CAnimHook
        /// </summary>
        public int Direction { get; set; }

        private IHook _hook = null;

        /// <summary>
        /// 0x004 next_hook : Ptr32 CAnimHook
        /// </summary>
        public IHook NextHook
        {
            get { return _hook; }
            private set { _hook = value; }
        }

        /// <summary>
        /// ----- (005271D0) --------------------------------------------------------
        /// CAnimHook *__cdecl CAnimHook::UnPackHook(void **addr, unsigned int size)
        /// acclient.c 342737
        /// </summary>
        public static AnimationHook Unpack(BinaryReader reader)
        {
            AnimationHook h = new AnimationHook
            {
                HookType = (AnimationHookType)reader.ReadUInt32(),
                Direction = reader.ReadInt32()
            };

            // The following HookTypes have no additional properties:
            // AnimationHookType.AnimationDone
            // AnimationHookType.DefaultScript
            // CreateBlockingParticle

            switch (h.HookType)
            {
                case AnimationHookType.Sound:
                    h._hook = SoundHook.Unpack(reader);
                    break;
                case AnimationHookType.SoundTable:
                    h._hook = SoundTableHook.Unpack(reader);
                    break;
                case AnimationHookType.Attack:
                    h._hook = AttackHook.Unpack(reader);
                    break;
                case AnimationHookType.ReplaceObject:
                    h._hook = ReplaceObjectHook.Unpack(reader);
                    break;
                case AnimationHookType.Ethereal:
                    h._hook = EtherealHook.Unpack(reader);
                    break;
                case AnimationHookType.TransparentPart:
                    h._hook = TransparentPartHook.Unpack(reader);
                    break;
                case AnimationHookType.Luminous:
                    h._hook = LuminousHook.Unpack(reader);
                    break;
                case AnimationHookType.LuminousPart:
                    h._hook = LuminousPartHook.Unpack(reader);
                    break;
                case AnimationHookType.Diffuse:
                    h._hook = DiffuseHook.Unpack(reader);
                    break;
                case AnimationHookType.DiffusePart:
                    h._hook = DiffusePartHook.Unpack(reader);
                    break;
                case AnimationHookType.Scale:
                    h._hook = ScaleHook.Unpack(reader);
                    break;
                case AnimationHookType.CreateParticle:
                    h._hook = CreateParticleHook.Unpack(reader);
                    break;
                case AnimationHookType.DestroyParticle:
                    h._hook = DestroyParticleHook.Unpack(reader);
                    break;
                case AnimationHookType.StopParticle:
                    h._hook = StopParticleHook.Unpack(reader);
                    break;
                case AnimationHookType.NoDraw:
                    h._hook = NoDrawHook.Unpack(reader);
                    break;
                case AnimationHookType.DefaultScriptPart:
                    h._hook = DefaultScriptPartHook.Unpack(reader);
                    break;
                case AnimationHookType.CallPES:
                    h._hook = CallPESHook.Unpack(reader);
                    break;
                case AnimationHookType.Transparent:
                    h._hook = TransparentHook.Unpack(reader);
                    break;
                case AnimationHookType.SoundTweaked:
                    h._hook = SoundTweakedHook.Unpack(reader);
                    break;
                case AnimationHookType.SetOmega:
                    h._hook = SetOmegaHook.Unpack(reader);
                    break;
                case AnimationHookType.TextureVelocity:
                    h._hook = TextureVelocityHook.Unpack(reader);
                    break;
                case AnimationHookType.TextureVelocityPart:
                    h._hook = TextureVelocityPartHook.Unpack(reader);
                    break;
                case AnimationHookType.SetLight:
                    h._hook = SetLightHook.Unpack(reader);
                    break;
            }

            return h;
        }
    }
}
