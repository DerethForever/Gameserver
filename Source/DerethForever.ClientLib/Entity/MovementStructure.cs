/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// dt acclient!movementstruct in the client
    /// </summary>
    public class MovementStructure
    {
        /// <summary>
        /// +0x000 type             : MovementTypes::Type
        /// </summary>
        public MovementTypes Type { get; set; }

        /// <summary>
        /// +0x004 motion           : Uint4B
        /// </summary>
        public MotionCommand Motion { get; set; }

        /// <summary>
        /// +0x008 object_id        : Uint4B
        /// </summary>
        public uint ObjectId { get; set; }

        /// <summary>
        /// +0x00c top_level_id     : Uint4B
        /// </summary>
        public uint TopLevelId { get; set; }

        /// <summary>
        /// +0x010 pos              : Position
        /// </summary>
        public Position Position { get; set; }

        /// <summary>
        /// +0x058 radius           : Float
        /// </summary>
        public float Radius { get; set; }

        /// <summary>
        /// +0x05c height           : Float
        /// </summary>
        public float Height { get; set; }

        /// <summary>
        /// +0x060 params           : Ptr32 MovementParameters
        /// </summary>
        public MovementParameters Params { get; set; }

        public MovementStructure (MovementTypes type)
        {
            Type = type;
        }

        public MovementStructure(MovementTypes type, MotionCommand motion, MovementParameters movementParams)
        {
            Type = type;
            Motion = motion;
            Params = movementParams;
        }
    }

}
