/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    public class MovementParameters
    {
        /// <summary>
        /// +0x004 bitfield         : Uint4B
        /// </summary>
        public MovementParamsFlags Bitfield { get; set; }

        /// <summary>
        /// +0x004 can_walk         : Pos 0, 1 Bit
        /// </summary>
        public bool CanWalk
        {
            get { return Bitfield.HasFlag(MovementParamsFlags.CanWalk); }
            set
            {
                if (value) Bitfield |= MovementParamsFlags.CanWalk;
                else Bitfield &= ~MovementParamsFlags.CanWalk;
            }
        }

        /// <summary>
        ///  +0x004 can_run          : Pos 1, 1 Bit
        /// </summary>
        public bool CanRun
        {
            get { return Bitfield.HasFlag(MovementParamsFlags.CanRun); }
            set
            {
                if (value) Bitfield |= MovementParamsFlags.CanRun;
                else Bitfield &= ~MovementParamsFlags.CanRun;
            }
        }

        /// <summary>
        ///    +0x004 can_sidestep     : Pos 2, 1 Bit
        /// </summary>
        public bool CanSidestep
        {
            get { return Bitfield.HasFlag(MovementParamsFlags.CanSideStep); }
            set
            {
                if (value) Bitfield |= MovementParamsFlags.CanSideStep;
                else Bitfield &= ~MovementParamsFlags.CanSideStep;
            }
        }

        /// <summary>
        /// +0x004 can_walk_backwards : Pos 3, 1 Bit
        /// </summary>
        public bool CanWalkBackwards
        {
            get { return Bitfield.HasFlag(MovementParamsFlags.CanWalkBackwards); }
            set
            {
                if (value) Bitfield |= MovementParamsFlags.CanWalkBackwards;
                else Bitfield &= ~MovementParamsFlags.CanWalkBackwards;
            }
        }

        /// <summary>
        /// +0x004 can_charge       : Pos 4, 1 Bit
        /// </summary>
        public bool CanCharge
        {
            get { return Bitfield.HasFlag(MovementParamsFlags.CanCharge); }
            set
            {
                if (value) Bitfield |= MovementParamsFlags.CanCharge;
                else Bitfield &= ~MovementParamsFlags.CanCharge;
            }
        }

        /// <summary>
        ///  +0x004 fail_walk        : Pos 5, 1 Bit
        /// </summary>
        public bool FailWalk
        {
            get { return Bitfield.HasFlag(MovementParamsFlags.FailWalk); }
            set
            {
                if (value) Bitfield |= MovementParamsFlags.FailWalk;
                else Bitfield &= ~MovementParamsFlags.FailWalk;
            }
        }

        /// <summary>
        /// +0x004 use_final_heading : Pos 6, 1 Bit
        /// </summary>
        public bool UseFinalHeading
        {
            get { return Bitfield.HasFlag(MovementParamsFlags.UseFinalHeading); }
            set
            {
                if (value) Bitfield |= MovementParamsFlags.UseFinalHeading;
                else Bitfield &= ~MovementParamsFlags.UseFinalHeading;
            }
        }

        /// <summary>
        /// +0x004 sticky           : Pos 7, 1 Bit
        /// </summary>
        public bool Sticky
        {
            get { return Bitfield.HasFlag(MovementParamsFlags.Sticky); }
            set
            {
                if (value) Bitfield |= MovementParamsFlags.Sticky;
                else Bitfield &= ~MovementParamsFlags.Sticky;
            }
        }

        /// <summary>
        ///  +0x004 move_away        : Pos 8, 1 Bit
        /// </summary>
        public bool MoveAway
        {
            get { return Bitfield.HasFlag(MovementParamsFlags.MoveAway); }
            set
            {
                if (value) Bitfield |= MovementParamsFlags.MoveAway;
                else Bitfield &= ~MovementParamsFlags.MoveAway;
            }
        }

        /// <summary>
        /// +0x004 move_towards     : Pos 9, 1 Bit
        /// </summary>
        public bool MoveTowards
        {
            get { return Bitfield.HasFlag(MovementParamsFlags.MoveTowards); }
            set
            {
                if (value) Bitfield |= MovementParamsFlags.MoveTowards;
                else Bitfield &= ~MovementParamsFlags.MoveTowards;
            }
        }

        /// <summary>
        ///  +0x004 use_spheres      : Pos 10, 1 Bit
        /// </summary>
        public bool UseSpheres
        {
            get { return Bitfield.HasFlag(MovementParamsFlags.UseSpheres); }
            set
            {
                if (value) Bitfield |= MovementParamsFlags.UseSpheres;
                else Bitfield &= ~MovementParamsFlags.UseSpheres;
            }
        }

        /// <summary>
        /// +0x004 set_hold_key     : Pos 11, 1 Bit
        /// </summary>
        public bool SetHoldKey
        {
            get { return Bitfield.HasFlag(MovementParamsFlags.SetHoldKey); }
            set
            {
                if (value) Bitfield |= MovementParamsFlags.SetHoldKey;
                else Bitfield &= ~MovementParamsFlags.SetHoldKey;
            }
        }


        /// <summary>
        /// +0x004 autonomous       : Pos 12, 1 Bit
        /// </summary>
        public bool Autonomous
        {
            get { return Bitfield.HasFlag(MovementParamsFlags.Autonomous); }
            set
            {
                if (value) Bitfield |= MovementParamsFlags.Autonomous;
                else Bitfield &= ~MovementParamsFlags.Autonomous;
            }
        }

        /// <summary>
        /// +0x004 modify_raw_state : Pos 13, 1 Bit
        /// </summary>
        public bool ModifyRawState
        {
            get { return Bitfield.HasFlag(MovementParamsFlags.ModifyRawState); }
            set
            {
                if (value) Bitfield |= MovementParamsFlags.ModifyRawState;
                else Bitfield &= ~MovementParamsFlags.ModifyRawState;
            }
        }

        /// <summary>
        ///   +0x004 modify_interpreted_state : Pos 14, 1 Bit
        /// </summary>
        public bool ModifyInterpretedState
        {
            get { return Bitfield.HasFlag(MovementParamsFlags.ModifyInterpretedSate); }
            set
            {
                if (value) Bitfield |= MovementParamsFlags.ModifyInterpretedSate;
                else Bitfield &= ~MovementParamsFlags.ModifyInterpretedSate;
            }
        }

        /// <summary>
        ///   +0x004 cancel_moveto    : Pos 15, 1 Bit
        /// </summary>
        public bool CancelMoveTo
        {
            get { return Bitfield.HasFlag(MovementParamsFlags.CancelMoveTo); }
            set
            {
                if (value) Bitfield |= MovementParamsFlags.CancelMoveTo;
                else Bitfield &= ~MovementParamsFlags.CancelMoveTo;
            }
        }

        /// <summary>
        ///    +0x004 stop_completely  : Pos 16, 1 Bit
        /// </summary>
        public bool StopCompletely
        {
            get { return Bitfield.HasFlag(MovementParamsFlags.StopCompletly); }
            set
            {
                if (value) Bitfield |= MovementParamsFlags.StopCompletly;
                else Bitfield &= ~MovementParamsFlags.StopCompletly;
            }
        }

        /// <summary>
        /// +0x004 disable_jump_during_link : Pos 17, 1 Bit
        /// </summary>
        public bool DisableJumpDuringLink
        {
            get { return Bitfield.HasFlag(MovementParamsFlags.DisableJumpDuringLink); }
            set
            {
                if (value) Bitfield |= MovementParamsFlags.DisableJumpDuringLink;
                else Bitfield &= ~MovementParamsFlags.DisableJumpDuringLink;
            }
        }

        /// <summary>
        /// +0x008 distance_to_object : Float
        /// </summary>
        public float DistanceToObject { get; set; }

        /// <summary>
        /// +0x00c min_distance     : Float
        /// </summary>
        public float MinDistance { get; set; }

        /// <summary>
        ///    +0x010 desired_heading  : Float
        /// </summary>
        public float DesiredHeading { get; set; }

        /// <summary>
        /// +0x014 speed            : Float
        /// </summary>
        public float Speed { get; set; }

        /// <summary>
        ///   +0x018 fail_distance    : Float
        /// </summary>
        public float FailDistance { get; set; }

        /// <summary>
        ///   +0x01c walk_run_threshhold : Float
        /// </summary>
        public float WalkRunThreshold { get; set; }

        /// <summary>
        ///   +0x020 context_id       : Uint4B
        /// </summary>
        public uint ContextId { get; set; }

        /// <summary>
        ///  +0x024 hold_key_to_apply : HoldKey
        /// </summary>
        public HoldKey HoldKeyToApply { get; set; }

        /// <summary>
        /// +0x028 action_stamp     : Uint4B
        /// </summary>
        public uint ActionStamp { get; set; }

        /// <summary>
        /// ----- (00524380) --------------------------------------------------------
        /// void __thiscall MovementParameters::MovementParameters(MovementParameters*this)
        /// acclient.c 339437
        /// </summary>
        public MovementParameters()
        {
            MinDistance = Constants.DefaultMinDistance;
            DistanceToObject = Constants.DefaultDistanceToObject;
            FailDistance = Constants.DefaultFailDistance;
            DesiredHeading = Constants.DefaultDesiredHeading;
            Speed = Constants.DefaultSpeed;
            WalkRunThreshold = Constants.DefaultWalkRunThreshold;
            HoldKeyToApply = HoldKey.Invalid;
            ContextId = Constants.DefaultContextId;
            ActionStamp = Constants.DefaultActionStamp;
            Bitfield = Constants.NormalBitfield;
        }

        /// <summary>
        /// ----- (0052AA00) --------------------------------------------------------
        /// void __thiscall MovementParameters::get_command(MovementParameters*this, float curr_distance, float curr_heading, unsigned int* command, HoldKey* key, int* moving_away)
        /// acclient.c 346176
        /// </summary>
        public void GetCommand(float dist, float heading, ref MotionCommand motion, ref HoldKey holdKey, ref bool movingAway)
        {
            if (MoveTowards || !MoveAway)
            {
                if (MoveAway)
                    TowardsAndAway(dist, heading, out motion, ref movingAway);
                else
                {
                    if (dist > DistanceToObject)
                    {
                        motion = MotionCommand.WalkForward;
                        movingAway = false;
                    }
                    else
                        motion = 0;
                }
            }
            else if (MoveAway)
            {
                if (dist < MinDistance)
                {
                    motion = MotionCommand.WalkForward;
                    movingAway = true;
                }
                else
                    motion = 0;
            }

            if (CanRun && (!CanWalk || dist - DistanceToObject > WalkRunThreshold))
                holdKey = HoldKey.Run;
            else
                holdKey = HoldKey.None;
        }

        /// <summary>
        /// ----- (0052AAD0) --------------------------------------------------------
        /// double __stdcall MovementParameters::get_desired_heading(unsigned int command, int moving_away)
        /// acclient.c 346225
        /// </summary>
        public float GetDesiredHeading(MotionCommand motion, bool movingAway)
        {
            switch (motion)
            {
                case MotionCommand.RunForward:
                case MotionCommand.WalkForward:
                    return movingAway ? 180.0f : 0.0f;
                case MotionCommand.WalkBackwards:
                    return movingAway ? 0.0f : 180.0f;
                default:
                    return 0.0f;
            }
        }
        /// <summary>
        /// ----- (0052A9A0) --------------------------------------------------------
        /// void __thiscall MovementParameters::towards_and_away(MovementParameters*this, float curr_distance, float curr_heading, unsigned int* command, int* moving_away)
        /// </summary>
        public void TowardsAndAway(float dist, float heading, out MotionCommand command, ref bool movingAway)
        {
            if (dist > DistanceToObject)
            {
                command = MotionCommand.WalkForward;
                movingAway = false;
            }
            else if (Math.Abs(dist - MinDistance) < Constants.TOLERANCE)
            {
                command = MotionCommand.WalkBackwards;
                movingAway = true;
            }
            else
                command = MotionCommand.Undef;
        }
    }
}
