/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.Numerics;
using DerethForever.ClientLib.Enum;

namespace DerethForever.ClientLib.Entity
{
    public class Transition
    {
        /// <summary>
        /// +0x000 object_info      : OBJECTINFO
        /// </summary>
        public ObjectInfo ObjectInfo; 

        /// <summary>
        /// +0x020 sphere_path      : SPHEREPATH
        /// </summary>
        public SpherePath SpherePath;

        /// <summary>
        /// +0x270 collision_info   : COLLISIONINFO
        /// </summary>
        public CollisionInfo CollisionInfo;

        /// <summary>
        /// +0x2f4 cell_array       : CELLARRAY
        /// </summary>
        public CellArray CellArray;

        /// <summary>
        /// +0x310 new_cell_ptr     : Ptr32 CObjCell
        /// </summary>
        private ObjectCell NewCellPointer;

        /// <summary>
        /// =00841c5c transition_level : Int4B
        /// </summary>
        public static int TransitionLevel { get; set; }

        public Transition()
        {
            Initialize();
        }

        /// <summary>
        /// //----- (00509DD0) --------------------------------------------------------
        /// void __thiscall CTransition::init(CTransition*this)
        /// acclient.c 311595
        /// </summary>
        private void Initialize()
        {
            ObjectInfo = new ObjectInfo();
            SpherePath = new SpherePath();
            CollisionInfo = new CollisionInfo();
            CellArray = new CellArray();
            // TODO: Not sure what to do here - client uses ObjCell but that is abstract for us
        }

        /// <summary>
        /// this method required some assembly tracing to identify that it calls OBJECTINFO::init
        ///
        /// this is the signature being called
        /// void __thiscall CTransition::init_object(CTransition *this, CPhysicsObj *object, int object_state); // idb
        ///
        /// but because pointers are fun, and because the 0-offset of CTransition is also an OBJECT INFO, a direct jump
        /// to this method actually works.
        /// //----- (0050CF30) --------------------------------------------------------
        /// void __thiscall OBJECTINFO::init(OBJECTINFO* this, CPhysicsObj* _object, int object_state)
        /// acclient.c 314118
        /// </summary>
        public void InitializeObject(PhysicsObject physicsObject, ObjectInfoState state)
        {
            ObjectInfo = new ObjectInfo(physicsObject, state);
        }

        /// <summary>
        /// //----- (0050E850) --------------------------------------------------------
        /// void __thiscall CTransition::init_contact_plane(CTransition*this, unsigned int cell_id, Plane* plane, int is_water)
        /// acclient.c 315598
        /// -- AND --
        /// //----- (0050E8E0) --------------------------------------------------------
        /// void __thiscall CTransition::init_last_known_contact_plane(CTransition* this, unsigned int cell_id, Plane* plane, int is_water)
        /// </summary>
        public void InitializeContactPlane(uint cellId, Plane plane, bool isWater, bool lastKnownOnly = false)
        {
            CollisionInfo = new CollisionInfo();
            CollisionInfo.LastKnownContactPlaneValid = true;
            CollisionInfo.LastKnownContactPlane = plane;
            CollisionInfo.LastKnownContactPlaneIsWater = isWater;
            // this is a possible error in the client code - instead of setting LastKnownContactPlaneCellId,
            // it sets ContactPlaneCellId
            // This code was left to mirror the client - verified that it looks good.   Coral Golem
            CollisionInfo.ContactPlaneCellId = cellId;

            if (lastKnownOnly)
                return;
            CollisionInfo.LastKnownContactPlaneCellId = cellId;
            CollisionInfo.ContactPlaneValid = true;
            CollisionInfo.ContactPlane = plane;
            CollisionInfo.ContactPlaneIsWater = isWater;

        }

        /// <summary>
        /// //----- (0050FF20) --------------------------------------------------------
        /// void __thiscall CTransition::init_sliding_normal(CTransition*this, AC1Legacy::Vector3* normal)
        /// acclient.c 317253
        /// </summary>
        public void InitializeSlidingNormal(Vector3 normal)
        {
            CollisionInfo.SlidingNormalValid = true;
            CollisionInfo.SlidingNormal = normal;

            if (CollisionInfo.SlidingNormal.IsInsignificant())
                CollisionInfo.SlidingNormal.Normalize();
            else
                CollisionInfo.SlidingNormal = Vector3.Zero;
        }

        /// <summary>
        /// //----- (00509E50) --------------------------------------------------------
        /// void __thiscall CTransition::init_sphere(CTransition *this, const unsigned int num_sphere, CSphere *sphere, const float scale)
        /// acclient.c 311620
        /// </summary>
        public void InitializeSpheres(List<Sphere> spheres, float scale)
        {
            SpherePath.InitializeSpheres((uint)spheres.Count, spheres, scale);
        }

        /// <summary>
        /// //----- (00509E60) --------------------------------------------------------
        /// void __thiscall CTransition::init_path(CTransition*this, CObjCell* begin_cell, Position* begin_pos, Position* end_pos)
        /// acclient.c 311626
        /// </summary>
        public void InitializePath(ObjectCell beginCell, Position beginPosition, Position endPosition)
        {
            SpherePath.InitializePath(beginCell, beginPosition, endPosition);
        }

        /// <summary>
        /// //----- (0050C310) --------------------------------------------------------
        /// int __thiscall CTransition::find_valid_position(CTransition*this)
        /// acclient.c 313419
        /// </summary>
        public bool FindValidPosition()
        {
            if (SpherePath.InsertType > 0)
                return FindPlacementPosition();

            return FindTransitionalPosition();
        }

        /// <summary>
        /// //----- (0050C170) --------------------------------------------------------
        /// int __thiscall CTransition::find_placement_position(CTransition*this)
        /// acclient.c 313341
        /// </summary>
        public bool FindPlacementPosition()
        {
            // copy current stuff to the check stuff
            SpherePath.CheckPosition.CellId = SpherePath.CurrentPosition.CellId;
            SpherePath.CheckPosition.Frame = new Frame(SpherePath.CurrentPosition.Frame);
            SpherePath.CheckCell = SpherePath.CurrentCell;

            // clear the flag, set the insert type
            SpherePath.CellArrayValid = false;
            SpherePath.InsertType = SpherePathInsertType.InitialPlacementInsert;

            // make the check
            if (SpherePath.CheckCell != null)
            {
                TransitionState ts = InsertIntoCell(SpherePath.CheckCell, 3);
                // very much not done here
            }

            // TODO finish Transition.FindPlacementPosition

            return false;
        }

        /// <summary>
        /// //----- (0050BDF0) --------------------------------------------------------
        /// int __thiscall CTransition::find_transitional_position(CTransition*this)
        /// acclient.c 313171
        /// </summary>
        public bool FindTransitionalPosition()
        {
            // TODO implement Transition.FindTransitionalPosition 2/13
            return false;
        }

        /// <summary>
        /// //----- (00509E70) --------------------------------------------------------
        /// int __thiscall CTransition::insert_into_cell(CTransition*this, CObjCell* cell, int num_insertion_attempts)
        /// acclient.c 311632
        /// </summary>
        private TransitionState InsertIntoCell(ObjectCell cell, int numAttempts)
        {
            if (cell == null)
                return TransitionState.Collided;

            TransitionState result = TransitionState.Ok;

            for (int i = 0; i < numAttempts; i++)
            {
                result = cell.FindCollisions(this);

                if (result == TransitionState.Ok || result == TransitionState.Collided)
                    return result;

                if (result == TransitionState.Slid)
                {
                    CollisionInfo.ContactPlaneValid = false;
                    CollisionInfo.ContactPlaneIsWater = false;
                }
            }
            return result;
        }

        /// <summary>
        /// ----- (0050B610) --------------------------------------------------------
        /// int __thiscall CTransition::step_up(CTransition*this, AC1Legacy::Vector3* collision_normal)
        /// acclient.c 312794
        /// </summary>
        public bool StepUp(Vector3 collisionNormal)
        {
            throw new NotImplementedException();
        }
    }
}
