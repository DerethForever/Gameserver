/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// DBObj in the client
    /// </summary>
    public class DatabaseObject
    {
        /// <summary>
        /// +0x004 m_dataCategory   : Uint4B
        /// </summary>
        public uint DataCategory;

        /// <summary>
        /// +0x008 m_bLoaded        : Bool
        /// </summary>
        public uint IsLoaded;

        /// <summary>
        /// +0x010 m_timeStamp      : Float
        /// </summary>
        public float Timestamp;

        /// <summary>
        /// +0x018 m_pNext          : Ptr32 DBObj
        /// </summary>
        public uint Next;

        /// <summary>
        /// +0x01c m_pLast          : Ptr32 DBObj
        /// </summary>
        public uint Last;

        /// <summary>
        /// +0x020 m_pMaintainer    : Ptr32 DBOCache
        /// </summary>
        public uint Maintainer;

        /// <summary>
        /// +0x024 m_numLinks       : Int4B
        /// </summary>
        public int NumLinks;

        /// <summary>
        /// property instead of a field because it's part of the interface
        /// +0x028 m_DID            : IDClass
        /// </summary>
        public uint Id { get; set; }

        /// <summary>
        /// +0x02c m_AllowedInFreeList : Bool
        /// </summary>
        public bool AllowedInFreeList;
    }
}
