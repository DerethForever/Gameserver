/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.IO;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// GfxObjInfo in the client
    /// </summary>
    public class GraphicsObjectInfo
    {
        /// <summary>
        /// 0x000 gfxobj_id : IDClass
        /// </summary>
        public uint GraphicsObjectId { get; private set; }

        /// <summary>
        /// 0x004 degrade_mode : Int4B
        /// </summary>
        public int DegradeMode { get; set; }

        /// <summary>
        /// 0x008 min_dist : Float
        /// </summary>
        public float MinimumDistance { get; set; }

        /// <summary>
        /// 0x00c ideal_dist : Float
        /// </summary>
        public float IdealDistance { get; set; }

        /// <summary>
        /// 0x010 max_dist : Float
        /// </summary>
        public float MaxDistance { get; set; }

        /// <summary>
        /// This is indirectly called in the client
        /// We call it from GraphicsObjectDegradeInfo like the client does.
        /// </summary>
        public static GraphicsObjectInfo Unpack(BinaryReader reader)
        {
            GraphicsObjectInfo g = new GraphicsObjectInfo();
            g.GraphicsObjectId = reader.ReadUInt32();
            g.DegradeMode = reader.ReadInt32();
            g.MinimumDistance = reader.ReadSingle();
            g.IdealDistance = reader.ReadSingle();
            g.MaxDistance = reader.ReadSingle();
            return g;
        }
    }
}
