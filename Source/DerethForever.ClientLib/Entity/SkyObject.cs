/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System.IO;
using System.Numerics;

namespace DerethForever.ClientLib.Entity
{
    /// <summary>
    /// SkyObject in the client
    /// </summary>
    public class SkyObject
    {
        /// <summary>
        /// private because all instances come from Unpack
        /// </summary>
        private SkyObject()
        {
        }

        /// <summary>
        /// +0x004 begin_time       : Float
        /// </summary>
        public float BeginTime { get; set; }

        /// <summary>
        /// +0x008 end_time         : Float
        /// </summary>
        public float EndTime { get; set; }

        /// <summary>
        /// +0x00c begin_angle      : Float
        /// </summary>
        public float BeginAngle { get; set; }

        /// <summary>
        /// +0x010 end_angle        : Float
        /// </summary>
        public float EndAngle { get; set; }

        /// <summary>
        /// +0x014 tex_velocity     : AC1Legacy::Vector3
        /// </summary>
        public Vector3 TexVelocity;

        /// <summary>
        /// +0x020 properties       : Uint4B
        /// </summary>
        public uint Properties { get; set; }

        /// <summary>
        /// +0x024 default_gfx_object : IDClass<_tagDataID,32,0>
        /// </summary>
        public uint DefaultGraphicsObjectDid { get; set; }

        /// <summary>
        /// +0x028 default_pes_object : IDClass<_tagDataID,32,0>
        /// </summary>
        public uint DefaultPesObjectDid { get; set; }

        /// <summary>
        /// //----- (00500FD0) --------------------------------------------------------
        /// int __thiscall SkyObject::UnPack(SkyObject *this, void **addr, unsigned int *size)
        /// acclient.c 301789
        /// </summary>
        public static SkyObject Unpack(BinaryReader reader)
        {
            SkyObject s = new SkyObject();

            s.BeginTime = reader.ReadSingle();
            s.EndTime = reader.ReadSingle();

            s.BeginAngle = reader.ReadSingle();
            s.EndAngle = reader.ReadSingle();

            s.TexVelocity.X = reader.ReadSingle();
            s.TexVelocity.Y = reader.ReadSingle();

            s.DefaultGraphicsObjectDid = reader.ReadUInt32();
            s.DefaultPesObjectDid = reader.ReadUInt32();
            s.Properties = reader.ReadUInt32();

            return s;
        }
    }
}
