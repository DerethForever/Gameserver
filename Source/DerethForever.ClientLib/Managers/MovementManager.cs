/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using DerethForever.ClientLib.Entity;
using DerethForever.ClientLib.Enum;
using DerethForever.ClientLib.Integration;

namespace DerethForever.ClientLib.Managers
{
    public class MovementManager
    {
        /// <summary>
        /// +0x000 motion_interpreter : Ptr32 CMotionInterp
        /// </summary>
        public MotionInterp MotionInterp;

        /// <summary>
        /// +0x004 moveto_manager   : Ptr32 MoveToManager
        /// </summary>
        public MoveToManager MoveToManager;

        /// <summary>
        /// +0x008 physics_obj      : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject PhysicsObject;

        /// <summary>
        /// //+0x00c weenie_obj       : Ptr32 CWeenieObject
        /// </summary>
        public IWorldObject WeenieObject;

        // TODO: Doing this like the client now, but need to comeback and refactor.
        // There is no need for the create method - will just substitute with a new
        // Coral Golem.

        public MovementManager(PhysicsObject physicsObj, IWorldObject weenieObj)
        {
            PhysicsObject = physicsObj;
            WeenieObject = weenieObj;
            MotionInterp = new MotionInterp(physicsObj, weenieObj);
            MoveToManager = new MoveToManager(physicsObj, weenieObj);
        }

        /// <summary>
        /// ----- (00524050) --------------------------------------------------------
        /// MovementManager* __cdecl MovementManager::Create(CPhysicsObj* _physics_obj, CWeenieObject* _weenie_obj)
        /// acclient.c 339132
        /// </summary>
        public static MovementManager Create(PhysicsObject physicsObj, IWorldObject weenieObj)
        {
            return new MovementManager(physicsObj, weenieObj);
        }

        /// <summary>
        /// ----- (00524000) --------------------------------------------------------
        /// void __thiscall MovementManager::MakeMoveToManager(MovementManager*this)
        /// acclient.c 339101
        /// </summary>
        public void MakeMoveToManager()
        {
            if (MoveToManager == null)
                MoveToManager = new MoveToManager(PhysicsObject, WeenieObject);
        }
        // TODO: Coral Golem - pick up here 3/22/2018

        /// <summary>
        /// ----- (00524020) --------------------------------------------------------
        /// void __thiscall MovementManager::SetWeenieObject(MovementManager*this, CWeenieObject* _weenie_obj)
        /// acclient.c 339115
        /// </summary>
        public void SetWeenieObject(IWorldObject weenieObj)
        {
            WeenieObject = weenieObj;
            MotionInterp?.SetWeenieObject(weenieObj);
            MoveToManager?.SetWeenieObject(weenieObj);
        }

        /// <summary>
        /// ----- (005240D0) --------------------------------------------------------
        /// signed int __thiscall MovementManager::PerformMovement(MovementManager*this, MovementStruct* movement_struct)
        /// </summary>
        /// acclient.c 339175
        public Sequence PerformMovement(MovementStructure movementStructure)
        {
            PhysicsObject.SetActive(true);
            switch (movementStructure.Type)
            {
                case MovementTypes.RawCommand:
                case MovementTypes.InterpretedCommand:
                case MovementTypes.StopRawCommand:
                case MovementTypes.StopInterpretedCommand:
                case MovementTypes.StopCompletely:

                    if (MotionInterp != null)
                        return MotionInterp.PerformMovement(movementStructure);

                    MotionInterp = new MotionInterp(PhysicsObject, WeenieObject);
                    if (PhysicsObject != null)
                        MotionInterp.EnterDefaultState();
                    return MotionInterp.PerformMovement(movementStructure);

                case MovementTypes.MoveToObject:
                case MovementTypes.MoveToPosition:
                case MovementTypes.TurnToObject:
                case MovementTypes.TurnToHeading:

                    if (MoveToManager == null)
                        MoveToManager = new MoveToManager(PhysicsObject, WeenieObject);

                    return MoveToManager.PerformMovement(movementStructure);

                default:
                    return new Sequence(71);
            }
        }

        /// <summary>
        /// ----- (005241B0) --------------------------------------------------------
        /// void __thiscall MovementManager::CancelMoveTo(MovementManager*this, unsigned int err)
        /// acclient.c 339240
        /// </summary>
        public void CancelMoveTo(int err)
        {
            MoveToManager?.CancelMoveTo(err);
        }

        /// <summary>
        /// ----- (005242F0) --------------------------------------------------------
        /// void __thiscall MovementManager::UseTime(MovementManager*this)
        /// acclient.c 339359
        /// </summary>
        public void UseTime(PhysicsObject physicsObject)
        {
            //TODO: Implement me please
        }

        /// <summary>
        /// ----- (005241C0) --------------------------------------------------------
        /// void __thiscall MovementManager::EnterDefaultState(MovementManager*this)
        /// acclient.c 339250
        /// </summary>
        public void EnterDefaultState()
        {
            if (PhysicsObject == null)
                return;

            if (MotionInterp == null)
                MotionInterp = new MotionInterp(PhysicsObject, WeenieObject);

            MotionInterp.EnterDefaultState();
        }

        /// <summary>
        /// ----- (005242A0) --------------------------------------------------------
        /// CMotionInterp* __thiscall MovementManager::get_minterp(MovementManager*this)
        /// acclient.c 339330
        /// </summary>
        public MotionInterp GetMotionInterp()
        {
            if (MotionInterp == null)
            {
                MotionInterp = new MotionInterp(PhysicsObject, WeenieObject);
                if (PhysicsObject != null)
                    MotionInterp.EnterDefaultState();
            }
            return MotionInterp;
        }
    }
}
