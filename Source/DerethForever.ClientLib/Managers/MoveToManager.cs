/************************************************************************
Copyright 2018 Dereth Forever - https://www.derethforever.com

MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************/
using System;
using System.Collections.Generic;
using DerethForever.ClientLib.Enum;
using DerethForever.ClientLib.Integration;

namespace DerethForever.ClientLib.Entity
{
    public class MoveToManager
    {
        /// <summary>
        /// +0x000 movement_type    : MovementTypes::Type
        /// </summary>
        public MovementTypes MovementType { get; set; }

        /// <summary>
        /// +0x004 sought_position  : Position
        /// </summary>
        public Position SoughtPosition { get; set; }

        /// <summary>
        /// +0x04c current_target_position : Position
        /// </summary>
        public Position CurrentTargetPosition { get; set; }

        /// <summary>
        /// +0x094 starting_position : Position
        /// </summary>
        public Position StartingPosition { get; set; }

        /// <summary>
        /// +0x0dc movement_params  : MovementParameters
        /// </summary>
        public MovementParameters MovementParams { get; set; }

        /// <summary>
        /// +0x108 previous_heading : Float
        /// </summary>
        public float PreviousHeading { get; set; }

        /// <summary>
        /// +0x10c previous_distance : Float
        /// </summary>
        public float PreviousDistance { get; set; }

        /// <summary>
        /// +0x110 previous_distance_time : Float
        /// </summary>
        public double PreviousDistanceTime { get; set; }

        /// <summary>
        /// +0x118 original_distance : Float
        /// </summary>
        public float OriginalDistance { get; set; }

        /// <summary>
        /// +0x120 original_distance_time : Float
        /// </summary>
        public double OriginalDistanceTime { get; set; }

        /// <summary>
        /// +0x128 fail_progress_count : Uint4B
        /// </summary>
        public uint FailProgressCount { get; set; }

        /// <summary>
        /// +0x12c sought_object_id : Uint4B
        /// </summary>
        public uint SoughtObjectId { get; set; }

        /// <summary>
        /// +0x130 top_level_object_id : Uint4B
        /// </summary>
        public uint TopLevelObjectId { get; set; }

        /// <summary>
        /// +0x134 sought_object_radius : Float
        /// </summary>
        public float SoughtObjectRadius { get; set; }

        /// <summary>
        /// +0x138 sought_object_height : Float
        /// </summary>
        public float SoughtObjectHeight { get; set; }

        /// <summary>
        /// +0x13c current_command  : Uint4B
        /// </summary>
        public MotionCommand CurrentCommand { get; set; }

        /// <summary>
        /// +0x13c current_command  : Uint4B
        /// </summary>
        public MotionCommand AuxCommand { get; set; }

        /// <summary>
        /// +0x144 moving_away      : Int4B
        /// </summary>
        public bool MovingAway { get; set; }

        /// <summary>
        /// +0x148 initialized      : Int4B
        /// </summary>
        public bool Initialized { get; set; }

        /// <summary>
        /// +0x14c pending_actions  : DLList<MoveToManager::MovementNode>
        /// </summary>
        public List<MovementNode> PendingActions { get; set; }

        /// <summary>
        /// +0x154 physics_obj      : Ptr32 CPhysicsObj
        /// </summary>
        public PhysicsObject PhysicsObject { get; set; }

        /// <summary>
        /// +0x158 weenie_obj       : Ptr32 CWeenieObject
        /// </summary>
        public IWorldObject WeenieObject { get; set; }

        public MoveToManager()
        {
            InitializeLocalVars();
        }

        public MoveToManager(PhysicsObject obj, IWorldObject wobj)
        {
            PhysicsObject = obj;
            WeenieObject = wobj;
            InitializeLocalVars();
        }

        //TODO: These are kind of worthless - would it be better to just call the .add to PendingActions list?   Coral Golem

        /// <summary>
        /// ----- (00529580) --------------------------------------------------------
        /// void __thiscall MoveToManager::AddMoveToPositionNode(MoveToManager*this)
        /// acclient.c 345120
        /// </summary>
        public void AddMoveToPositionNode()
        {
            PendingActions.Add(new MovementNode(MovementTypes.MoveToPosition));
        }

        /// <summary>
        /// ----- (00529530) --------------------------------------------------------
        /// void __thiscall MoveToManager::AddTurnToHeadingNode(MoveToManager*this, float global_heading)
        /// acclient.c 345096
        /// </summary>
        public void AddTurnToHeadingNode(float heading)
        {
            PendingActions.Add(new MovementNode(MovementTypes.TurnToHeading, heading));
        }

        /// <summary>
        /// ----- (00529250) --------------------------------------------------------
        /// void __thiscall MoveToManager::InitializeLocalVariables(MoveToManager*this)
        /// acclient.c 344913
        /// </summary>
        public void InitializeLocalVars()
        {
            MovementType = MovementTypes.Invalid;

            MovementParams = new MovementParameters();

            PreviousDistanceTime = Timer.CurrentTime;
            OriginalDistanceTime = Timer.CurrentTime;

            PreviousHeading = 0.0f;

            FailProgressCount = 0;
            CurrentCommand = 0;
            AuxCommand = 0;
            MovingAway = false;
            Initialized = false;

            SoughtPosition = new Position();
            CurrentTargetPosition = new Position();

            SoughtObjectId = 0;
            TopLevelObjectId = 0;
            SoughtObjectRadius = 0;
            SoughtObjectHeight = 0;
        }

        /// <summary>
        /// ----- (00529240) --------------------------------------------------------
        /// void __thiscall MoveToManager::SetPhysicsObject(MoveToManager*this, CPhysicsObj* pobj)
        /// acclient.c 344908
        /// </summary>
        public void SetPhysicsObject(PhysicsObject physicsObj)
        {
            PhysicsObject = physicsObj;
        }

        /// <summary>
        /// //----- (00529230) --------------------------------------------------------
        /// void __thiscall MoveToManager::SetWeenieObject(MoveToManager*this, CWeenieObject* wobj)
        /// acclient.c 344902
        /// </summary>
        public void SetWeenieObject(IWorldObject weenieObj)
        {
            WeenieObject = weenieObj;
        }

        /// <summary>
        /// ----- (00529930) --------------------------------------------------------
        /// void __thiscall MoveToManager::CancelMoveTo(MoveToManager*this, unsigned int retval)
        /// acclient.c 345298
        /// </summary>
        public void CancelMoveTo(int retval)
        {
            if (MovementType == MovementTypes.Invalid)
                return;

            PendingActions.Clear();
            CleanUpAndCallWeenie(retval);
        }

        /// <summary>
        /// ----- (00529650) --------------------------------------------------------
        /// void __thiscall MoveToManager::CleanUpAndCallWeenie(MoveToManager*this, unsigned int status)
        /// acclient.c 345172
        /// </summary>
        public void CleanUpAndCallWeenie(int status)
        {
            CleanUp();
            PhysicsObject?.StopCompletely(false);
        }

        /// <summary>
        /// ----- (005295C0) --------------------------------------------------------
        /// void __thiscall MoveToManager::CleanUp(MoveToManager*this)
        /// acclient.c 345143
        /// </summary>
        public void CleanUp()
        {
            MovementParameters movementParams = new MovementParameters
            {
                HoldKeyToApply = MovementParams.HoldKeyToApply,
                CancelMoveTo = false
            };

            if (PhysicsObject != null)
            {
                if (CurrentCommand != 0)
                    StopMotion(CurrentCommand, movementParams);

                if (AuxCommand != 0)
                    StopMotion(AuxCommand, movementParams);

                if (TopLevelObjectId != 0 && MovementType != MovementTypes.Invalid)
                    PhysicsObject.ClearTarget();
            }
            InitializeLocalVars();
        }

        /// <summary>
        /// ----- (0052A900) --------------------------------------------------------
        /// unsigned int __thiscall MoveToManager::PerformMovement(MoveToManager*this, MovementStruct* mvs)
        /// </summary>
        public Sequence PerformMovement(MovementStructure movementStructure)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ----- (0052A240) --------------------------------------------------------
        /// void __thiscall MoveToManager::MoveToPosition(MoveToManager*this, Position* p, MovementParameters*params)
        /// acclient.c 339585
        /// </summary>
        public void MoveToPosition(ref Position postion, ref MovementParameters movementParameters)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ----- (00529D80) --------------------------------------------------------
        /// void __thiscall MoveToManager::HandleMoveToPosition(MoveToManager*this)
        /// acclient.c 345577
        /// </summary>
        public void HandleMoveToPosition()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ----- (0052A0C0) --------------------------------------------------------
        /// void __thiscall MoveToManager::HandleTurnToHeading(MoveToManager*this)
        /// acclient.c 345712
        /// </summary>
        public void HandleTurnToHeading()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ----- (0052A780) --------------------------------------------------------
        /// void __thiscall MoveToManager::UseTime(MoveToManager*this)
        /// acclient.c 346018
        /// </summary>
        public void UseTime()
        {
            if (PhysicsObject == null || !PhysicsObject.TransientState.HasFlag(TransientState.Contact))
                return;

            if (TopLevelObjectId == 0 || MovementType == MovementTypes.Invalid || !Initialized)
                return;

            foreach (MovementNode pendingAction in PendingActions)
            {
                switch (pendingAction.Type)
                {
                    case MovementTypes.MoveToPosition:
                        HandleMoveToPosition();
                        break;
                    case MovementTypes.TurnToHeading:
                        HandleTurnToHeading();
                        break;
                }
            }
        }

        /// <summary>
        /// ----- (00529080) --------------------------------------------------------
        /// signed int __thiscall MoveToManager::_StopMotion(MoveToManager*this, unsigned int motion, MovementParameters*params)
        /// acclient.c 344793
        /// </summary>
        public Sequence StopMotion(MotionCommand motion, MovementParameters movementParams)
        {
            if (PhysicsObject == null)
                return new Sequence(8);

            MotionInterp motionInterp = PhysicsObject.GetMotionInterp();
            if (motionInterp == null)
                return new Sequence(11);

            motionInterp.AdjustMotion(motion, movementParams.Speed, movementParams.HoldKeyToApply);
            return motionInterp.StopInterpretedMotion(motion, movementParams);
        }

    }
}
