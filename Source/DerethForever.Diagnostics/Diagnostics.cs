﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Entity;
using DerethForever.Entity.Enum;

namespace DerethForever.Diagnostics
{
    public static class Diagnostics
    {
        private static readonly object landBlockMutex = new object();
        private static LandBlockStatus[,] landBlockInfos = new LandBlockStatus[256, 256];
        public static bool LandBlockDiag = false;

        public static void SetLandBlockKeys(LandBlockStatus[,] info)
        {
            lock (landBlockMutex)
            {
                landBlockInfos = info;
            }
        }

        public static LandBlockStatus[,] GetLandBlockKeys(LandBlockStatus[,] infos)
        {
            return landBlockInfos;
        }

        public static void SetLandBlockKey(int col, int row, LandBlockStatus info)
        {
            lock (landBlockMutex)
            {
                landBlockInfos[col, row] = info;
            }
        }

        public static LandBlockStatusFlag GetLandBlockKeyFlag(int col, int row)
        {
            if (landBlockInfos[col, row] == null)
                return LandBlockStatusFlag.IdleUnloaded;
            else
                return landBlockInfos[col, row].LandBlockStatusFlag;
        }

        public static LandBlockStatus GetLandBlockKey(int col, int row)
        {
            if (landBlockInfos[col, row] == null)
                return null;
            else
                return landBlockInfos[col, row];
        }
    }
}
