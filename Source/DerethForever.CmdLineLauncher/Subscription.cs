﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using Newtonsoft.Json;
using System;

namespace DerethForever.CmdLineLauncher
{
    public class Subscription
    { 
        [JsonProperty("subscriptionId")]
        public uint SubscriptionId { get; set; }

        [JsonProperty("subscriptionGuid")]
        public Guid SubscriptionGuid { get; set; }

        [JsonProperty("accountGuid")]
        public Guid AccountGuid { get; set; }

        [JsonProperty("accessLevel")]
        public AccessLevel AccessLevel { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
