/************************************************************************

    DerethForever.com
    Copyright (C) 2018 Dereth Forever Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of
the GNU General Public License, any modifications of this work must
retain the text of this header, including all copyright authors, dates,
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Configuration;
using System.IO;
using DerethForever.ClientLib.DatUtil;
using DerethForever.ClientLib.Entity;
using DerethForever.ClientLib.Enum;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DerethForever.ClientLib.Tests
{
    [TestClass]
    public class DatReaderTests
    {
        private static string _datFolder;
        private static string _portalDatFilename = "client_portal.dat";
        private static string _portalDat;
        private static string _cellDatFilename = "client_cell_1.dat";
        private static string _cellDat;

        [ClassInitialize]
        public static void TestSetup(TestContext context)
        {
            _datFolder = ConfigurationManager.AppSettings["DatFolder"];
            _portalDat = Path.Combine(_datFolder, _portalDatFilename);
            _cellDat = Path.Combine(_datFolder, _cellDatFilename);
        }

        [TestMethod]
        public void DatReader_PortalDat_BuildsTableOfContents()
        {
            if (!File.Exists(_portalDat))
                Assert.Inconclusive($"Unable to find {_portalDatFilename}");

            DatReader dr = new DatReader(_portalDat);

            Assert.IsNotNull(dr.TableOfContents, $"{_portalDatFilename} table of contents is null");
            Console.WriteLine($"{_portalDat} has {dr.TableOfContents.Count} files.");
        }

        [TestMethod]
        public void DatReader_CellDat_BuildsTableOfContents()
        {
            if (!File.Exists(_cellDat))
                Assert.Inconclusive($"Unable to find {_cellDatFilename}");

            DatReader dr = new DatReader(_cellDat);

            Assert.IsNotNull(dr.TableOfContents, $"{_cellDatFilename} table of contents is null");
            Console.WriteLine($"{_cellDat} has {dr.TableOfContents.Count} files.");
        }

        [TestMethod]
        public void SetupModel_Unpack_LoadsCleanly()
        {
            if (!File.Exists(_portalDat))
                Assert.Inconclusive($"Unable to find {_portalDat}");

            DatReader dr = new DatReader(_portalDat);

            var behemoth = dr.Unpack<SetupModel>(33556427);

            Assert.IsNotNull(behemoth);
        }

        [TestMethod]
        public void PortalDatReader_Ctor_LoadsRegionData()
        {
            if (!File.Exists(_portalDat))
                Assert.Inconclusive($"Unable to find {_portalDatFilename}");

            uint regionId = 0x13000000;

            PortalDatReader.Initialize(_portalDat);

            RegionDescription rd = PortalDatReader.Current.Unpack<RegionDescription>(regionId);

            Assert.IsNotNull(rd);
        }

        [TestMethod]
        public void DatReader_Ctor_LoadsAllItems()
        {
            if (!File.Exists(_cellDat))
                Assert.Inconclusive($"Unable to find {_cellDatFilename}");

            DatReader dr = new DatReader(_cellDat);

            Assert.AreEqual(805348, dr.TableOfContents.Count, "table of contents is missing stuff!");
        }

        [TestMethod]
        public void TabooFile_Load_WithoutError()
        {
            uint fileId = 0x0E00001E;

            if (!File.Exists(_portalDat))
                Assert.Inconclusive($"Unable to find {_portalDatFilename}");

            PortalDatReader.Initialize(_portalDat);

            TabooTable tt = PortalDatReader.Current.Unpack<TabooTable>(fileId);
        }

        [TestMethod]
        public void MotionTable_Unpack_LoadsCleanly()
        {
            if (!File.Exists(_portalDat))
                Assert.Inconclusive($"Unable to find {_portalDat}");

            DatReader dr = new DatReader(_portalDat);

            // http://ac.yotesfan.com/09-MotionTable/index.php?id=0900020A
            // Big nasty one to parse - lets test this puppy out.
            const uint bmId                        = 0x0900020A;
            const MotionCommand bmDefaultStyle     = MotionCommand.NonCombat;
            const int bmCyclesCnt                  = 0x16e;
            const int bmLinksCnt                   = 0x13e;
            const int bmModCnt                     = 0x08;
            const int bmStylesDefaultCnt           = 0x0e;

            MotionTable mt = dr.Unpack<MotionTable>(bmId);

            Assert.IsNotNull(mt);
            Assert.AreEqual(mt.Id, bmId);
            Assert.AreEqual(mt.DefaultStyle, bmDefaultStyle);
            Assert.AreEqual(mt.Cycles.Count, bmCyclesCnt);
            Assert.AreEqual(mt.Links.Count, bmLinksCnt);
            Assert.AreEqual(mt.Modifiers.Count, bmModCnt);
            Assert.AreEqual(mt.StyleDefaults.Count, bmStylesDefaultCnt);
        }

        [TestMethod]
        public void Animation_Unpack_LoadsCleanly()
        {
            if (!File.Exists(_portalDat))
                Assert.Inconclusive($"Unable to find {_portalDat}");

            DatReader dr = new DatReader(_portalDat);
            // http://ac.yotesfan.com/03-animation/index_.php?id=030004AB

            const uint animId = 0x030004ab;

            Animation anim = dr.Unpack<Animation>(animId);

            Assert.IsNotNull(anim);
        }

        [TestMethod]
        public void AnimationSequenceNode_Unpack_LoadsCleanly()
        {
            if (!File.Exists(_portalDat))
                Assert.Inconclusive($"Unable to find {_portalDat}");

            DatReader dr = new DatReader(_portalDat);
            // http://ac.yotesfan.com/09-MotionTable/index.php?id=09000003

            const uint animId = 0x03000235;

            AnimationSequenceNode anim = dr.Unpack<AnimationSequenceNode>(animId);

            Assert.IsNotNull(anim);
        }

        [TestMethod]
        public void MotionTable_GetLink()
        {
            if (!File.Exists(_portalDat))
                Assert.Inconclusive($"Unable to find {_portalDat}");

            DatReader dr = new DatReader(_portalDat);

            // http://ac.yotesfan.com/09-MotionTable/index.php?id=09000001
            // Human - lets test this puppy out.
            const uint bmId = 0x09000001;

            MotionTable mt = dr.Unpack<MotionTable>(bmId);

            Assert.IsNotNull(mt);
            Assert.AreEqual(mt.Id, bmId);
            MotionData motion = mt.GetLink(MotionCommand.NonCombat, MotionCommand.Ready, 1.0f, MotionCommand.Falling, 1.0f);
            Assert.IsNotNull(motion);
            Assert.AreEqual(motion.Animations[0].AnimationId, (uint)0x030004aa);
        }

        [TestMethod]
        public void PhysicsScript_Unpack_LoadsCleanly()
        {
            if (!File.Exists(_portalDat))
                Assert.Inconclusive($"Unable to find {_portalDat}");

            DatReader dr = new DatReader(_portalDat);
            const uint id = 0x33000007;
            PhysicsScript ps = dr.Unpack<PhysicsScript>(id);

            Assert.IsNotNull(ps);
        }
        [TestMethod]
        public void Landblock_LoadAndGenerate_WithoutError()
        {
            uint landblockId = 0xC98C0000;

            if (!File.Exists(_cellDat))
                Assert.Inconclusive($"Unable to find {_cellDatFilename}");

            CellDatReader.Initialize(_cellDat);
            PortalDatReader.Initialize(_portalDat);

            Landblock landblock = Landblock.Load(landblockId);

            Assert.IsNotNull(landblock);
            Assert.IsTrue(landblock.HeightMap[0, 0] != 0, "Landblock heightmap is hosed");
            Assert.AreEqual(6, landblock.Buildings.Count, "Landblock buildings did not load.");
            Assert.AreEqual(81, landblock.VertexArray.Vertices.Length, "Landblock vertex array is hosed");
            Assert.AreEqual(128, landblock.Polygons.Count, "Landblock polygons are not populated correctly.");
            Assert.AreEqual(64, landblock.LandblockCells.Length, "Landblock cells are not populated correctly.");
            Assert.AreEqual(53, landblock.LandblockInfo.Cells.Count, "Environment cells are not populated correctly.");
        }
    }
}
