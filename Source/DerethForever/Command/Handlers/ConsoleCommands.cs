/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;

using DerethForever.Database;
using DerethForever.DatLoader;
using DerethForever.DatLoader.FileTypes;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using DerethForever.Managers;
using DerethForever.Network;

namespace DerethForever.Command.Handlers
{
    public static class ConsoleCommands
    {
        [CommandHandler("cell-export", AccessLevel.Admin, CommandHandlerFlag.ConsoleInvoke, 1, "Export contents of CELL DAT file.", "<export-directory-without-spaces>")]
        public static void ExportCellDatContents(Session session, params string[] parameters)
        {
            if (parameters?.Length != 1)
                Console.WriteLine("cell-export <export-directory-without-spaces>");

            string exportDir = parameters[0];

            Console.WriteLine($"Exporting cell.dat contents to {exportDir}.  This can take longer than an hour.");
            DatManager.CellDat.ExtractLandblockContents(exportDir);
            Console.WriteLine($"Export of cell.dat to {exportDir} complete.");
        }

        [CommandHandler("portal-export", AccessLevel.Admin, CommandHandlerFlag.ConsoleInvoke, 1, "Export contents of PORTAL DAT file.", "<export-directory-without-spaces>")]
        public static void ExportPortalDatContents(Session session, params string[] parameters)
        {
            if (parameters?.Length != 1)
                Console.WriteLine("portal-export <export-directory-without-spaces>");

            string exportDir = parameters[0];

            Console.WriteLine($"Exporting portal.dat contents to {exportDir}.  This will take a while.");
            DatManager.PortalDat.ExtractCategorizedContents(exportDir);
            Console.WriteLine($"Export of portal.dat to {exportDir} complete.");
        }

        [CommandHandler("loadALB", AccessLevel.Admin, CommandHandlerFlag.ConsoleInvoke, 0, "Loads all 65k+ Landblocks, Caution.. it takes a very long time")]
        public static void LoadLALB(Session session, params string[] parameters)
        {
            Console.WriteLine($"Loading ALL Landblocks..  This will take a while.  type abortALB to stop");
            LandblockLoader.StartLoading();
        }

        [CommandHandler("abortALB", AccessLevel.Admin, CommandHandlerFlag.ConsoleInvoke, 0, "Aborts ALL Landblock loading process")]
        public static void AbortLL(Session session, params string[] parameters)
        {
            Console.WriteLine($"Landblock load aborting");
            LandblockLoader.StopLoading();
        }

        [CommandHandler("loadLB", AccessLevel.Admin, CommandHandlerFlag.ConsoleInvoke, 1, "Loads Landblock by LandblockId")]
        public static void LoadLandBlock(Session session, params string[] parameters)
        {
            try
            {
                uint rawid;
                if (!uint.TryParse(parameters[0], out rawid))
                    return;
                LandblockManager.ForceLoadLandBlock(new LandblockId((rawid) << 16));
            }
            catch
            {
                Console.WriteLine($"Invalid LandblockId");
            }
        }

        [CommandHandler("diag", AccessLevel.Admin, CommandHandlerFlag.ConsoleInvoke, 0, "Launches Landblock Diagnostic Monitor")]
        public static void Diag(Session session, params string[] parameters)
        {
            Diagnostics.Diagnostics.LandBlockDiag = true;
            Diagnostics.Properties.Common.Monitor.ShowDialog();
        }

        /// <summary>
        /// Export all wav files to a specific directory.
        /// </summary>
        [CommandHandler("wave-export", AccessLevel.Admin, CommandHandlerFlag.ConsoleInvoke, 0, "Export Wave Files")]
        public static void CMT(Session session, params string[] parameters)
        {
            if (parameters?.Length != 1)
            {
                Console.WriteLine("wave-export <export-directory-without-spaces>");
                return;
            }

            string exportDir = parameters[0];

            Console.WriteLine($"Exporting portal.dat WAV files to {exportDir}.  This may take a while.");
            foreach (KeyValuePair<uint, DatFile> entry in DatManager.PortalDat.AllFiles)
            {
                if (entry.Value.GetFileType() == DatFileType.Wave)
                {
                    DatLoader.FileTypes.Wave.ExportWave(entry.Value.ObjectId, exportDir);
                }
            }
            Console.WriteLine($"Export to {exportDir} complete.");
        }

        [CommandHandler("redeploy-world", AccessLevel.Developer, CommandHandlerFlag.ConsoleInvoke, 1, "Download and redeploy the world content.")]
        public static void RedeployWorld(Session session, params string[] parameters)
        {
            bool forceRedploy = false;
            var sourceSelection = new ResourceSelectionOption();
            var userModifiedFlagPresent = DatabaseManager.World.UserModifiedFlagPresent();

            // Match parameters
            if (parameters?.Length == 2)
            {
                foreach (string sourceSelectionItem in System.Enum.GetNames(typeof(ResourceSelectionOption)))
                {
                    if (parameters[0].ToLower() == sourceSelectionItem.ToLower())
                    {
                        if (Enum.TryParse(sourceSelectionItem, out sourceSelection))
                            break;
                    }
                }

                string force = parameters[1];
                if (force.Length > 0)
                {
                    if (force.ToLowerInvariant().Contains("force"))
                    {
                        Console.WriteLine("Force redeploy reached!");
                        forceRedploy = true;
                    }
                }
            }
            if (forceRedploy || !userModifiedFlagPresent)
            {
                string errorResult = Redeploy.RedeployDatabaseFromSource(DatabaseSelectionOption.World, sourceSelection);
                if (errorResult == null)
                {
                    Console.WriteLine("The World Database has been deployed!");
                    ServerManager.RestartServer();
                }
                else
                    Console.WriteLine($"There was an error durring your request. {errorResult}");
                return;
            }
            Console.WriteLine("User created content has been detected in the database. Please export the current database or include the 'force' parameter with this command.");
        }

        [CommandHandler("redeploy", AccessLevel.Developer, CommandHandlerFlag.ConsoleInvoke, 2,
            "Downloads and redeploys database content. WARNING: THIS CAN WIPE DATA!",
            "<datbase selection> <source selection> <force>\n\nYou must pass in a database selection and a source selection, but the force string is optional.\nDatabase Selection Options include: None, Authentication, Shard, World, All.\nSource Selections include: LocalDisk and Gitlab.\n\nWARNING: THIS COMMAND MAY RESULT IN LOST DATA!")]
        public static void RedeployAllDatabases(Session session, params string[] parameters)
        {
            if (parameters?.Length < 2 && parameters?.Length > 3)
            {
                Console.WriteLine("Usage: redeploy <datbase selection> <source selection> force");
                return;
            }

            var sourceSelection = new DatabaseSelectionOption();
            var resourceSelection = new ResourceSelectionOption();
            bool forceRedploy = false;

            if (parameters?.Length > 0)
            {
                // Loop through the enum to attempt at matching the first parameter with an option
                foreach (string dbSelection in System.Enum.GetNames(typeof(DatabaseSelectionOption)))
                {
                    if (parameters[0].ToLower() == dbSelection.ToLower())
                    {
                        if (Enum.TryParse(dbSelection, out sourceSelection))
                            break;
                    }
                }

                // Loop through the enum to attempt at matching the second parameter with an option
                foreach (string sourceSelectionItem in System.Enum.GetNames(typeof(ResourceSelectionOption)))
                {
                    if (parameters[1].ToLower() == sourceSelectionItem.ToLower())
                    {
                        if (Enum.TryParse(sourceSelectionItem, out resourceSelection))
                            break;
                    }
                }

                if (parameters?.Length > 2)
                {
                    if (parameters[2].ToLowerInvariant().Contains("force"))
                    {
                        Console.WriteLine("Force redeploy reached!");
                        forceRedploy = true;
                    }
                }
            }
            var userModifiedFlagPresent = DatabaseManager.World.UserModifiedFlagPresent() && (sourceSelection == DatabaseSelectionOption.All) ? true : false;
            if (forceRedploy || !userModifiedFlagPresent)
            {
                string errorResult = Database.Redeploy.RedeployDatabaseFromSource(sourceSelection, resourceSelection);

                if (errorResult == null)
                {
                    Console.WriteLine("All databases have been redeployed!");
                    ServerManager.RestartServer();
                }
                else
                    Console.WriteLine($"There was an error durring your request. {errorResult}");
                return;
            }
            Console.WriteLine("User modified objects were found in the database.\nYou must also pass the 'force' parameter with this command, to start the database reset process.");
        }

        [CommandHandler("download-gitlab-content", AccessLevel.Developer, CommandHandlerFlag.ConsoleInvoke, 0,
            "Downloads content from gitlab.",
            "")]
        public static void DownloadGitlabContent(Session session, params string[] parameters)
        {
            Redeploy.GetResources(ResourceSelectionOption.Gitlab);
            Console.WriteLine("...finished!");
        }

        /// <summary>
        /// Saves a 0x06-RenderSurface from the Portal.dat to the specified folder.
        /// </summary>
        [CommandHandler("get-texture", AccessLevel.Developer, CommandHandlerFlag.ConsoleInvoke, 0, "Saves a 0x06-RenderSurface as a PNG file to the specified folder")]
        public static void Texture(Session session, params string[] parameters)
        {
            if (parameters?.Length < 2)
            {
                Console.WriteLine(@"texture <texture_id> <save-directory-without-spaces>");
                return;
            }

            uint textureId;
            if (parameters[0].StartsWith("0x"))
            {
                string tmp = parameters[0].Substring(2);
                textureId = UInt32.Parse(tmp, System.Globalization.NumberStyles.HexNumber);
            }
            else
                textureId = UInt32.Parse(parameters[0], System.Globalization.NumberStyles.HexNumber);

            string exportDir = parameters[1];

            RenderSurface r = RenderSurface.ReadFromDat(textureId);
            if (r.Colors.Count > 0)
            {
                System.Drawing.Bitmap image = r.GetBitmap();

                string filename = System.IO.Path.Combine(exportDir, textureId.ToString("X8") + ".png");
                image.Save(filename, System.Drawing.Imaging.ImageFormat.Png);
                Console.WriteLine("File saved to " + filename);
            }
            else
                Console.WriteLine("Unable to save file.");
        }
    }
}
