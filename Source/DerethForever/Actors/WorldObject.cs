/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using DerethForever.DatLoader.FileTypes;
using DerethForever.Actors.Actions;
using DerethForever.Database;
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using DerethForever.Entity.Enum.Properties;
using DerethForever.Managers;
using DerethForever.Network;
using DerethForever.Network.Enum;
using DerethForever.Network.GameEvent.Events;
using DerethForever.Network.GameMessages;
using DerethForever.Network.GameMessages.Messages;
using DerethForever.Network.Motion;
using DerethForever.Network.Sequence;
using DerethForever.DatLoader.Entity;

namespace DerethForever.Actors
{
    public abstract class WorldObject : IActor
    {
        private static readonly ILog log =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static float MaxObjectTrackingRange { get; } = 20000f;

        // object_id
        private ObjectGuid guid;

        public ObjectGuid Guid
        {
            get
            {
                return guid;
            }
            set
            {
                guid = new ObjectGuid(value.Full);
                DataObject.DataObjectId = value.Full;
            }
        }

        protected DataObject DataObject { get; set; }

        protected DataObject DataWeenie { get; set; }

        protected internal Dictionary<ObjectGuid, WorldObject> WieldedObjects { get; set; }

        protected internal Dictionary<ObjectGuid, WorldObject> InventoryObjects { get; set; }

        protected internal Dictionary<ObjectGuid, DataObject> Inventory
        {
            get
            {
                return DataObject.Inventory;
            }
        }

        // This dictionary is only used to load WieldedObjects and to save them.   Other than the load and save, it should never be added to or removed from.
        protected internal Dictionary<ObjectGuid, DataObject> WieldedItems
        {
            get
            {
                return DataObject.WieldedItems;
            }
        }

        // we need to expose this read only for examine to work. Og II
        public List<DataObjectPropertiesInt> PropertiesInt
        {
            get
            {
                return DataObject.IntProperties;
            }
        }

        public int? MerchandiseItemTypes
        {
            get
            {
                return DataObject.MerchandiseItemTypes;
            }
            set
            {
                DataObject.MerchandiseItemTypes = value;
            }
        }

        public int? MerchandiseMinValue
        {
            get
            {
                return DataObject.MerchandiseMinValue;
            }
            set
            {
                DataObject.MerchandiseMinValue = value;
            }
        }

        public int? MerchandiseMaxValue
        {
            get
            {
                return DataObject.MerchandiseMaxValue;
            }
            set
            {
                DataObject.MerchandiseMaxValue = value;
            }
        }

        public List<DataObjectPropertiesInt64> PropertiesInt64
        {
            get
            {
                return DataObject.Int64Properties;
            }
        }

        public List<DataObjectPropertiesBool> PropertiesBool
        {
            get
            {
                return DataObject.BoolProperties;
            }
        }

        public List<DataObjectPropertiesString> PropertiesString
        {
            get
            {
                return DataObject.StringProperties;
            }
        }

        public List<DataObjectPropertiesDouble> PropertiesDouble
        {
            get
            {
                return DataObject.DoubleProperties;
            }
        }

        public List<DataObjectPropertiesDataId> PropertiesDid
        {
            get
            {
                return DataObject.DataIdProperties;
            }
        }

        public List<DataObjectPropertiesInstanceId> PropertiesIid
        {
            get
            {
                return DataObject.InstanceIdProperties;
            }
        }

        public List<DataObjectPropertiesSpell> PropertiesSpellId
        {
            get
            {
                return DataObject.SpellIdProperties;
            }
        }

        public List<DataObjectPropertiesBook> PropertiesBook
        {
            get
            {
                return DataObject.BookProperties;
            }
        }
        
        private readonly List<ModelPalette> modelPalettes = new List<ModelPalette>();

        private readonly List<ModelTexture> modelTextures = new List<ModelTexture>();

        private readonly List<Model> models = new List<Model>();

        // subpalettes
        public List<ModelPalette> GetPalettes
        {
            get
            {
                return modelPalettes.ToList();
            }
        }

        // tmChanges
        public List<ModelTexture> GetTextures
        {
            get
            {
                return modelTextures.ToList();
            }
        }

        // apChanges
        public List<Model> GetModels
        {
            get
            {
                return models.ToList();
            }
        }

        public void AddPalette(uint paletteId, ushort offset, ushort length)
        {
            ModelPalette newpalette = new ModelPalette(paletteId, offset, length);
            modelPalettes.Add(newpalette);
        }

        public void AddTexture(byte index, uint oldtexture, uint newtexture)
        {
            ModelTexture nextTexture = new ModelTexture(index, oldtexture, newtexture);
            modelTextures.Add(nextTexture);
        }

        public void AddModel(byte index, uint modelresourceid)
        {
            Model newmodel = new Model(index, modelresourceid);
            models.Add(newmodel);
        }

        public void ClearObjDesc()
        {
            modelPalettes.Clear();
            modelTextures.Clear();
            models.Clear();
        }

        // START of Logical Model Data

        public uint? PaletteBaseId
        {
            get
            {
                return DataObject.PaletteBaseDID;
            }
            set
            {
                DataObject.PaletteBaseDID = value;
            }
        }

        // PhysicsData Logical

        // bitfield
        public PhysicsDescriptionFlag PhysicsDescriptionFlag
        {
            get
            {
                return SetPhysicsDescriptionFlag();
            }
            protected internal set
            {
                DataObject.PhysicsDescriptionFlag = (uint)SetPhysicsDescriptionFlag();
            }
        }

        // state
        public PhysicsState PhysicsState
        {
            get
            {
                return (PhysicsState)DataObject.PhysicsState;
            }
            set
            {
                DataObject.PhysicsState = (int)value;
            }
        }

        /// <summary>
        /// setup_id in aclogviewer - used to get the correct model out of the dat file
        /// </summary>
        public uint? SetupTableId
        {
            get
            {
                return DataObject.SetupDID;
            }
            set
            {
                DataObject.SetupDID = value;
            }
        }

        /// <summary>
        /// mtable_id in aclogviewer This is the sound table for the object.   Looked up from dat file.
        /// </summary>
        public uint? MotionTableId
        {
            get
            {
                return DataObject.MotionTableDID;
            }
            set
            {
                DataObject.MotionTableDID = value;
            }
        }

        /// <summary>
        /// stable_id in aclogviewer This is the sound table for the object.   Looked up from dat file.
        /// </summary>
        public uint? SoundTableId
        {
            get
            {
                return DataObject.SoundTableDID;
            }
            set
            {
                DataObject.SoundTableDID = value;
            }
        }

        /// <summary>
        /// phstable_id in aclogviewer This is the physics table for the object.   Looked up from dat file.
        /// </summary>
        public uint? PhysicsTableId
        {
            get
            {
                return DataObject.PhysicsEffectTableDID;
            }
            set
            {
                DataObject.PhysicsEffectTableDID = value;
            }
        }

        public int? ParentLocation
        {
            get
            {
                return DataObject.ParentLocation;
            }
            set
            {
                DataObject.ParentLocation = value;
            }
        }

        public int? ArmorLevel
        {
            get
            {
                return DataObject.ArmorLevel;
            }
            set
            {
                DataObject.ArmorLevel = value;
            }
        }

        public List<HeldItem> Children { get; } = new List<HeldItem>();

        public float? ObjScale
        {
            get
            {
                return DataObject.DefaultScale;
            }
            set
            {
                DataObject.DefaultScale = value;
            }
        }

        public float? Friction
        {
            get
            {
                return DataObject.Friction;
            }
            set
            {
                DataObject.Friction = value;
            }
        }

        public float? Elasticity
        {
            get
            {
                return DataObject.Elasticity;
            }
            set
            {
                DataObject.Elasticity = value;
            }
        }

        public int? AnimationFrame
        {
            get
            {
                return DataObject.PlacementPosition;
            }
            set
            {
                DataObject.PlacementPosition = value;
            }
        }

        public DFVector3 Acceleration { get; set; }

        public float? Translucency
        {
            get
            {
                return DataObject.Translucency;
            }
            set
            {
                DataObject.Translucency = value;
            }
        }

        public DFVector3 Velocity = null;

        public DFVector3 Omega = null;

        // movement_buffer
        public MotionState CurrentMotionState { get; set; }

        public uint? DefaultScriptId
        {
            get
            {
                return Script;
            }
            set
            {
                Script = (ushort?)value;
            }
        }

        public float? DefaultScriptIntensity
        {
            get
            {
                return DataObject.PhysicsScriptIntensity;
            }
            set
            {
                DataObject.PhysicsScriptIntensity = value;
            }
        }

        // pos
        public virtual Position Location
        {
            get
            {
                return DataObject.Location;
            }
            set
            {
                /*
                log.Debug($"{Name} moved to {Position}");

                Position = value;
                */
                if (DataObject.Location != null)
                    LastUpdatedTicks = WorldManager.PortalYearTicks;
                DataObject.Location = value;
            }
        }

        public UpdatePositionFlag PositionFlag { get; set; }

        // bitfield
        public WeenieHeaderFlag WeenieFlags
        {
            get
            {
                return SetWeenieHeaderFlag();
            }
            protected internal set
            {
                DataObject.WeenieHeaderFlags = (uint)value;
            }
        }

        // bitfield2
        public WeenieHeaderFlag2 WeenieFlags2
        {
            get
            {
                WeenieHeaderFlag2 flags = SetWeenieHeaderFlag2();
                if (flags != WeenieHeaderFlag2.None)
                    IncludesSecondHeader = true;
                return flags;
            }
            protected internal set
            {
                DataObject.WeenieHeaderFlags2 = (uint)value;
            }
        }

        public string Name
        {
            get
            {
                return DataObject.Name;
            }
            protected set
            {
                DataObject.Name = value;
            }
        }

        /// <summary>
        /// wcid - stands for weenie class id
        /// </summary>
        public uint WeenieClassId
        {
            get
            {
                return DataObject.WeenieClassId;
            }
            protected set
            {
                DataObject.WeenieClassId = value;
            }
        }

        public uint? IconId
        {
            get
            {
                return DataObject.IconDID;
            }
            set
            {
                DataObject.IconDID = value;
            }
        }

        // type
        public ItemType ItemType
        {
            get
            {
                return (ItemType?)DataObject.ItemType ?? 0;
            }
            protected set
            {
                DataObject.ItemType = (int)value;
            }
        }

        // header
        public ObjectDescriptionFlag DescriptionFlags
        {
            get
            {
                return (ObjectDescriptionFlag)DataObject.DataObjectDescriptionFlags;
            }
            protected internal set
            {
                DataObject.DataObjectDescriptionFlags = (uint)value;
            }
        }

        public string NamePlural
        {
            get
            {
                return DataObject.PluralName;
            }
            set
            {
                DataObject.PluralName = value;
            }
        }

        public byte? ItemCapacity
        {
            get
            {
                return DataObject.ItemsCapacity;
            }
            set
            {
                DataObject.ItemsCapacity = value;
            }
        }

        public byte? ContainerCapacity
        {
            get
            {
                return DataObject.ContainersCapacity;
            }
            set
            {
                DataObject.ContainersCapacity = value;
            }
        }

        public AmmoType? AmmoType
        {
            get
            {
                return (AmmoType?)DataObject.AmmoType;
            }
            set
            {
                DataObject.AmmoType = (int?)value;
            }
        }

        public virtual int? Value
        {
            get
            {
                return (StackUnitValue * (StackSize ?? 1));
            }
            set
            {
                DataObject.Value = value;
            }
        }

        private readonly int? stackUnitValue;

        public virtual int? StackUnitValue
        {
            get
            {
                // return Weenie.Value ?? 0;
                return stackUnitValue ?? 0;
            }
        }

        public Usable? Usable
        {
            get
            {
                return (Usable?)DataObject.ItemUseable;
            }
            set
            {
                DataObject.ItemUseable = (int?)value;
            }
        }

        public float? UseRadius
        {
            get
            {
                return DataObject.UseRadius;
            }
            set
            {
                DataObject.UseRadius = value;
            }
        }

        public int? TargetType
        {
            get
            {
                return DataObject.TargetType;
            }
            set
            {
                DataObject.TargetType = value;
            }
        }

        public UiEffects? UiEffects
        {
            get
            {
                return (UiEffects?)DataObject.UiEffects;
            }
            set
            {
                DataObject.UiEffects = (int?)value;
            }
        }

        public CombatUse? CombatUse
        {
            get
            {
                return (CombatUse?)DataObject.CombatUse;
            }
            set
            {
                DataObject.CombatUse = (byte?)value;
            }
        }

        /// <summary>
        /// This is used to indicate the number of uses remaining.  Example 32 uses left out of 50 (MaxStructure)
        /// </summary>
        public ushort? Structure
        {
            get
            {
                return DataObject.Structure;
            }
            set
            {
                DataObject.Structure = value;
            }
        }

        /// <summary>
        /// Use Limit - example 50 use healing kit
        /// </summary>
        public ushort? MaxStructure
        {
            get
            {
                return DataObject.MaxStructure;
            }
            set
            {
                DataObject.MaxStructure = value;
            }
        }

        public virtual ushort? StackSize
        {
            get
            {
                return DataObject.StackSize;
            }
            set
            {
                DataObject.StackSize = value;
            }
        }

        public ushort? MaxStackSize
        {
            get
            {
                return DataObject.MaxStackSize;
            }
            set
            {
                DataObject.MaxStackSize = value;
            }
        }

        public uint? ContainerId
        {
            get
            {
                return DataObject.ContainerIID;
            }
            set
            {
                DataObject.ContainerIID = value;
            }
        }

        public int? Placement
        {
            get
            {
                return DataObject.Placement;
            }
            set
            {
                DataObject.Placement = value;
            }
        }

        public uint? WielderId
        {
            get
            {
                return DataObject.WielderIID;
            }
            set
            {
                DataObject.WielderIID = value;
            }
        }

        // Locations
        public EquipMask? ValidLocations
        {
            get
            {
                return (EquipMask?)DataObject.ValidLocations;
            }
            set
            {
                DataObject.ValidLocations = (int?)value;
            }
        }

        public EquipMask? CurrentWieldedLocation
        {
            get
            {
                return (EquipMask?)DataObject.CurrentWieldedLocation;
            }
            set
            {
                DataObject.CurrentWieldedLocation = (int?)value;
            }
        }

        public CoverageMask? Priority
        {
            get
            {
                return (CoverageMask?)DataObject.ClothingPriority;
            }
            set
            {
                DataObject.ClothingPriority = (int?)value;
            }
        }

        public RadarColor? RadarColor
        {
            get
            {
                return (RadarColor?)DataObject.RadarBlipColor;
            }
            set
            {
                DataObject.RadarBlipColor = (byte?)value;
            }
        }

        public RadarBehavior? RadarBehavior
        {
            get
            {
                return (RadarBehavior?)DataObject.ShowableOnRadar;
            }
            set
            {
                DataObject.ShowableOnRadar = (byte?)value;
            }
        }

        public ushort? Script
        {
            get
            {
                return DataObject.PhysicsScriptDID;
            }
            set
            {
                DataObject.PhysicsScriptDID = value;
            }
        }

        public float? Workmanship
        {
            get
            {
                if ((ItemWorkmanship != null) && (Structure != null) && (Structure != 0))
                {
                    return (float)Convert.ToDouble(ItemWorkmanship / (10000 * Structure));
                }
                return (ItemWorkmanship);
            }
            set
            {
                if ((Structure != null) && (Structure != 0))
                {
                    ItemWorkmanship = (int)Convert.ToInt32(value * 10000 * Structure);
                }
                else
                {
                    ItemWorkmanship = (int)Convert.ToInt32(value);
                }
            }
        }

        private int? ItemWorkmanship
        {
            get
            {
                return DataObject.ItemWorkmanship;
            }
            set
            {
                DataObject.ItemWorkmanship = value;
            }
        }

        public virtual ushort? Burden
        {
            get
            {
                return (ushort)(StackUnitBurden ?? 0 * (StackSize ?? 1));
            }
            set
            {
                DataObject.EncumbranceVal = value;
            }
        }

        private readonly ushort? stackUnitBurden;
        public virtual ushort? StackUnitBurden
        {
            get
            {
                // return Weenie.EncumbranceVal ?? 0;
                return stackUnitBurden ?? 0;
            }
        }

        public Spell? Spell
        {
            get
            {
                return (Spell?)DataObject.SpellDID;
            }
            set
            {
                DataObject.SpellDID = (ushort?)value;
            }
        }

        /// <summary>
        /// Housing links to another packet, that needs sent.. The HouseRestrictions ACL Control list that contains all the housing data
        /// </summary>
        public uint? HouseOwner { get; set; }

        public uint? HouseRestrictions { get; set; }

        public ushort? HookItemType
        {
            get
            {
                return DataObject.HookItemType;
            }
            set
            {
                DataObject.HookItemType = value;
            }
        }

        public uint? Monarch { get; set; }

        public ushort? HookType
        {
            get
            {
                return (ushort?)DataObject.HookType;
            }
            set
            {
                DataObject.HookType = value;
            }
        }

        public uint? IconOverlayId
        {
            get
            {
                return DataObject.IconOverlayDID;
            }
            set
            {
                DataObject.IconOverlayDID = value;
            }
        }

        public uint? IconUnderlayId
        {
            get
            {
                return DataObject.IconUnderlayDID;
            }
            set
            {
                DataObject.IconUnderlayDID = value;
            }
        }

        public Material? MaterialType
        {
            get
            {
                return (Material?)DataObject.MaterialType;
            }
            set
            {
                DataObject.MaterialType = (byte?)value;
            }
        }

        public uint? PetOwner { get; set; }

        public int? CooldownId
        {
            get
            {
                return DataObject.SharedCooldown;
            }
            set
            {
                DataObject.SharedCooldown = value;
            }
        }

        public double? CooldownDuration
        {
            get
            {
                return DataObject.CooldownDuration;
            }
            set
            {
                DataObject.CooldownDuration = value;
            }
        }

        ////None                   = 0x00000000,
        ////Openable               = 0x00000001,
        public bool Openable
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.Openable);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.Openable;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.Openable;

                // DataObject.Openable = value;
            }
        }

        ////Inscribable            = 0x00000002,
        public bool Inscribable
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.Inscribable);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.Inscribable;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.Inscribable;
                DataObject.Inscribable = value;
            }
        }

        ////Stuck                  = 0x00000004,
        public bool Stuck
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.Stuck);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.Stuck;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.Stuck;
                DataObject.Stuck = value;
            }
        }

        ////Player                 = 0x00000008,
        public bool Player
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.Player);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.Player;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.Player;

                // DataObject.Player = value;
            }
        }

        ////Attackable             = 0x00000010,
        public bool Attackable
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.Attackable);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.Attackable;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.Attackable;
                DataObject.Attackable = value;
            }
        }

        ////PlayerKiller           = 0x00000020,
        public bool PlayerKiller
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.PlayerKiller);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.PlayerKiller;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.PlayerKiller;

                // DataObject.PlayerKiller = value;
            }
        }

        ////HiddenAdmin            = 0x00000040,
        public bool HiddenAdmin
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.HiddenAdmin);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.HiddenAdmin;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.HiddenAdmin;
                DataObject.HiddenAdmin = value;
            }
        }

        ////UiHidden               = 0x00000080,
        public bool UiHidden
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.UiHidden);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.UiHidden;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.UiHidden;
                DataObject.UiHidden = value;
            }
        }

        ////Book                   = 0x00000100,
        public bool Book
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.Book);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.Book;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.Book;

                // DataObject.Book = value;
            }
        }

        ////Vendor                 = 0x00000200,
        public bool Vendor
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.Vendor);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.Vendor;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.Vendor;

                // DataObject.Vendor = value;
            }
        }

        ////PkSwitch               = 0x00000400,
        public bool PkSwitch
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.PkSwitch);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.PkSwitch;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.PkSwitch;

                // DataObject.PkSwitch = value;
            }
        }

        ////NpkSwitch              = 0x00000800,
        public bool NpkSwitch
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.NpkSwitch);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.NpkSwitch;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.NpkSwitch;

                // DataObject.NpkSwitch = value;
            }
        }

        ////Door                   = 0x00001000,
        public bool Door
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.Door);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.Door;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.Door;

                // DataObject.Door = value;
            }
        }

        ////Corpse                 = 0x00002000,
        public bool Corpse
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.Corpse);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.Corpse;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.Corpse;

                // DataObject.Corpse = value;
            }
        }

        ////LifeStone              = 0x00004000,
        public bool LifeStone
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.LifeStone);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.LifeStone;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.LifeStone;

                // DataObject.LifeStone = value;
            }
        }

        ////Food                   = 0x00008000,
        public bool Food
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.Food);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.Food;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.Food;

                // DataObject.Food = value;
            }
        }

        ////Healer                 = 0x00010000,
        public bool Healer
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.Healer);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.Healer;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.Healer;

                // DataObject.Healer = value;
            }
        }

        ////Lockpick               = 0x00020000,
        public bool Lockpick
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.Lockpick);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.Lockpick;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.Lockpick;

                // DataObject.Lockpick = value;
            }
        }

        ////Portal                 = 0x00040000,
        public bool Portal
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.Portal);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.Portal;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.Portal;

                // DataObject.Portal = value;
            }
        }

        ////Admin                  = 0x00100000,
        public bool Admin
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.Admin);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.Admin;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.Admin;

                // DataObject.Admin = value;
            }
        }

        ////FreePkStatus           = 0x00200000,
        public bool FreePkStatus
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.FreePkStatus);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.FreePkStatus;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.FreePkStatus;

                // DataObject.FreePkStatus = value;
            }
        }

        ////ImmuneCellRestrictions = 0x00400000,
        public bool ImmuneCellRestrictions
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.ImmuneCellRestrictions);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.ImmuneCellRestrictions;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.ImmuneCellRestrictions;
                DataObject.IgnoreHouseBarriers = value;
            }
        }

        ////RequiresPackSlot       = 0x00800000,
        public bool RequiresPackSlot
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.RequiresPackSlot);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.RequiresPackSlot;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.RequiresPackSlot;
                DataObject.RequiresBackpackSlot = value;
            }
        }

        public bool UseBackpackSlot
        {
            get
            {
                return DataObject.UseBackpackSlot;
            }
        }

        ////Retained               = 0x01000000,
        public bool Retained
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.Retained);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.Retained;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.Retained;
                DataObject.Retained = value;
            }
        }

        ////PkLiteStatus           = 0x02000000,
        public bool PkLiteStatus
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.PkLiteStatus);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.PkLiteStatus;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.PkLiteStatus;

                // DataObject.PkLiteStatus = value;
            }
        }

        ////IncludesSecondHeader   = 0x04000000,
        public bool IncludesSecondHeader
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.IncludesSecondHeader);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.IncludesSecondHeader;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.IncludesSecondHeader;

                // DataObject.IncludesSecondHeader = value;
            }
        }

        ////BindStone              = 0x08000000,
        public bool BindStone
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.BindStone);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.BindStone;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.BindStone;

                // DataObject.BindStone = value;
            }
        }

        ////VolatileRare           = 0x10000000,
        public bool VolatileRare
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.VolatileRare);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.VolatileRare;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.VolatileRare;

                // DataObject.VolatileRare = value;
            }
        }

        ////WieldOnUse             = 0x20000000,
        public bool WieldOnUse
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.WieldOnUse);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.WieldOnUse;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.WieldOnUse;
                DataObject.WieldOnUse = value;
            }
        }

        ////WieldLeft              = 0x40000000,
        public bool WieldLeft
        {
            get
            {
                return DescriptionFlags.HasFlag(ObjectDescriptionFlag.WieldLeft);
            }
            set
            {
                if (value == true)
                    DescriptionFlags |= ObjectDescriptionFlag.WieldLeft;
                else
                    DescriptionFlags &= ~ObjectDescriptionFlag.WieldLeft;
                DataObject.AutowieldLeft = value;
            }
        }

        ////Static                      = 0x00000001,
        public bool Static
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.Static);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.Static;
                else
                    PhysicsState &= ~PhysicsState.Static;

                // DataObject.Static = value;
            }
        }

        ////Unused1                     = 0x00000002,
        ////Ethereal                    = 0x00000004,
        public bool Ethereal
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.Ethereal);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.Ethereal;
                else
                    PhysicsState &= ~PhysicsState.Ethereal;
                DataObject.Ethereal = value;
            }
        }

        ////ReportCollision             = 0x00000008,
        public bool ReportCollision
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.ReportCollision);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.ReportCollision;
                else
                    PhysicsState &= ~PhysicsState.ReportCollision;
                DataObject.ReportCollisions = value;
            }
        }

        ////IgnoreCollision             = 0x00000010,
        public bool IgnoreCollision
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.IgnoreCollision);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.IgnoreCollision;
                else
                    PhysicsState &= ~PhysicsState.IgnoreCollision;
                DataObject.IgnoreCollisions = value;
            }
        }

        ////NoDraw                      = 0x00000020,
        public bool NoDraw
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.NoDraw);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.NoDraw;
                else
                    PhysicsState &= ~PhysicsState.NoDraw;
                DataObject.NoDraw = value;
            }
        }

        ////Missile                     = 0x00000040,
        public bool Missile
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.Missile);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.Missile;
                else
                    PhysicsState &= ~PhysicsState.Missile;

                ////DataObject.Missile = value;
            }
        }

        ////Pushable                    = 0x00000080,
        public bool Pushable
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.Pushable);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.Pushable;
                else
                    PhysicsState &= ~PhysicsState.Pushable;

                ////DataObject.AlignPath = value;
            }
        }

        ////AlignPath                   = 0x00000100,
        public bool AlignPath
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.AlignPath);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.AlignPath;
                else
                    PhysicsState &= ~PhysicsState.AlignPath;

                ////DataObject.AlignPath = value;
            }
        }

        ////PathClipped                 = 0x00000200,
        public bool PathClipped
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.PathClipped);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.PathClipped;
                else
                    PhysicsState &= ~PhysicsState.PathClipped;

                ////DataObject.PathClipped = value;
            }
        }

        ////Gravity                     = 0x00000400,
        public bool Gravity
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.Gravity);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.Gravity;
                else
                    PhysicsState &= ~PhysicsState.Gravity;
                DataObject.GravityStatus = value;
            }
        }

        ////LightingOn                  = 0x00000800,
        public bool LightingOn
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.LightingOn);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.LightingOn;
                else
                    PhysicsState &= ~PhysicsState.LightingOn;
                DataObject.LightsStatus = value;
            }
        }

        ////ParticleEmitter             = 0x00001000,
        public bool ParticleEmitter
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.ParticleEmitter);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.ParticleEmitter;
                else
                    PhysicsState &= ~PhysicsState.ParticleEmitter;

                ////DataObject.HasPhysicsBsp = value;
            }
        }

        ////Unused2                     = 0x00002000,
        ////Hidden                      = 0x00004000,
        public bool Hidden
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.Hidden);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.Hidden;
                else
                    PhysicsState &= ~PhysicsState.Hidden;

                // DataObject.Hidden = value;
            }
        }

        ////ScriptedCollision           = 0x00008000,
        public bool ScriptedCollision
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.ScriptedCollision);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.ScriptedCollision;
                else
                    PhysicsState &= ~PhysicsState.ScriptedCollision;
                DataObject.ScriptedCollision = value;
            }
        }

        ////HasPhysicsBsp               = 0x00010000,
        public bool HasPhysicsBsp
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.HasPhysicsBsp);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.HasPhysicsBsp;
                else
                    PhysicsState &= ~PhysicsState.HasPhysicsBsp;

                ////DataObject.HasPhysicsBsp = value;
            }
        }

        ////Inelastic                   = 0x00020000,
        public bool Inelastic
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.Inelastic);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.Inelastic;
                else
                    PhysicsState &= ~PhysicsState.Inelastic;
                DataObject.Inelastic = value;
            }
        }

        ////HasDefaultAnim              = 0x00040000,
        public bool HasDefaultAnim
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.HasDefaultAnim);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.HasDefaultAnim;
                else
                    PhysicsState &= ~PhysicsState.HasDefaultAnim;

                ////DataObject.HasDefaultAnim = value;
            }
        }

        ////HasDefaultScript            = 0x00080000,
        public bool HasDefaultScript
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.HasDefaultScript);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.HasDefaultScript;
                else
                    PhysicsState &= ~PhysicsState.HasDefaultScript;

                ////DataObject.HasDefaultScript = value;
            }
        }

        ////Cloaked                     = 0x00100000,
        public bool Cloaked
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.Cloaked);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.Cloaked;
                else
                    PhysicsState &= ~PhysicsState.Cloaked;

                ////DataObject.Cloaked = value;
            }
        }

        ////ReportCollisionAsEnviroment = 0x00200000,
        public bool ReportCollisionAsEnviroment
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.ReportCollisionAsEnviroment);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.ReportCollisionAsEnviroment;
                else
                    PhysicsState &= ~PhysicsState.ReportCollisionAsEnviroment;
                DataObject.ReportCollisionsAsEnvironment = value;
            }
        }

        ////EdgeSlide                   = 0x00400000,
        public bool EdgeSlide
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.EdgeSlide);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.EdgeSlide;
                else
                    PhysicsState &= ~PhysicsState.EdgeSlide;
                DataObject.AllowEdgeSlide = value;
            }
        }

        ////Sledding                    = 0x00800000,
        public bool Sledding
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.Sledding);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.Sledding;
                else
                    PhysicsState &= ~PhysicsState.Sledding;

                ////DataObject.Sledding = value;
            }
        }

        ////Frozen                      = 0x01000000,
        public bool Frozen
        {
            get
            {
                return PhysicsState.HasFlag(PhysicsState.Frozen);
            }
            set
            {
                if (value == true)
                    PhysicsState |= PhysicsState.Frozen;
                else
                    PhysicsState &= ~PhysicsState.Frozen;
                DataObject.IsFrozen = value;
            }
        }

        ////public bool? Visibility
        ////{
        ////    get { return DataObject.Visibility; }
        ////    set { DataObject.Visibility = value; }
        ////}

        public WeenieType WeenieType
        {
            get
            {
                return (WeenieType?)DataObject.WeenieType ?? WeenieType.Undef;
            }
            protected set
            {
                DataObject.WeenieType = (int)value;
            }
        }

        public IActor CurrentParent { get; private set; }

        public Position ForcedLocation { get; private set; }

        public Position RequestedLocation { get; private set; }

        public bool MovementDataChanged { get; set; } = false;

        /// <summary>
        /// Should only be adjusted by LandblockManager -- default is null
        /// </summary>
        public Landblock CurrentLandblock
        {
            get
            {
                return CurrentParent as Landblock;
            }
        }

        /// <summary>
        /// tick-stamp for the last time this object changed in any way.
        /// </summary>
        public double LastUpdatedTicks { get; set; }

        /// <summary>
        /// Time when this object will despawn, -1 is never.
        /// </summary>
        public double DespawnTime { get; set; } = -1;

        private readonly NestedActionQueue actionQueue = new NestedActionQueue();

        /// <summary>
        /// tick-stamp for the last time a movement update was sent
        /// </summary>
        public double LastMovementBroadcastTicks { get; set; }

        /// <summary>
        /// tick-stamp for the server time of the last time the player moved.
        /// TODO: implement
        /// </summary>
        public double LastAnimatedTicks { get; set; }

        public virtual void PlayScript(Session session)
        {
        }

        public virtual float ListeningRadius { get; protected set; } = 5f;

        ////// Logical Game Data
        public ContainerType ContainerType
        {
            get
            {
                if (ItemCapacity != null && ItemCapacity != 0)
                    return ContainerType.Container;
                if (Name.Contains("Foci"))
                    return ContainerType.Foci;
                return ContainerType.NonContainer;
            }
        }

        public CombatStyle? DefaultCombatStyle
        {
            get
            {
                return (CombatStyle?)DataObject.DefaultCombatStyle;
            }
            set
            {
                DataObject.DefaultCombatStyle = (int?)value;
            }
        }

        public uint? GeneratorId
        {
            get
            {
                return DataObject.GeneratorIID;
            }
            set
            {
                DataObject.GeneratorIID = value;
            }
        }

        public uint? ClothingBase
        {
            get
            {
                return DataObject.ClothingBaseDID;
            }
            set
            {
                DataObject.ClothingBaseDID = value;
            }
        }

        public int? PaletteTemplate
        {
            get
            {
                return DataObject.PaletteTemplate;
            }
            set
            {
                DataObject.PaletteTemplate = value;
            }
        }

        public double? Shade
        {
            get
            {
                return DataObject.Shade;
            }
            set
            {
                DataObject.Shade = value;
            }
        }

        public bool? Dyeable
        {
            get
            {
                return DataObject.Dyeable;
            }
            set
            {
                DataObject.Dyeable = value;
            }
        }

        public int? ItemCurMana
        {
            get
            {
                return DataObject.ItemCurMana;
            }
            set
            {
                DataObject.ItemCurMana = value;
            }
        }

        public int? ItemMaxMana
        {
            get
            {
                return DataObject.ItemMaxMana;
            }
            set
            {
                DataObject.ItemMaxMana = value;
            }
        }

        public bool? AdvocateState
        {
            get
            {
                return DataObject.AdvocateState;
            }
            set
            {
                DataObject.AdvocateState = value;
            }
        }

        public bool? UnderLifestoneProtection
        {
            get
            {
                return DataObject.UnderLifestoneProtection;
            }
            set
            {
                DataObject.UnderLifestoneProtection = value;
            }
        }

        public bool? DefaultOn
        {
            get
            {
                return DataObject.DefaultOn;
            }
            set
            {
                DataObject.DefaultOn = value;
            }
        }

        public bool? AdvocateQuest
        {
            get
            {
                return DataObject.AdvocateQuest;
            }
            set
            {
                DataObject.AdvocateQuest = value;
            }
        }

        public bool? IsAdvocate
        {
            get
            {
                return DataObject.IsAdvocate;
            }
            set
            {
                DataObject.IsAdvocate = value;
            }
        }

        public bool? IsSentinel
        {
            get
            {
                return DataObject.IsSentinel;
            }
            set
            {
                DataObject.IsSentinel = value;
            }
        }

        public bool? IgnorePortalRestrictions
        {
            get
            {
                return DataObject.IgnorePortalRestrictions;
            }
            set
            {
                DataObject.IgnorePortalRestrictions = value;
            }
        }

        public bool? Invincible
        {
            get
            {
                return DataObject.Invincible;
            }
            set
            {
                DataObject.Invincible = value;
            }
        }

        public bool? IsGagged
        {
            get
            {
                return DataObject.IsGagged;
            }
            set
            {
                DataObject.IsGagged = value;
            }
        }

        public bool? Afk
        {
            get
            {
                return DataObject.Afk;
            }
            set
            {
                DataObject.Afk = value;
            }
        }

        public bool? IgnoreAuthor
        {
            get
            {
                return DataObject.IgnoreAuthor;
            }
            set
            {
                DataObject.IgnoreAuthor = value;
            }
        }

        public bool? NpcLooksLikeObject
        {
            get
            {
                return DataObject.NpcLooksLikeObject;
            }
            set
            {
                DataObject.NpcLooksLikeObject = value;
            }
        }

        public CreatureType? CreatureType
        {
            get
            {
                return (CreatureType?)DataObject.CreatureType;
            }
            set
            {
                DataObject.CreatureType = (int)value;
            }
        }
        public SetupModel CSetup
        {
            get
            {
                Debug.Assert(SetupTableId != null, nameof(SetupTableId) + " != null");
                return SetupModel.ReadFromDat(SetupTableId.Value);
            }
        }

        /// <summary>
        /// This is used to determine how close you need to be to use an item.
        /// NOTE: cheat factor added for items with null use radius.   Og II
        /// </summary>
        public float UseRadiusSquared
        {
            get
            {
                return ((UseRadius ?? 2) + CSetup.Radius) * ((UseRadius ?? 2) + CSetup.Radius);
            }
        }

        public bool IsWithinUseRadiusOf(WorldObject wo)
        {
            return !(Location.SquaredDistanceTo(wo.Location) >= wo.UseRadiusSquared);
        }

        public float? ManaStoneDestroyChance
        {
            get
            {
                return DataObject.ManaStoneDestroyChance;
            }
            set
            {
                DataObject.ManaStoneDestroyChance = value;
            }
        }

        public float? ItemEfficiency
        {
            get
            {
                return DataObject.ItemEfficiency;
            }
            set
            {
                DataObject.ItemEfficiency = value;
            }
        }

        public string LongDesc
        {
            get
            {
                return DataObject.LongDesc;
            }
            set
            {
                DataObject.LongDesc = value;
            }
        }

        public string Use
        {
            get
            {
                return DataObject.Use;
            }
            set
            {
                DataObject.Use = value;
            }
        }

        public string Inscription
        {
            get
            {
                return DataObject.Inscription;
            }
            set
            {
                DataObject.Inscription = value;
            }
        }

        public string ScribeAccount
        {
            get
            {
                return DataObject.ScribeAccount;
            }
            set
            {
                DataObject.ScribeAccount = value;
            }
        }

        public string ScribeName
        {
            get
            {
                return DataObject.ScribeName;
            }
            set
            {
                DataObject.ScribeName = value;
            }
        }

        public uint? Scribe
        {
            get
            {
                return DataObject.ScribeIID;
            }
            set
            {
                DataObject.ScribeIID = value;
            }
        }

        public int? Pages
        {
            get
            {
                return DataObject.AppraisalPages;
            }
            set
            {
                DataObject.AppraisalPages = value;
            }
        }

        public int? MaxPages
        {
            get
            {
                return DataObject.AppraisalMaxPages;
            }
            set
            {
                DataObject.AppraisalMaxPages = value;
            }
        }

        public int? MaxCharactersPerPage
        {
            get
            {
                return DataObject.AvailableCharacter;
            }
            set
            {
                DataObject.AvailableCharacter = value;
            }
        }

        public int? Boost
        {
            get
            {
                return DataObject.Boost;
            }
            set
            {
                DataObject.Boost = value;
            }
        }

        public uint? SpellDID
        {
            get
            {
                return DataObject.SpellDID ?? null;
            }
            set
            {
                DataObject.SpellDID = value;
            }
        }

        public int? BoostEnum
        {
            get
            {
                return DataObject.BoostEnum ?? 0;
            }
            set
            {
                DataObject.BoostEnum = value;
            }
        }

        public double? HealkitMod
        {
            get
            {
                return DataObject.HealkitMod;
            }
            set
            {
                DataObject.HealkitMod = value;
            }
        }

        public virtual int? CoinValue
        {
            get
            {
                return DataObject.CoinValue;
            }
            set
            {
                DataObject.CoinValue = value;
            }
        }

        public int? Heritage
        {
            get
            {
                return DataObject.Heritage;
            }
        }

        public int? Gender
        {
            get
            {
                return DataObject.Gender;
            }
        }

        private MovementData _movementData;

        public MovementData CurrentMovement
        {
            get { return _movementData; }
            protected set
            {
                _movementData = value;
                MovementDataChanged = true;
            }
        }

        private UniversalMotion _currentMotion;

        public UniversalMotion CurrentMotion
        {
            get { return _currentMotion; }
            protected set { _currentMotion = value; }
        }

        public void BroadcastCurrentMotion()
        {
            CurrentLandblock.EnqueueBroadcastMotion(this, CurrentMotion);
        }

        private MotionStance _stance = MotionStance.Standing;

        public MotionStance Stance
        {
            get { return _stance; }
            protected set { _stance = value; }
        }

        public SequenceManager Sequences { get; }

        /// <summary>
        /// This is a way to essentially "lock" the object from doing until actions.
        /// Any function that sets this, should clear it after it is complete.
        /// e.g. set this while casting a spell, and clear after spellcasting is complete
        /// </summary>
        public bool Busy = false;

        protected WorldObject(ObjectGuid guid)
        {
            DataObject = new DataObject { DataObjectId = guid.Full };

            Guid = guid;

            Sequences = new SequenceManager();
            Sequences.AddOrSetSequence(SequenceType.ObjectPosition, new UShortSequence());
            Sequences.AddOrSetSequence(SequenceType.ObjectMovement, new UShortSequence());
            Sequences.AddOrSetSequence(SequenceType.ObjectState, new UShortSequence());
            Sequences.AddOrSetSequence(SequenceType.ObjectVector, new UShortSequence());
            Sequences.AddOrSetSequence(SequenceType.ObjectTeleport, new UShortSequence());
            Sequences.AddOrSetSequence(SequenceType.ObjectServerControl, new UShortSequence());
            Sequences.AddOrSetSequence(SequenceType.ObjectForcePosition, new UShortSequence());
            Sequences.AddOrSetSequence(SequenceType.ObjectVisualDesc, new UShortSequence());
            Sequences.AddOrSetSequence(SequenceType.ObjectInstance, new UShortSequence());
            Sequences.AddOrSetSequence(SequenceType.Motion, new UShortSequence(1, 0x7FFF));

                // MSB is reserved, so set max value to exclude it.

            Sequences.AddOrSetSequence(SequenceType.PrivateUpdateAttribute, new ByteSequence(false));
            Sequences.AddOrSetSequence(SequenceType.PrivateUpdateAttribute2ndLevel, new ByteSequence(false));
            Sequences.AddOrSetSequence(SequenceType.PrivateUpdateAttribute2ndLevelHealth, new ByteSequence(false));
            Sequences.AddOrSetSequence(SequenceType.PrivateUpdateAttribute2ndLevelStamina, new ByteSequence(false));
            Sequences.AddOrSetSequence(SequenceType.PrivateUpdateAttribute2ndLevelMana, new ByteSequence(false));
            Sequences.AddOrSetSequence(SequenceType.PrivateUpdateSkill, new ByteSequence(false));
            Sequences.AddOrSetSequence(SequenceType.PrivateUpdatePropertyBool, new ByteSequence(false));
            Sequences.AddOrSetSequence(SequenceType.PrivateUpdatePropertyInt, new ByteSequence(false));
            Sequences.AddOrSetSequence(SequenceType.PrivateUpdatePropertyInt64, new ByteSequence(false));
            Sequences.AddOrSetSequence(SequenceType.PrivateUpdatePropertyDouble, new ByteSequence(false));
            Sequences.AddOrSetSequence(SequenceType.PrivateUpdatePropertyString, new ByteSequence(false));
            Sequences.AddOrSetSequence(SequenceType.PrivateUpdatePropertyDataID, new ByteSequence(false));
            Sequences.AddOrSetSequence(SequenceType.PublicUpdatePropertyBool, new ByteSequence(false));
            Sequences.AddOrSetSequence(SequenceType.PublicUpdatePropertyInt, new ByteSequence(false));
            Sequences.AddOrSetSequence(SequenceType.PublicUpdatePropertyInt64, new ByteSequence(false));
            Sequences.AddOrSetSequence(SequenceType.PublicUpdatePropertyDouble, new ByteSequence(false));
            Sequences.AddOrSetSequence(SequenceType.PublicUpdatePropertyString, new ByteSequence(false));
            Sequences.AddOrSetSequence(SequenceType.PublicUpdatePropertyDataID, new ByteSequence(false));
            Sequences.AddOrSetSequence(SequenceType.PublicUpdatePropertyInstanceId, new ByteSequence(false));

            Sequences.AddOrSetSequence(SequenceType.SetStackSize, new ByteSequence(false));
        }

        protected WorldObject(DataObject dataObject)
            : this(new ObjectGuid(dataObject.DataObjectId))
        {
            DataWeenie = DatabaseManager.World.GetWeenie(dataObject.WeenieClassId);
            stackUnitValue = DataWeenie.Value;
            stackUnitBurden = DataWeenie.EncumbranceVal;

            DataObject = dataObject;
            Burden = (ushort)(StackUnitBurden * StackSize ?? 1);
            SetWeenieHeaderFlag();
            SetWeenieHeaderFlag2();
            RecallAndSetObjectDescriptionBools(); // Read bools stored in DB and apply them

            RecallAndSetPhysicsStateBools(); // Read bools stored in DB and apply them

            if (dataObject.CurrentMotionState == "0" || dataObject.CurrentMotionState == null)
                CurrentMotionState = null;
            else
                CurrentMotionState = new UniversalMotion(Convert.FromBase64String(dataObject.CurrentMotionState));

            UpdateModelInfo();
        }

        protected WorldObject(ObjectGuid guid, DataObject dataObject)
            : this(guid)
        {
            DataWeenie = DatabaseManager.World.GetWeenie(dataObject.WeenieClassId);
            stackUnitValue = DataWeenie.Value;
            stackUnitBurden = DataWeenie.EncumbranceVal;
            DataObject = dataObject;
            Guid = guid;
            Burden = (ushort)(StackUnitBurden * StackSize ?? 1);

            RecallAndSetObjectDescriptionBools(); // Read bools stored in DB and apply them

            RecallAndSetPhysicsStateBools(); // Read bools stored in DB and apply them

            if (dataObject.CurrentMotionState == "0" || dataObject.CurrentMotionState == null)
                CurrentMotionState = null;
            else
                CurrentMotionState = new UniversalMotion(Convert.FromBase64String(dataObject.CurrentMotionState));

            UpdateModelInfo();
        }

        internal void SetInventoryForVendor(WorldObject inventoryItem)
        {
            inventoryItem.Location = null;
            inventoryItem.PositionFlag = UpdatePositionFlag.None;
            inventoryItem.ContainerId = null;
            inventoryItem.Placement = null;
            inventoryItem.WielderId = null;
            inventoryItem.CurrentWieldedLocation = null;

            // TODO: create enum for this once we understand this better.
            // This is needed to make items lay flat on the ground.
            inventoryItem.AnimationFrame = (int)Entity.Enum.Placement.Resting;
        }

        internal void SetInventoryForWorld(WorldObject inventoryItem)
        {
            inventoryItem.Location = Location.InFrontOf(1.1f);
            inventoryItem.PositionFlag = UpdatePositionFlag.Contact | UpdatePositionFlag.Placement
                                         | UpdatePositionFlag.ZeroQy | UpdatePositionFlag.ZeroQx;

            inventoryItem.ContainerId = null;
            inventoryItem.Placement = null;
            inventoryItem.WielderId = null;
            inventoryItem.CurrentWieldedLocation = null;
            // This is needed to make items lay flat on the ground.
            inventoryItem.AnimationFrame = (int)Entity.Enum.Placement.Resting;
        }

        internal void SetInventoryForContainer(WorldObject inventoryItem, int placement)
        {
            if (inventoryItem.Location != null)
                LandblockManager.RemoveObject(inventoryItem);
            inventoryItem.PositionFlag = UpdatePositionFlag.None;

            // TODO: Create enums for this.
            inventoryItem.AnimationFrame = 1;
            inventoryItem.Placement = placement;
            inventoryItem.Location = null;
            inventoryItem.ParentLocation = null;
            inventoryItem.CurrentWieldedLocation = null;
            inventoryItem.WielderId = null;
        }

        public void Examine(Session examiner)
        {
            // TODO : calculate if we were successful
            bool successfulId = true;
            GameEventIdentifyObjectResponse identifyResponse = new GameEventIdentifyObjectResponse(
                examiner,
                this,
                successfulId);
            examiner.Network.EnqueueSend(identifyResponse);

#if DEBUG
            examiner.Network.EnqueueSend(new GameMessageSystemChat("", ChatMessageType.System));
            examiner.Network.EnqueueSend(
                new GameMessageSystemChat($"{DebugOutputString(GetType(), this)}", ChatMessageType.System));
#endif
        }

        public void ReadBookPage(Session reader, uint pageNum)
        {
            PageData pageData = new PageData();
            DataObjectPropertiesBook bookPage = PropertiesBook.FirstOrDefault(b => b.Page == pageNum);

            if (bookPage == null)
                return;

            pageData.AuthorID = bookPage.AuthorId;
            pageData.AuthorName = bookPage.AuthorName;
            pageData.AuthorAccount = bookPage.AuthorAccount;
            pageData.PageIdx = pageNum;
            pageData.PageText = bookPage.PageText;
            pageData.IgnoreAuthor = false;

            // TODO - check for PropertyBool.IgnoreAuthor flag

            GameEventBookPageDataResponse bookDataResponse = new GameEventBookPageDataResponse(
                reader,
                guid.Full,
                pageData);
            reader.Network.EnqueueSend(bookDataResponse);
        }

        public virtual void SerializeIdentifyObjectResponse(
            BinaryWriter writer,
            bool success,
            IdentifyResponseFlags flags = IdentifyResponseFlags.None)
        {
            // Excluding some times that are sent later as weapon status Og II
            List<DataObjectPropertiesInt> propertiesInt =
                PropertiesInt.Where(
                    x =>
                        x.PropertyId < 9000 && x.PropertyId != (uint)PropertyInt.Damage
                        && x.PropertyId != (uint)PropertyInt.DamageType && x.PropertyId != (uint)PropertyInt.WeaponSkill
                        && x.PropertyId != (uint)PropertyInt.WeaponTime).ToList();

            if (propertiesInt.Count > 0)
            {
                flags |= IdentifyResponseFlags.IntStatsTable;
            }

            List<DataObjectPropertiesInt64> propertiesInt64 = PropertiesInt64.Where(x => x.PropertyId < 9000).ToList();

            if (propertiesInt64.Count > 0)
            {
                flags |= IdentifyResponseFlags.Int64StatsTable;
            }

            List<DataObjectPropertiesBool> propertiesBool = PropertiesBool.Where(x => x.PropertyId < 9000).ToList();

            if (propertiesBool.Count > 0)
            {
                flags |= IdentifyResponseFlags.BoolStatsTable;
            }

            // the float values 13 - 19 + 165 (nether added way later) are armor resistance and is shown in a different list. Og II
            // 21-22, 26, 62-63 are all sent as part of the weapons profile and not duplicated.
            List<DataObjectPropertiesDouble> propertiesDouble =
                PropertiesDouble.Where(
                    x =>
                        x.PropertyId < 9000
                        && (x.PropertyId < (uint)PropertyDouble.ArmorModVsSlash
                            || x.PropertyId > (uint)PropertyDouble.ArmorModVsElectric)
                        && x.PropertyId != (uint)PropertyDouble.WeaponLength
                        && x.PropertyId != (uint)PropertyDouble.DamageVariance
                        && x.PropertyId != (uint)PropertyDouble.MaximumVelocity
                        && x.PropertyId != (uint)PropertyDouble.WeaponOffense
                        && x.PropertyId != (uint)PropertyDouble.DamageMod
                        && x.PropertyId != (uint)PropertyDouble.ArmorModVsNether).ToList();
            if (propertiesDouble.Count > 0)
            {
                flags |= IdentifyResponseFlags.FloatStatsTable;
            }

            List<DataObjectPropertiesDataId> propertiesDid = PropertiesDid.Where(x => x.PropertyId < 9000).ToList();

            if (propertiesDid.Count > 0)
            {
                flags |= IdentifyResponseFlags.DidStatsTable;
            }

            List<DataObjectPropertiesString> propertiesString =
                PropertiesString.Where(x => x.PropertyId < 9000).ToList();

            List<DataObjectPropertiesSpell> propertiesSpellId = PropertiesSpellId.ToList();

            if (propertiesSpellId.Count > 0)
            {
                flags |= IdentifyResponseFlags.SpellBook;
            }

            // TODO: Move to Armor class
            List<DataObjectPropertiesDouble> propertiesArmor =
                PropertiesDouble.Where(
                    x =>
                        (x.PropertyId < 9000
                         && (x.PropertyId >= (uint)PropertyDouble.ArmorModVsSlash
                         && x.PropertyId <= (uint)PropertyDouble.ArmorModVsElectric))
                        || x.PropertyId == (uint)PropertyDouble.ArmorModVsNether)
                        .OrderBy(x => x.PropertyId == (uint)PropertyDouble.ArmorModVsElectric)
                        .ThenBy(x => x.PropertyId == (uint)PropertyDouble.ArmorModVsNether)
                        .ThenBy(x => x.PropertyId == (uint)PropertyDouble.ArmorModVsAcid)
                        .ThenBy(x => x.PropertyId == (uint)PropertyDouble.ArmorModVsFire)
                        .ThenBy(x => x.PropertyId == (uint)PropertyDouble.ArmorModVsCold)
                        .ThenBy(x => x.PropertyId == (uint)PropertyDouble.ArmorModVsBludgeon)
                        .ThenBy(x => x.PropertyId == (uint)PropertyDouble.ArmorModVsPierce)
                        .ThenBy(x => x.PropertyId == (uint)PropertyDouble.ArmorModVsSlash).ToList();
            if (propertiesArmor.Count > 0)
            {
                flags |= IdentifyResponseFlags.ArmorProfile;
            }

            // TODO: Move to Weapon class
            // Weapons Profile
            List<DataObjectPropertiesDouble> propertiesWeaponsD =
                PropertiesDouble.Where(
                    x =>
                        x.PropertyId < 9000
                        && (x.PropertyId == (uint)PropertyDouble.WeaponLength
                            || x.PropertyId == (uint)PropertyDouble.DamageVariance
                            || x.PropertyId == (uint)PropertyDouble.MaximumVelocity
                            || x.PropertyId == (uint)PropertyDouble.WeaponOffense
                            || x.PropertyId == (uint)PropertyDouble.DamageMod)).ToList();

            List<DataObjectPropertiesInt> propertiesWeaponsI =
                PropertiesInt.Where(
                    x =>
                        x.PropertyId < 9000
                        && (x.PropertyId == (uint)PropertyInt.Damage || x.PropertyId == (uint)PropertyInt.DamageType
                            || x.PropertyId == (uint)PropertyInt.WeaponSkill
                            || x.PropertyId == (uint)PropertyInt.WeaponTime)).ToList();

            if (propertiesWeaponsI.Count + propertiesWeaponsD.Count > 0)
            {
                flags |= IdentifyResponseFlags.WeaponProfile;
            }

            if (propertiesString.Count > 0)
            {
                flags |= IdentifyResponseFlags.StringStatsTable;
            }

            // Ok Down to business - let's identify all of this stuff.
            WriteIdentifyObjectHeader(writer, flags, success);
            WriteIdentifyObjectIntProperties(writer, flags, propertiesInt);
            WriteIdentifyObjectInt64Properties(writer, flags, propertiesInt64);
            WriteIdentifyObjectBoolProperties(writer, flags, propertiesBool);
            WriteIdentifyObjectDoubleProperties(writer, flags, propertiesDouble);
            WriteIdentifyObjectStringsProperties(writer, flags, propertiesString);
            WriteIdentifyObjectDidProperties(writer, flags, propertiesDid);
            WriteIdentifyObjectSpellIdProperties(writer, flags, propertiesSpellId);

            // TODO: Move to Armor class
            WriteIdentifyObjectArmorProfile(writer, flags, propertiesArmor);

            // TODO: Move to Weapon class
            WriteIdentifyObjectWeaponsProfile(writer, flags, propertiesWeaponsD, propertiesWeaponsI);
        }

        private string DebugOutputString(Type type, WorldObject obj)
        {
            string debugOutput = "DerethForever Debug Output:\n";
            debugOutput += "DerethForever Class File: " + type.Name + ".cs" + "\n";
            debugOutput += "DataObjectId: " + obj.Guid.Full.ToString() + " (0x" + obj.Guid.Full.ToString("X") + ")"
                           + "\n";

            debugOutput += "-Private Fields-\n";
            foreach (FieldInfo prop in obj.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (prop.GetValue(obj) == null)
                    continue;

                debugOutput += $"{prop.Name.Replace("<", "").Replace(">k__BackingField", "")} = {prop.GetValue(obj)}"
                               + "\n";
            }

            debugOutput += "-Public Properties-\n";
            foreach (PropertyInfo prop in obj.GetType().GetProperties())
            {
                if (prop.GetValue(obj, null) == null)
                    continue;

                switch (prop.Name.ToLower())
                {
                    case "guid":
                        debugOutput += $"{prop.Name} = {obj.Guid.Full.ToString()} (GuidType.{obj.guid.Type.ToString()})"
                                       + "\n";
                        break;
                    case "descriptionflags":
                        debugOutput += $"{prop.Name} = {obj.DescriptionFlags.ToString()}" + " ("
                                       + (uint)obj.DescriptionFlags + ")" + "\n";
                        break;
                    case "weenieflags":
                        debugOutput += $"{prop.Name} = {obj.WeenieFlags.ToString()}" + " (" + (uint)obj.WeenieFlags
                                       + ")" + "\n";
                        break;
                    case "weenieflags2":
                        debugOutput += $"{prop.Name} = {obj.WeenieFlags2.ToString()}" + " (" + (uint)obj.WeenieFlags2
                                       + ")" + "\n";
                        break;
                    case "positionflag":
                        debugOutput += $"{prop.Name} = {obj.PositionFlag.ToString()}" + " (" + (uint)obj.PositionFlag
                                       + ")" + "\n";
                        break;
                    case "itemtype":
                        debugOutput += $"{prop.Name} = {obj.ItemType.ToString()}" + " (" + (uint)obj.ItemType + ")"
                                       + "\n";
                        break;
                    case "creaturetype":
                        debugOutput += $"{prop.Name} = {obj.CreatureType.ToString()}" + " (" + (uint)obj.CreatureType
                                       + ")" + "\n";
                        break;
                    case "containertype":
                        debugOutput += $"{prop.Name} = {obj.ContainerType.ToString()}" + " (" + (uint)obj.ContainerType
                                       + ")" + "\n";
                        break;
                    case "usable":
                        debugOutput += $"{prop.Name} = {obj.Usable.ToString()}" + " (" + (uint)obj.Usable + ")" + "\n";
                        break;
                    case "radarbehavior":
                        debugOutput += $"{prop.Name} = {obj.RadarBehavior.ToString()}" + " (" + (uint)obj.RadarBehavior
                                       + ")" + "\n";
                        break;
                    case "physicsdescriptionflag":
                        debugOutput += $"{prop.Name} = {obj.PhysicsDescriptionFlag.ToString()}" + " ("
                                       + (uint)obj.PhysicsDescriptionFlag + ")" + "\n";
                        break;
                    case "physicsstate":
                        debugOutput += $"{prop.Name} = {obj.PhysicsState.ToString()}" + " (" + (uint)obj.PhysicsState
                                       + ")" + "\n";
                        break;
                    case "propertiesint":
                        foreach (DataObjectPropertiesInt item in obj.PropertiesInt)
                        {
                            debugOutput +=
                                $"PropertyInt.{System.Enum.GetName(typeof(PropertyInt), item.PropertyId)} ({item.PropertyId}) = {item.PropertyValue}"
                                + "\n";
                        }
                        break;
                    case "propertiesint64":
                        foreach (DataObjectPropertiesInt64 item in obj.PropertiesInt64)
                        {
                            debugOutput +=
                                $"PropertyInt64.{System.Enum.GetName(typeof(PropertyInt64), item.PropertyId)} ({item.PropertyId}) = {item.PropertyValue}"
                                + "\n";
                        }
                        break;
                    case "propertiesbool":
                        foreach (DataObjectPropertiesBool item in obj.PropertiesBool)
                        {
                            debugOutput +=
                                $"PropertyBool.{System.Enum.GetName(typeof(PropertyBool), item.PropertyId)} ({item.PropertyId}) = {item.PropertyValue}"
                                + "\n";
                        }
                        break;
                    case "propertiesstring":
                        foreach (DataObjectPropertiesString item in obj.PropertiesString)
                        {
                            debugOutput +=
                                $"PropertyString.{System.Enum.GetName(typeof(PropertyString), item.PropertyId)} ({item.PropertyId}) = {item.PropertyValue}"
                                + "\n";
                        }
                        break;
                    case "propertiesdouble":
                        foreach (DataObjectPropertiesDouble item in obj.PropertiesDouble)
                        {
                            debugOutput +=
                                $"PropertyDouble.{System.Enum.GetName(typeof(PropertyDouble), item.PropertyId)} ({item.PropertyId}) = {item.PropertyValue}"
                                + "\n";
                        }
                        break;
                    case "propertiesdid":
                        foreach (DataObjectPropertiesDataId item in obj.PropertiesDid)
                        {
                            debugOutput +=
                                $"PropertyDataId.{System.Enum.GetName(typeof(PropertyDataId), item.PropertyId)} ({item.PropertyId}) = {item.PropertyValue}"
                                + "\n";
                        }
                        break;
                    case "propertiesiid":
                        foreach (DataObjectPropertiesInstanceId item in obj.PropertiesIid)
                        {
                            debugOutput +=
                                $"PropertyInstanceId.{System.Enum.GetName(typeof(PropertyInstanceId), item.PropertyId)} ({item.PropertyId}) = {item.PropertyValue}"
                                + "\n";
                        }
                        break;
                    case "propertiesspellid":
                        foreach (DataObjectPropertiesSpell item in obj.PropertiesSpellId)
                        {
                            debugOutput +=
                                $"PropertySpellId.{System.Enum.GetName(typeof(Spell), item.SpellId)} ({item.SpellId})"
                                + "\n";
                        }
                        break;
                    case "validlocations":
                        debugOutput += $"{prop.Name} = {obj.ValidLocations}" + " (" + (uint)obj.ValidLocations + ")"
                                       + "\n";
                        break;
                    case "currentwieldedlocation":
                        debugOutput += $"{prop.Name} = {obj.CurrentWieldedLocation}" + " ("
                                       + (uint)obj.CurrentWieldedLocation + ")" + "\n";
                        break;
                    case "priority":
                        debugOutput += $"{prop.Name} = {obj.Priority}" + " (" + (uint)obj.Priority + ")" + "\n";
                        break;
                    case "radarcolor":
                        debugOutput += $"{prop.Name} = {obj.RadarColor}" + " (" + (uint)obj.RadarColor + ")" + "\n";
                        break;
                    default:
                        debugOutput += $"{prop.Name} = {prop.GetValue(obj, null)}" + "\n";
                        break;
                }
            }

            return debugOutput;
        }

        protected static void WriteIdentifyObjectHeader(BinaryWriter writer, IdentifyResponseFlags flags, bool success)
        {
            writer.Write((uint)flags); // Flags
            writer.Write(Convert.ToUInt32(success)); // Success bool
        }

        protected static void WriteIdentifyObjectIntProperties(
            BinaryWriter writer,
            IdentifyResponseFlags flags,
            List<DataObjectPropertiesInt> propertiesInt)
        {
            const ushort tableSize = 16;
            List<DataObjectPropertiesInt> notNull = propertiesInt.Where(p => p.PropertyValue != null).ToList();
            if ((flags & IdentifyResponseFlags.IntStatsTable) == 0 || (notNull.Count == 0))
                return;
            writer.Write((ushort)notNull.Count);
            writer.Write(tableSize);

            foreach (DataObjectPropertiesInt x in notNull)
            {
                writer.Write(x.PropertyId);
                writer.Write(x.PropertyValue.Value);
            }
        }

        protected static void WriteIdentifyObjectInt64Properties(
            BinaryWriter writer,
            IdentifyResponseFlags flags,
            List<DataObjectPropertiesInt64> propertiesInt64)
        {
            const ushort tableSize = 8;
            List<DataObjectPropertiesInt64> notNull = propertiesInt64.Where(p => p.PropertyValue != null).ToList();
            if ((flags & IdentifyResponseFlags.Int64StatsTable) == 0 || (notNull.Count == 0))
                return;
            writer.Write((ushort)notNull.Count);
            writer.Write(tableSize);

            foreach (DataObjectPropertiesInt64 x in notNull)
            {
                writer.Write(x.PropertyId);
                writer.Write(x.PropertyValue.Value);
            }
        }

        protected static void WriteIdentifyObjectBoolProperties(
            BinaryWriter writer,
            IdentifyResponseFlags flags,
            List<DataObjectPropertiesBool> propertiesBool)
        {
            const ushort tableSize = 8;
            List<DataObjectPropertiesBool> notNull = propertiesBool.Where(p => p.PropertyValue != null).ToList();
            if ((flags & IdentifyResponseFlags.BoolStatsTable) == 0 || (notNull.Count == 0))
                return;
            writer.Write((ushort)notNull.Count);
            writer.Write(tableSize);

            foreach (DataObjectPropertiesBool x in notNull)
            {
                writer.Write(x.PropertyId);
                writer.Write(Convert.ToUInt32(x.PropertyValue.Value));
            }
        }

        protected static void WriteIdentifyObjectDoubleProperties(
            BinaryWriter writer,
            IdentifyResponseFlags flags,
            List<DataObjectPropertiesDouble> propertiesDouble)
        {
            const ushort tableSize = 8;
            List<DataObjectPropertiesDouble> notNull = propertiesDouble.Where(p => p.PropertyValue != null).ToList();
            if ((flags & IdentifyResponseFlags.FloatStatsTable) == 0 || (notNull.Count == 0))
                return;
            writer.Write((ushort)notNull.Count);
            writer.Write(tableSize);

            foreach (DataObjectPropertiesDouble x in notNull)
            {
                writer.Write((uint)x.PropertyId);
                writer.Write(x.PropertyValue.Value);
            }
        }

        protected static void WriteIdentifyObjectStringsProperties(
            BinaryWriter writer,
            IdentifyResponseFlags flags,
            List<DataObjectPropertiesString> propertiesStrings)
        {
            const ushort tableSize = 8;
            List<DataObjectPropertiesString> notNull =
                propertiesStrings.Where(p => !string.IsNullOrWhiteSpace(p.PropertyValue)).ToList();
            if ((flags & IdentifyResponseFlags.StringStatsTable) == 0 || (notNull.Count == 0))
                return;
            writer.Write((ushort)notNull.Count);
            writer.Write(tableSize);

            foreach (DataObjectPropertiesString x in notNull)
            {
                writer.Write((uint)x.PropertyId);
                writer.WriteString16L(x.PropertyValue);
            }
        }

        protected static void WriteIdentifyObjectDidProperties(
            BinaryWriter writer,
            IdentifyResponseFlags flags,
            List<DataObjectPropertiesDataId> propertiesDid)
        {
            const ushort tableSize = 16;
            List<DataObjectPropertiesDataId> notNull = propertiesDid.Where(p => p.PropertyValue != null).ToList();
            if ((flags & IdentifyResponseFlags.DidStatsTable) == 0 || (notNull.Count == 0))
                return;
            writer.Write((ushort)notNull.Count);
            writer.Write(tableSize);

            foreach (DataObjectPropertiesDataId x in notNull)
            {
                writer.Write(x.PropertyId);
                writer.Write(x.PropertyValue.Value);
            }
        }

        protected static void WriteIdentifyObjectSpellIdProperties(
            BinaryWriter writer,
            IdentifyResponseFlags flags,
            List<DataObjectPropertiesSpell> propertiesSpellId)
        {
            if ((flags & IdentifyResponseFlags.SpellBook) == 0 || (propertiesSpellId.Count == 0))
                return;
            writer.Write((uint)propertiesSpellId.Count);

            foreach (DataObjectPropertiesSpell x in propertiesSpellId)
            {
                writer.Write(x.SpellId);
            }
        }

        // TODO: Move to Armor class
        protected static void WriteIdentifyObjectArmorProfile(
            BinaryWriter writer,
            IdentifyResponseFlags flags,
            List<DataObjectPropertiesDouble> propertiesArmor)
        {
            List<DataObjectPropertiesDouble> notNull = propertiesArmor.Where(p => p.PropertyValue != null).ToList();
            if ((flags & IdentifyResponseFlags.ArmorProfile) == 0 || (notNull.Count == 0))
                return;

            foreach (DataObjectPropertiesDouble x in notNull)
            {
                writer.Write((float)x.PropertyValue.Value);
            }
        }

        // TODO: Move to Weapon class
        protected static void WriteIdentifyObjectWeaponsProfile(
            BinaryWriter writer,
            IdentifyResponseFlags flags,
            List<DataObjectPropertiesDouble> propertiesWeaponsD,
            List<DataObjectPropertiesInt> propertiesWeaponsI)
        {
            if ((flags & IdentifyResponseFlags.WeaponProfile) == 0)
                return;
            writer.Write(propertiesWeaponsI.Find(x => x.PropertyId == (uint)PropertyInt.DamageType)?.PropertyValue ?? 0);

            // Signed
            writer.Write(
                (int?)propertiesWeaponsI.Find(x => x.PropertyId == (int)PropertyInt.WeaponTime)?.PropertyValue ?? 0);
            writer.Write(
                propertiesWeaponsI.Find(x => x.PropertyId == (uint)PropertyInt.WeaponSkill)?.PropertyValue ?? 0);

            // Signed
            writer.Write(
                (int?)propertiesWeaponsI.Find(x => x.PropertyId == (int)PropertyInt.Damage)?.PropertyValue ?? 0);
            writer.Write(
                propertiesWeaponsD.Find(x => x.PropertyId == (uint)PropertyDouble.DamageVariance)?.PropertyValue ?? 0.00);
            writer.Write(
                propertiesWeaponsD.Find(x => x.PropertyId == (uint)PropertyDouble.DamageMod)?.PropertyValue ?? 0.00);
            writer.Write(
                propertiesWeaponsD.Find(x => x.PropertyId == (uint)PropertyDouble.WeaponLength)?.PropertyValue ?? 0.00);
            writer.Write(
                propertiesWeaponsD.Find(x => x.PropertyId == (uint)PropertyDouble.MaximumVelocity)?.PropertyValue
                ?? 0.00);
            writer.Write(
                propertiesWeaponsD.Find(x => x.PropertyId == (uint)PropertyDouble.WeaponOffense)?.PropertyValue ?? 0.00);

            // This one looks to be 0 - I did not find one with this calculated.   It is called Max Velocity Calculated
            writer.Write(0u);
        }

        public void QueryHealth(Session examiner)
        {
            float healthPercentage = 1f;

            if (Guid.IsPlayer())
            {
                Player tmpTarget = (Player)this;
                healthPercentage = (float)tmpTarget.Health.Current / (float)tmpTarget.Health.MaxValue;
            }
            else if (Guid.IsCreature())
            {
                Creature tmpTarget = (Creature)this;
                healthPercentage = (float)tmpTarget.Health.Current / (float)tmpTarget.Health.MaxValue;
            }

            GameEventUpdateHealth updateHealth = new GameEventUpdateHealth(examiner, Guid.Full, healthPercentage);
            examiner.Network.EnqueueSend(updateHealth);
        }

        public void QueryItemMana(Session examiner)
        {
            float manaPercentage = 1f;
            uint success = 0;

            if (ItemCurMana != null && ItemMaxMana != null)
            {
                manaPercentage = (float)ItemCurMana / (float)ItemMaxMana;
                success = 1;
            }

            if (success == 0) // according to retail PCAPs, if success = 0, mana = 0.
                manaPercentage = 0;

            GameEventQueryItemManaResponse updateMana = new GameEventQueryItemManaResponse(
                examiner,
                Guid.Full,
                manaPercentage,
                success);
            examiner.Network.EnqueueSend(updateMana);
        }

        public virtual void SerializeUpdateObject(BinaryWriter writer)
        {
            // content of these 2 is the same? TODO: Validate that?
            SerializeCreateObject(writer);
        }

        // This fully replaces the PhysicsState of the WO, use sparingly?
        public void SetPhysicsState(PhysicsState state, bool packet = true)
        {
            PhysicsState = state;

            if (packet)
            {
                EnqueueBroadcastPhysicsState();
            }
        }

        public void EnqueueBroadcastPhysicsState()
        {
            if (CurrentLandblock != null)
            {
                GameMessage msg = new GameMessageSetState(this, PhysicsState);
                CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, msg);
            }
        }

        public void EnqueueBroadcastUpdateObject()
        {
            if (CurrentLandblock != null)
            {
                GameMessage msg = new GameMessageUpdateObject(this);
                CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, msg);
            }
        }

        private WeenieHeaderFlag SetWeenieHeaderFlag()
        {
            WeenieHeaderFlag weenieHeaderFlag = WeenieHeaderFlag.None;
            if (NamePlural != null)
                weenieHeaderFlag |= WeenieHeaderFlag.PluralName;

            if (ItemCapacity != null)
                weenieHeaderFlag |= WeenieHeaderFlag.ItemsCapacity;

            if (ContainerCapacity != null)
                weenieHeaderFlag |= WeenieHeaderFlag.ContainersCapacity;

            if (AmmoType != null)
                weenieHeaderFlag |= WeenieHeaderFlag.AmmoType;

            if (Value != null && (Value > 0))
                weenieHeaderFlag |= WeenieHeaderFlag.Value;

            if (Usable != null)
                weenieHeaderFlag |= WeenieHeaderFlag.Usable;

            if (UseRadius != null)
                weenieHeaderFlag |= WeenieHeaderFlag.UseRadius;

            if (TargetType != null)
                weenieHeaderFlag |= WeenieHeaderFlag.TargetType;

            if (UiEffects != null)
                weenieHeaderFlag |= WeenieHeaderFlag.UiEffects;

            if (CombatUse != null)
                weenieHeaderFlag |= WeenieHeaderFlag.CombatUse;

            if (Structure != null)
                weenieHeaderFlag |= WeenieHeaderFlag.Structure;

            if (MaxStructure != null)
                weenieHeaderFlag |= WeenieHeaderFlag.MaxStructure;

            if (StackSize != null)
                weenieHeaderFlag |= WeenieHeaderFlag.StackSize;

            if (MaxStackSize != null)
                weenieHeaderFlag |= WeenieHeaderFlag.MaxStackSize;

            if (ContainerId != null)
                weenieHeaderFlag |= WeenieHeaderFlag.Container;

            if (WielderId != null)
                weenieHeaderFlag |= WeenieHeaderFlag.Wielder;

            if (ValidLocations != null)
                weenieHeaderFlag |= WeenieHeaderFlag.ValidLocations;

            // You can't be in a wielded location if you don't have a wielder.   This is a gurad against crap data. Og II
            if ((CurrentWieldedLocation != null) && (CurrentWieldedLocation != 0) && (WielderId != null)
                && (WielderId != 0))
                weenieHeaderFlag |= WeenieHeaderFlag.CurrentlyWieldedLocation;

            if (Priority != null)
                weenieHeaderFlag |= WeenieHeaderFlag.Priority;

            if (RadarColor != null)
                weenieHeaderFlag |= WeenieHeaderFlag.RadarBlipColor;

            if (RadarBehavior != null)
                weenieHeaderFlag |= WeenieHeaderFlag.RadarBehavior;

            if ((Script != null) && (Script != 0u))
                weenieHeaderFlag |= WeenieHeaderFlag.PScript;

            if ((Workmanship != null) && (uint?)Workmanship != 0u)
                weenieHeaderFlag |= WeenieHeaderFlag.Workmanship;

            if (Burden != null)
                weenieHeaderFlag |= WeenieHeaderFlag.Burden;

            if ((Spell != null) && (Spell != 0))
                weenieHeaderFlag |= WeenieHeaderFlag.Spell;

            if (HouseOwner != null)
                weenieHeaderFlag |= WeenieHeaderFlag.HouseOwner;

            if (HouseRestrictions != null)
                weenieHeaderFlag |= WeenieHeaderFlag.HouseRestrictions;

            if (HookItemType != null)
                weenieHeaderFlag |= WeenieHeaderFlag.HookItemTypes;

            if (Monarch != null)
                weenieHeaderFlag |= WeenieHeaderFlag.Monarch;

            if (HookType != null)
                weenieHeaderFlag |= WeenieHeaderFlag.HookType;

            if ((IconOverlayId != null) && (IconOverlayId != 0))
                weenieHeaderFlag |= WeenieHeaderFlag.IconOverlay;

            if (MaterialType != null)
                weenieHeaderFlag |= WeenieHeaderFlag.MaterialType;

            SetWeenieHeaderFlag2();

            return weenieHeaderFlag;
        }

        private WeenieHeaderFlag2 SetWeenieHeaderFlag2()
        {
            WeenieHeaderFlag2 weenieHeaderFlag2 = WeenieHeaderFlag2.None;

            if ((IconUnderlayId != null) && (IconUnderlayId != 0))
                weenieHeaderFlag2 |= WeenieHeaderFlag2.IconUnderlay;

            if ((CooldownId != null) && (CooldownId != 0))
                weenieHeaderFlag2 |= WeenieHeaderFlag2.Cooldown;

            if ((CooldownDuration != null) && Math.Abs((float)CooldownDuration) >= 0.001)
                weenieHeaderFlag2 |= WeenieHeaderFlag2.CooldownDuration;

            if ((PetOwner != null) && (PetOwner != 0))
                weenieHeaderFlag2 |= WeenieHeaderFlag2.PetOwner;

            return weenieHeaderFlag2;
        }

        public virtual void SendPartialUpdates(Session targetSession, List<ObjectPropertyId> properties)
        {
            foreach (ObjectPropertyId property in properties)
            {
                switch (property.PropertyType)
                {
                    case ObjectPropertyType.PropertyInt:
                        int? value = this.DataObject.GetIntProperty((PropertyInt)property.PropertyId);
                        if (value != null)
                            targetSession.Network.EnqueueSend(
                                new GameMessagePublicUpdatePropertyInt(
                                    targetSession.Player.Sequences,
                                    (PropertyInt)property.PropertyId,
                                    value.Value));
                        break;
                    default:
                        log.Debug(
                            $"Unsupported property in SendPartialUpdates: id {property.PropertyId}, type {property.PropertyType}.");
                        break;
                }
            }
        }

        public virtual void SerializeCreateObject(BinaryWriter writer)
        {
            SerializeCreateObject(writer, false);
        }

        public virtual void SerializeGameDataOnly(BinaryWriter writer)
        {
            SerializeCreateObject(writer, true);
        }

        public virtual void SerializeCreateObject(BinaryWriter writer, bool gamedataonly)
        {
            if (!gamedataonly)
            {
                writer.WriteGuid(Guid);
                SerializeModelData(writer);
                SerializePhysicsData(writer);
            }
            writer.Write((uint)WeenieFlags);
            writer.WriteString16L(Name);
            writer.WritePackedDword(WeenieClassId);
            writer.WritePackedDwordOfKnownType(IconId ?? 0, 0x6000000);
            writer.Write((uint)ItemType);
            writer.Write((uint)DescriptionFlags);
            writer.Align();

            if ((DescriptionFlags & ObjectDescriptionFlag.IncludesSecondHeader) != 0)
                writer.Write((uint)WeenieFlags2);

            if ((WeenieFlags & WeenieHeaderFlag.PluralName) != 0)
                writer.WriteString16L(NamePlural);

            if ((WeenieFlags & WeenieHeaderFlag.ItemsCapacity) != 0)
                writer.Write(ItemCapacity ?? 0);

            if ((WeenieFlags & WeenieHeaderFlag.ContainersCapacity) != 0)
                writer.Write(ContainerCapacity ?? 0);

            if ((WeenieFlags & WeenieHeaderFlag.AmmoType) != 0)
                writer.Write((ushort?)AmmoType ?? 0);

            if ((WeenieFlags & WeenieHeaderFlag.Value) != 0)
                writer.Write(Value ?? 0);

            if ((WeenieFlags & WeenieHeaderFlag.Usable) != 0)
                writer.Write((uint?)Usable ?? 0u);

            if ((WeenieFlags & WeenieHeaderFlag.UseRadius) != 0)
                writer.Write(UseRadius ?? 0u);

            if ((WeenieFlags & WeenieHeaderFlag.TargetType) != 0)
                writer.Write(TargetType ?? 0);

            if ((WeenieFlags & WeenieHeaderFlag.UiEffects) != 0)
                writer.Write((uint?)UiEffects ?? 0u);

            if ((WeenieFlags & WeenieHeaderFlag.CombatUse) != 0)
                writer.Write((sbyte?)CombatUse ?? 0);

            if ((WeenieFlags & WeenieHeaderFlag.Structure) != 0)
                writer.Write(Structure ?? (ushort)0);

            if ((WeenieFlags & WeenieHeaderFlag.MaxStructure) != 0)
                writer.Write(MaxStructure ?? (ushort)0);

            if ((WeenieFlags & WeenieHeaderFlag.StackSize) != 0)
                writer.Write(StackSize ?? (ushort)0);

            if ((WeenieFlags & WeenieHeaderFlag.MaxStackSize) != 0)
                writer.Write(MaxStackSize ?? (ushort)0);

            if ((WeenieFlags & WeenieHeaderFlag.Container) != 0)
                writer.Write(ContainerId ?? 0u);

            if ((WeenieFlags & WeenieHeaderFlag.Wielder) != 0)
                writer.Write(WielderId ?? 0u);

            if ((WeenieFlags & WeenieHeaderFlag.ValidLocations) != 0)
                writer.Write((uint?)ValidLocations ?? 0u);

            if ((WeenieFlags & WeenieHeaderFlag.CurrentlyWieldedLocation) != 0)
                writer.Write((uint?)CurrentWieldedLocation ?? 0u);

            if ((WeenieFlags & WeenieHeaderFlag.Priority) != 0)
                writer.Write((uint?)Priority ?? 0u);

            if ((WeenieFlags & WeenieHeaderFlag.RadarBlipColor) != 0)
                writer.Write((byte?)RadarColor ?? 0);

            if ((WeenieFlags & WeenieHeaderFlag.RadarBehavior) != 0)
                writer.Write((byte?)RadarBehavior ?? 0);

            if ((WeenieFlags & WeenieHeaderFlag.PScript) != 0)
                writer.Write(Script ?? 0);

            if ((WeenieFlags & WeenieHeaderFlag.Workmanship) != 0)
                writer.Write(Workmanship ?? 0);

            if ((WeenieFlags & WeenieHeaderFlag.Burden) != 0)
                writer.Write(Burden ?? 0);

            if ((WeenieFlags & WeenieHeaderFlag.Spell) != 0)
                writer.Write((ushort?)Spell ?? 0);

            if ((WeenieFlags & WeenieHeaderFlag.HouseOwner) != 0)
                writer.Write(HouseOwner ?? 0u);

            if ((WeenieFlags & WeenieHeaderFlag.HouseRestrictions) != 0)
                writer.Write(HouseRestrictions ?? 0u);

            if ((WeenieFlags & WeenieHeaderFlag.HookItemTypes) != 0)
                writer.Write(HookItemType ?? 0);

            if ((WeenieFlags & WeenieHeaderFlag.Monarch) != 0)
                writer.Write(Monarch ?? 0u);

            if ((WeenieFlags & WeenieHeaderFlag.HookType) != 0)
                writer.Write(HookType ?? 0);

            if ((WeenieFlags & WeenieHeaderFlag.IconOverlay) != 0)
                writer.WritePackedDwordOfKnownType((IconOverlayId ?? 0), 0x6000000);

            if ((WeenieFlags2 & WeenieHeaderFlag2.IconUnderlay) != 0)
                writer.WritePackedDwordOfKnownType((IconUnderlayId ?? 0), 0x6000000);

            if ((WeenieFlags & WeenieHeaderFlag.MaterialType) != 0)
                writer.Write((uint)(MaterialType ?? 0u));

            if ((WeenieFlags2 & WeenieHeaderFlag2.Cooldown) != 0)
                writer.Write(CooldownId ?? 0);

            if ((WeenieFlags2 & WeenieHeaderFlag2.CooldownDuration) != 0)
                writer.Write((double?)CooldownDuration ?? 0u);

            if ((WeenieFlags2 & WeenieHeaderFlag2.PetOwner) != 0)
                writer.Write(PetOwner ?? 0u);

            writer.Align();
        }

        public void AddBaseModelData()
        {
            // Hair/head
            if (DataObject.HeadObjectDID != null)
                AddModel(0x10, (uint)DataObject.HeadObjectDID);

            if (DataObject.DefaultHairTextureDID != null && (DataObject.HairTextureDID != null))
                AddTexture(0x10, (uint)DataObject.DefaultHairTextureDID, (uint)DataObject.HairTextureDID);

            if (DataObject.HairPaletteDID != null)
                AddPalette((uint)DataObject.HairPaletteDID, 0x18, 0x8);

            // Skin
            PaletteBaseId = DataObject.PaletteBaseDID;

            if (DataObject.SkinPaletteDID != null)
                AddPalette((uint)DataObject.SkinPaletteDID, 0x0, 0x18);

            // Eyes
            if (DataObject.DefaultEyesTextureDID != null && DataObject.EyesTextureDID != null)
                AddTexture(0x10, (uint)DataObject.DefaultEyesTextureDID, (uint)DataObject.EyesTextureDID);

            if (DataObject.EyesPaletteDID != null)
                AddPalette((uint)DataObject.EyesPaletteDID, 0x20, 0x8);

            // Nose & Mouth
            if (DataObject.DefaultNoseTextureDID != null && DataObject.NoseTextureDID != null)
                AddTexture(0x10, (uint)DataObject.DefaultNoseTextureDID, (uint)DataObject.NoseTextureDID);

            if (DataObject.DefaultMouthTextureDID != null && DataObject.MouthTextureDID != null)
                AddTexture(0x10, (uint)DataObject.DefaultMouthTextureDID, (uint)DataObject.MouthTextureDID);
        }

        // Update the model, palette and texture info based on int.PaletteTemplate and double.Shade values from the corresponding ClothingBase values
        private void UpdateModelInfo()
        {
            if (ClothingBase == null)
            {
                AddBaseModelData();
                return;
            }

            ClothingTable item = ClothingTable.ReadFromDat((uint)ClothingBase);

            if (SetupTableId == null || !item.ClothingBaseEffects.ContainsKey((uint)SetupTableId))
                return;

            models.Clear(); // Clear existing models
            modelTextures.Clear(); // Clear existing textures

            // Add the model and texture(s)
            ClothingBaseEffect clothingBaseEffect = item.ClothingBaseEffects[(uint)SetupTableId];
            foreach (CloObjectEffect t in clothingBaseEffect.CloObjectEffects)
            {
                AddModel((byte)t.Index, (ushort)t.ModelId);
                foreach (CloTextureEffect t1 in t.CloTextureEffects)
                    AddTexture((byte)t.Index, (ushort)t1.OldTexture, (ushort)t1.NewTexture);
            }

            // We will use the PaletteTemplate / Shade system for palettes -- if there are any
            if (PaletteTemplate == null)
                return;

            if (!item.ClothingSubPalEffects.ContainsKey((int)PaletteTemplate))
                return;

            CloSubPalEffect itemSubPal = item.ClothingSubPalEffects[(int)PaletteTemplate];
            foreach (CloSubPalette t in itemSubPal.CloSubPalettes)
            {
                PaletteSet itemPalSet = PaletteSet.ReadFromDat(t.PaletteSet);

                double shadeTmp;
                if (Shade == null)
                    shadeTmp = 0; // TODO - Is this the correct default shade?
                else
                    shadeTmp = (double)Shade;

                ushort itemPal = (ushort)itemPalSet.GetPaletteID(shadeTmp);

                foreach (CloSubPalleteRange t1 in t.Ranges)
                {
                    uint palOffset = t1.Offset / 8;
                    uint numColors = t1.NumColors / 8;
                    AddPalette(itemPal, (ushort)palOffset, (ushort)numColors);
                }
            }
        }

        /// <summary>
        /// This is the function used for the GameMessage.ObjDescEvent
        /// </summary>
        /// <param name="writer">Passed from the GameMessageEvent</param>
        public virtual void SerializeUpdateModelData(BinaryWriter writer)
        {
            writer.WriteGuid(Guid);
            SerializeModelData(writer);
            writer.Write(Sequences.GetCurrentSequence(SequenceType.ObjectInstance));
            writer.Write(Sequences.GetNextSequence(SequenceType.ObjectPosition));
        }

        public void SerializeModelData(BinaryWriter writer)
        {
            writer.Write((byte)0x11);
            writer.Write((byte)modelPalettes.Count);
            writer.Write((byte)modelTextures.Count);
            writer.Write((byte)models.Count);

            if ((modelPalettes.Count > 0) && (PaletteBaseId != null))
                writer.WritePackedDwordOfKnownType((uint)PaletteBaseId, 0x4000000);
            foreach (ModelPalette palette in modelPalettes)
            {
                writer.WritePackedDwordOfKnownType(palette.PaletteId, 0x4000000);
                writer.Write((byte)palette.Offset);
                writer.Write((byte)palette.Length);
            }

            foreach (ModelTexture texture in modelTextures)
            {
                writer.Write((byte)texture.Index);
                writer.WritePackedDwordOfKnownType(texture.OldTexture, 0x5000000);
                writer.WritePackedDwordOfKnownType(texture.NewTexture, 0x5000000);
            }

            foreach (Model model in models)
            {
                writer.Write((byte)model.Index);
                writer.WritePackedDwordOfKnownType(model.ModelID, 0x1000000);
            }

            writer.Align();
        }

        public void WriteUpdatePositionPayload(BinaryWriter writer)
        {
            writer.WriteGuid(Guid);
            Location.Serialize(writer, PositionFlag, this.AnimationFrame ?? 0);
            writer.Write(Sequences.GetCurrentSequence(SequenceType.ObjectInstance));
            writer.Write(Sequences.GetNextSequence(SequenceType.ObjectPosition));
            writer.Write(Sequences.GetCurrentSequence(SequenceType.ObjectTeleport));
            writer.Write(Sequences.GetCurrentSequence(SequenceType.ObjectForcePosition));
        }

        /// <summary>
        /// Records some game-logic based desired position update (e.g. teleport), for use by physics engine
        /// </summary>
        /// <param name="newPosition"></param>
        protected void ForceUpdatePosition(Position newPosition)
        {
            ForcedLocation = newPosition;
        }

        /// <summary>
        /// Records where the client thinks we are, for use by physics engine later
        /// </summary>
        /// <param name="newPosition"></param>
        protected void PrepUpdatePosition(Position newPosition)
        {
            RequestedLocation = newPosition;
        }

        public void ClearRequestedPositions()
        {
            ForcedLocation = null;
            RequestedLocation = null;
        }

        /// <summary>
        /// Alerts clients of change in position
        /// </summary>
        protected virtual void SendUpdatePosition()
        {
            LastMovementBroadcastTicks = WorldManager.PortalYearTicks;
            GameMessage msg = new GameMessageUpdatePosition(this);
            if (CurrentLandblock != null)
            {
                CurrentLandblock.EnqueueBroadcast(Location, Landblock.MaxObjectRange, msg);
            }
        }

        /// <summary>
        /// Used by physics engine to actually update the entities position
        /// Automatically notifies clients of updated position
        /// </summary>
        /// <param name="newPosition"></param>
        public void PhysicsUpdatePosition(Position newPosition)
        {
            Location = newPosition;
            SendUpdatePosition();

            ForcedLocation = null;
            RequestedLocation = null;
        }

        /// <summary>
        /// Manages action/broadcast infrastructure
        /// </summary>
        /// <param name="parent"></param>
        public void SetParent(IActor parent)
        {
            CurrentParent = parent;
            actionQueue.RemoveParent();
            actionQueue.SetParent(parent);
        }

        /// <summary>
        /// Prepare new action to run on this object
        /// </summary>
        public LinkedListNode<IAction> EnqueueAction(IAction action)
        {
            return actionQueue.EnqueueAction(action);
        }

        /// <summary>
        /// Satisfies action interface
        /// </summary>
        /// <param name="node"></param>
        public void DequeueAction(LinkedListNode<IAction> node)
        {
            actionQueue.DequeueAction(node);
        }

        public DataObject NewDataObjectFromCopy()
        {
            return (DataObject)DataObject.Clone(GuidManager.NewItemGuid().Full);
        }

        public DataObject SnapShotOfDataObject(bool clearDirtyFlags = false)
        {
            DataObject snapshot = (DataObject)DataObject.Clone();
            if (clearDirtyFlags)
                DataObject.ClearDirtyFlags();
            return snapshot;
        }

        public void InitializeDataObjectForSave()
        {
            DataObject.SetDirtyFlags();
        }

        /// <summary>
        /// Runs all actions pending on this WorldObject
        /// </summary>
        public void RunActions()
        {
            actionQueue.RunActions();
        }

        private PhysicsDescriptionFlag SetPhysicsDescriptionFlag()
        {
            PhysicsDescriptionFlag physicsDescriptionFlag = PhysicsDescriptionFlag.None;

            byte[] movementData = CurrentMotionState?.GetPayload(Guid, Sequences);

            if (CurrentMotionState != null && movementData != null && movementData.Length > 0)
                physicsDescriptionFlag |= PhysicsDescriptionFlag.Movement;

            if (AnimationFrame != null)
                physicsDescriptionFlag |= PhysicsDescriptionFlag.AnimationFrame;

            if (Location != null)
                physicsDescriptionFlag |= PhysicsDescriptionFlag.Position;

            if (MotionTableId != 0)
                physicsDescriptionFlag |= PhysicsDescriptionFlag.MTable;

            if (SoundTableId != 0)
                physicsDescriptionFlag |= PhysicsDescriptionFlag.STable;

            if (PhysicsTableId != 0)
                physicsDescriptionFlag |= PhysicsDescriptionFlag.PeTable;

            if (SetupTableId != 0)
                physicsDescriptionFlag |= PhysicsDescriptionFlag.CSetup;

            if (Children.Count != 0)
                physicsDescriptionFlag |= PhysicsDescriptionFlag.Children;

            if (WielderId != null && ParentLocation != null)
                physicsDescriptionFlag |= PhysicsDescriptionFlag.Parent;

            if ((ObjScale != null) && (Math.Abs((float)ObjScale) >= 0.001))
                physicsDescriptionFlag |= PhysicsDescriptionFlag.ObjScale;

            if (Friction != null)
                physicsDescriptionFlag |= PhysicsDescriptionFlag.Friction;

            if (Elasticity != null)
                physicsDescriptionFlag |= PhysicsDescriptionFlag.Elasticity;

            if ((Translucency != null) && (Math.Abs((float)Translucency) >= 0.001))
                physicsDescriptionFlag |= PhysicsDescriptionFlag.Translucency;

            if (Velocity != null)
                physicsDescriptionFlag |= PhysicsDescriptionFlag.Velocity;

            if (Acceleration != null)
                physicsDescriptionFlag |= PhysicsDescriptionFlag.Acceleration;

            if (Omega != null)
                physicsDescriptionFlag |= PhysicsDescriptionFlag.Omega;

            if (DefaultScriptId != null)
                physicsDescriptionFlag |= PhysicsDescriptionFlag.DefaultScript;

            if (DefaultScriptIntensity != null)
                physicsDescriptionFlag |= PhysicsDescriptionFlag.DefaultScriptIntensity;

            return physicsDescriptionFlag;
        }

        // todo: return bytes of data for network write ? ?
        public void SerializePhysicsData(BinaryWriter writer)
        {
            writer.Write((uint)PhysicsDescriptionFlag);

            writer.Write((uint)PhysicsState);

            // PhysicsDescriptionFlag.Movement takes priorty over PhysicsDescription.FlagAnimationFrame
            // If both are set, only Movement is written.
            if ((PhysicsDescriptionFlag & PhysicsDescriptionFlag.Movement) != 0)
            {
                if (CurrentMotionState != null)
                {
                    byte[] movementData = CurrentMotionState.GetPayload(Guid, Sequences);
                    if (movementData.Length > 0)
                    {
                        writer.Write((uint)movementData.Length);

                            // May not need this cast from int to uint, but the protocol says uint Og II
                        writer.Write(movementData);
                        uint autonomous = CurrentMotionState.IsAutonomous ? (ushort)1 : (ushort)0;
                        writer.Write(autonomous);
                    }
                    else
                    {
                        // Adding these debug lines - don't think we can hit these, but want to make sure. Og II
                        log.Debug($"Our flag is set but we have no data length. {this.Guid.Full:X}");
                        writer.Write(0u);
                    }
                }
                else
                {
                    log.Debug($"Our flag is set but our current motion state is null. {this.Guid.Full:X}");
                    writer.Write(0u);
                }
            }
            else if ((PhysicsDescriptionFlag & PhysicsDescriptionFlag.AnimationFrame) != 0)
                writer.Write((AnimationFrame ?? 0));

            // TODO: Keep an eye on this, are we sure the client does not just ignore it?   I would think they way it reads by buffer length that this would blow up.
            // probably an edge case - just watch this - Og II

            if ((PhysicsDescriptionFlag & PhysicsDescriptionFlag.Position) != 0)
                Location.Serialize(writer);

            if ((PhysicsDescriptionFlag & PhysicsDescriptionFlag.MTable) != 0)
                writer.Write(MotionTableId ?? 0u);

            if ((PhysicsDescriptionFlag & PhysicsDescriptionFlag.STable) != 0)
                writer.Write(SoundTableId ?? 0u);

            if ((PhysicsDescriptionFlag & PhysicsDescriptionFlag.PeTable) != 0)
                writer.Write(PhysicsTableId ?? 0u);

            if ((PhysicsDescriptionFlag & PhysicsDescriptionFlag.CSetup) != 0)
                writer.Write(SetupTableId ?? 0u);

            if ((PhysicsDescriptionFlag & PhysicsDescriptionFlag.Parent) != 0)
            {
                writer.Write(WielderId ?? 0u);
                writer.Write(ParentLocation ?? 0);
            }

            if ((PhysicsDescriptionFlag & PhysicsDescriptionFlag.Children) != 0)
            {
                writer.Write(Children.Count);
                foreach (HeldItem child in Children)
                {
                    writer.Write(child.Guid);
                    writer.Write(child.LocationId);
                }
            }

            if ((PhysicsDescriptionFlag & PhysicsDescriptionFlag.ObjScale) != 0)
                writer.Write(ObjScale ?? 0u);

            if ((PhysicsDescriptionFlag & PhysicsDescriptionFlag.Friction) != 0)
                writer.Write(Friction ?? 0u);

            if ((PhysicsDescriptionFlag & PhysicsDescriptionFlag.Elasticity) != 0)
                writer.Write(Elasticity ?? 0u);

            if ((PhysicsDescriptionFlag & PhysicsDescriptionFlag.Translucency) != 0)
                writer.Write(Translucency ?? 0u);

            if ((PhysicsDescriptionFlag & PhysicsDescriptionFlag.Velocity) != 0)
            {
                Velocity.Serialize(writer);
            }

            if ((PhysicsDescriptionFlag & PhysicsDescriptionFlag.Acceleration) != 0)
            {
                Acceleration.Serialize(writer);
            }

            if ((PhysicsDescriptionFlag & PhysicsDescriptionFlag.Omega) != 0)
            {
                Omega.Serialize(writer);
            }

            if ((PhysicsDescriptionFlag & PhysicsDescriptionFlag.DefaultScript) != 0)
                writer.Write(DefaultScriptId ?? 0u);

            if ((PhysicsDescriptionFlag & PhysicsDescriptionFlag.DefaultScriptIntensity) != 0)
                writer.Write(DefaultScriptIntensity ?? 0u);

            // timestamps
            writer.Write(Sequences.GetCurrentSequence(SequenceType.ObjectPosition)); // 0
            writer.Write(Sequences.GetCurrentSequence(SequenceType.ObjectMovement)); // 1
            writer.Write(Sequences.GetCurrentSequence(SequenceType.ObjectState)); // 2
            writer.Write(Sequences.GetCurrentSequence(SequenceType.ObjectVector)); // 3
            writer.Write(Sequences.GetCurrentSequence(SequenceType.ObjectTeleport)); // 4
            writer.Write(Sequences.GetCurrentSequence(SequenceType.ObjectServerControl)); // 5
            writer.Write(Sequences.GetCurrentSequence(SequenceType.ObjectForcePosition)); // 6
            writer.Write(Sequences.GetCurrentSequence(SequenceType.ObjectVisualDesc)); // 7
            writer.Write(Sequences.GetCurrentSequence(SequenceType.ObjectInstance)); // 8

            writer.Align();
        }

        private void RecallAndSetObjectDescriptionBools()
        {
            // TODO: More uncommentting and wiring up for other flags
            ////None                   = 0x00000000,
            ////Openable               = 0x00000001,
            // if (DataObject.Openable ?? false)
            // Openable = true;
            ////Inscribable            = 0x00000002,
            if (DataObject.Inscribable ?? false)
                Inscribable = true;

            ////Stuck                  = 0x00000004,
            if (DataObject.Stuck ?? false)
                Stuck = true;

            ////Player                 = 0x00000008,
            // if (DataObject.Player ?? false)
            // Player = true;
            ////Attackable             = 0x00000010,
            if (DataObject.Attackable ?? false)
                Attackable = true;

            ////PlayerKiller           = 0x00000020,
            // if (DataObject.PlayerKiller ?? false)
            // PlayerKiller = true;
            ////HiddenAdmin            = 0x00000040,
            if (DataObject.HiddenAdmin ?? false)
                HiddenAdmin = true;

            ////UiHidden               = 0x00000080,
            if (DataObject.UiHidden ?? false)
                UiHidden = true;

            ////Book                   = 0x00000100,
            // if (DataObject.Book ?? false)
            // Book = true;
            ////Vendor                 = 0x00000200,
            // if (DataObject.Vendor ?? false)
            // Vendor = true;
            ////PkSwitch               = 0x00000400,
            // if (DataObject.PkSwitch ?? false)
            // PkSwitch = true;
            ////NpkSwitch              = 0x00000800,
            // if (DataObject.NpkSwitch ?? false)
            // NpkSwitch = true;
            ////Door                   = 0x00001000,
            // if (DataObject.Door ?? false)
            // Door = true;
            ////Corpse                 = 0x00002000,
            // if (DataObject.Corpse ?? false)
            // Corpse = true;
            ////LifeStone              = 0x00004000,
            // if (DataObject.LifeStone ?? false)
            // LifeStone = true;
            ////Food                   = 0x00008000,
            // if (DataObject.Food ?? false)
            // Food = true;
            ////Healer                 = 0x00010000,
            // if (DataObject.Healer ?? false)
            // Healer = true;
            ////Lockpick               = 0x00020000,
            // if (DataObject.Lockpick ?? false)
            // Lockpick = true;
            ////Portal                 = 0x00040000,
            // if (DataObject.Portal ?? false)
            // Portal = true;
            ////Admin                  = 0x00100000,
            // if (DataObject.Admin ?? false)
            // Admin = true;
            ////FreePkStatus           = 0x00200000,
            // if (DataObject.FreePkStatus ?? false)
            // FreePkStatus = true;
            ////ImmuneCellRestrictions = 0x00400000,
            if (DataObject.IgnoreHouseBarriers ?? false)
                ImmuneCellRestrictions = true;

            ////RequiresPackSlot       = 0x00800000,
            if (DataObject.RequiresBackpackSlot ?? false)
                RequiresPackSlot = true;

            ////Retained               = 0x01000000,
            if (DataObject.Retained ?? false)
                Retained = true;

            ////PkLiteStatus           = 0x02000000,
            // if (DataObject.PkLiteStatus ?? false)
            // PkLiteStatus = true;
            ////IncludesSecondHeader   = 0x04000000,
            // if (DataObject.IncludesSecondHeader ?? false)
            // IncludesSecondHeader = true;
            ////BindStone              = 0x08000000,
            // if (DataObject.BindStone ?? false)
            // BindStone = true;
            ////VolatileRare           = 0x10000000,
            // if (DataObject.VolatileRare ?? false)
            // VolatileRare = true;
            ////WieldOnUse             = 0x20000000,
            if (DataObject.WieldOnUse ?? false)
                WieldOnUse = true;

            ////WieldLeft              = 0x40000000,
            if (DataObject.AutowieldLeft ?? false)
                WieldLeft = true;
        }

        private void RecallAndSetPhysicsStateBools()
        {
            // TODO: More uncommentting and wiring up for other flags

            ////Static                      = 0x00000001,
            // if (DataObject.Static ?? false)
            // Static = true;
            ////Unused1                     = 0x00000002,
            ////Ethereal                    = 0x00000004,
            if (DataObject.Ethereal ?? false)
                Ethereal = true;

            ////ReportCollision             = 0x00000008,
            if (DataObject.ReportCollisions ?? false)
                ReportCollision = true;

            ////IgnoreCollision             = 0x00000010,
            if (DataObject.IgnoreCollisions ?? false)
                IgnoreCollision = true;

            ////NoDraw                      = 0x00000020,
            if (DataObject.NoDraw ?? false)
                NoDraw = true;

            ////Missile                     = 0x00000040,
            // if (DataObject.Missile ?? false)
            // Missile = true;
            ////Pushable                    = 0x00000080,
            // if (DataObject.Pushable ?? false)
            // Pushable = true;
            ////AlignPath                   = 0x00000100,
            // if (DataObject.AlignPath ?? false)
            // AlignPath = true;
            ////PathClipped                 = 0x00000200,
            // if (DataObject.PathClipped ?? false)
            // PathClipped = true;
            ////Gravity                     = 0x00000400,
            if (DataObject.GravityStatus ?? false)
                Gravity = true;

            ////LightingOn                  = 0x00000800,
            if (DataObject.LightsStatus ?? false)
                LightingOn = true;

            ////ParticleEmitter             = 0x00001000,
            // if (DataObject.ParticleEmitter ?? false)
            // ParticleEmitter = true;
            ////Unused2                     = 0x00002000,
            ////Hidden                      = 0x00004000,
            // if (DataObject.Hidden ?? false) // Probably PropertyBool.Visibility which would make me think if true, Hidden is false... Opposite of most other bools
            // Hidden = true;
            ////ScriptedCollision           = 0x00008000,
            if (DataObject.ScriptedCollision ?? false)
                ScriptedCollision = true;

            ////HasPhysicsBsp               = 0x00010000,
            // if (DataObject.HasPhysicsBsp ?? false)
            // HasPhysicsBsp = true;
            ////Inelastic                   = 0x00020000,
            if (DataObject.Inelastic ?? false)
                Inelastic = true;

            ////HasDefaultAnim              = 0x00040000,
            // if (DataObject.HasDefaultAnim ?? false)
            // HasDefaultAnim = true;
            ////HasDefaultScript            = 0x00080000,
            // if (DataObject.HasDefaultScript ?? false) // Probably based on PhysicsDescriptionFlag
            // HasDefaultScript = true;
            ////Cloaked                     = 0x00100000,
            // if (DataObject.Cloaked ?? false) // PropertyInt.CloakStatus probably plays in to this.
            // Cloaked = true;
            ////ReportCollisionAsEnviroment = 0x00200000,
            if (DataObject.ReportCollisionsAsEnvironment ?? false)
                ReportCollisionAsEnviroment = true;

            ////EdgeSlide                   = 0x00400000,
            if (DataObject.AllowEdgeSlide ?? false)
                EdgeSlide = true;

            ////Sledding                    = 0x00800000,
            // if (DataObject.Sledding ?? false)
            // Sledding = true;
            ////Frozen                      = 0x01000000,
            if (DataObject.IsFrozen ?? false)
                Frozen = true;
        }

        public virtual void ActOnUse(ObjectGuid playerId)
        {
            // Do Nothing by default
            if (CurrentLandblock != null)
            {
                Player player = CurrentLandblock.GetObject(playerId) as Player;
                if (player == null)
                {
                    return;
                }

#if DEBUG
                GameMessageSystemChat errorMessage =
                    new GameMessageSystemChat(
                        $"Default HandleActionOnUse reached, this object ({Name}) not programmed yet.",
                        ChatMessageType.System);
                player.Session.Network.EnqueueSend(errorMessage);
#endif

                GameEventUseDone sendUseDoneEvent = new GameEventUseDone(player.Session);
                player.Session.Network.EnqueueSend(sendUseDoneEvent);
            }
        }

        public virtual void OnUse(Session session)
        {
            // Do Nothing by default
#if DEBUG
            GameMessageSystemChat errorMessage =
                new GameMessageSystemChat(
                    $"Default OnUse reached, this object ({Name}) not programmed yet.",
                    ChatMessageType.System);
            session.Network.EnqueueSend(errorMessage);
#endif

            GameEventUseDone sendUseDoneEvent = new GameEventUseDone(session);
            session.Network.EnqueueSend(sendUseDoneEvent);
        }

        public virtual void HandleActionOnCollide(ObjectGuid playerId)
        {
            // todo: implement.  default is probably to do nothing.
        }
    }
}
