/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Entity;
using DerethForever.Entity.Enum;
using DerethForever.Managers;
using DerethForever.Network.GameEvent.Events;
using DerethForever.Network.GameMessages.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DerethForever.Actors
{
    public class Fellowship
    {
        public const int FellowMaxMembers = 9;
        public string FellowshipName;
        public uint FellowshipLeaderGuid;

        public bool ShareXP; // XP sharing: 0=no, 1=yes
        public bool EvenXpShare; // flag sent to clients if even split of xp
        public bool Open; // open fellowship: 0=no, 1=yes
        public bool ShareLoot; // is looing open to all

        public List<Player> Fellows = null;

        public Fellowship(Player leader, string fellowshipName, bool shareXPOption, bool shareLootOption)
        {
            Fellows = new List<Player>(FellowMaxMembers);
            ShareXP = shareXPOption;
            FellowshipLeaderGuid = leader.Guid.Full;
            FellowshipName = fellowshipName;
            ShareLoot = shareLootOption;
            EvenXpShare = false;
            Open = false;

            Fellows.Add(leader);
        }

        public void AddFellowshipMember(Player inviter, Player newMember)
        {
            if (Fellows.Count < FellowMaxMembers)
            {
                Fellows.Add(newMember);
                SendAddNewMemberEvents(newMember);
                newMember.Fellowship = inviter.Fellowship;
            }
            else
            {
                inviter.SendSystemMessage($"{newMember.Name} could not join as fellowship is full", ChatMessageType.Fellowship);
                newMember.SendSystemMessage($"Fellowship is full", ChatMessageType.System);
            }
            CalculateEvenSplit();
            SendFullUpdateToMembers();
        }

        /// <summary>
        /// Dismisses a player from the fellowship.  Only the leader can dismiss
        /// </summary>
        /// <param name="dismisser">Guid of the person doing the dismiss.  Used for security</param>
        /// <param name="dismissedPlayer">Player being dismissed from fellowship.</param>
        public void RemoveFellowshipMember(uint dismisser, Player dismissedPlayer)
        {
            if (dismisser == FellowshipLeaderGuid && Fellows.Contains(dismissedPlayer))
            {
                SendDismissedMessageToMembers(dismissedPlayer);
                Fellows.Remove(dismissedPlayer);
                dismissedPlayer.Fellowship = null;
                CalculateEvenSplit();
                SendFullUpdateToMembers();
            }
        }

        /// <summary>
        /// Sends the Message of fellowship quit to all for the player being dismissed and notifies remaining members
        /// of who was dismissed
        /// </summary>
        /// <param name="dismissedPlayer">Player being dismissed</param>
        private void SendDismissedMessageToMembers(Player dismissedPlayer)
        {
            foreach (Player fellow in Fellows)
            {
                fellow.Session.Network.EnqueueSend(new GameEventFellowshipDismiss(fellow.Session, dismissedPlayer));
                fellow.SendSystemMessage($"{dismissedPlayer.Name} was dismissed from the fellowship", ChatMessageType.Fellowship);
            };
        }

        /// <summary>
        /// Player quits the fellow.  if the leader selects disband, it handles disband for all members.
        /// If the leader logs or quits, a new leader is picked randomly.  Security settings so only leader
        /// can disband.
        /// </summary>
        /// <param name="quitter">Player leaving the fellowship</param>
        /// <param name="disband">Whether the leader decides to disband the fellow</param>
        public void QuitFellowship(Player quitter, bool disband)
        {
            if (disband && quitter.Guid.Full == FellowshipLeaderGuid)
            {
                DisbandFellowship();
                return;
            }
            else 
            {
                Fellows.Remove(quitter);
                quitter.Session.Network.EnqueueSend(new GameMessageFellowshipQuit(quitter.Session, quitter.Guid.Full));
                quitter.SendSystemMessage("You leave the fellowship", ChatMessageType.Fellowship);
                quitter.Fellowship = null;
            }

            if (quitter.Guid.Full == FellowshipLeaderGuid)
            {
                FindNewLeader();
            }

            SendQuitMessageToMembers(quitter);
            CalculateEvenSplit();
            SendFullUpdateToMembers();
        }

        private void FindNewLeader()
        {
            if (Fellows.Count > 0)
            {
                Random random = new Random();
                Player nextLeaderId = Fellows[random.Next(Fellows.Count)];
                AssignNewLeader(nextLeaderId);
            }
        }

        /// <summary>
        /// Notifies all remaining members when a player quits a fellowship
        /// </summary>
        /// <param name="quitter">Player quitting</param>
        private void SendQuitMessageToMembers(Player quitter)
        {
            foreach (Player fellow in Fellows)
            {
                fellow.Session.Network.EnqueueSend(new GameMessageFellowshipQuit(fellow.Session, quitter.Guid.Full));
                fellow.SendSystemMessage($"{quitter.Name} has left the fellowship", ChatMessageType.Fellowship);
            };
        }

        /// <summary>
        /// Sends quit messages to all members
        /// </summary>
        public void DisbandFellowship()
        {
            Player leader = Fellows.SingleOrDefault(s => s.Guid.Full == FellowshipLeaderGuid);
            foreach (Player fellow in Fellows)
            {
                fellow.Session.Network.EnqueueSend(new GameMessageFellowshipQuit(fellow.Session, fellow.Guid.Full));
                if (fellow.Guid.Full == FellowshipLeaderGuid)
                {
                    fellow.SendSystemMessage($"You disbanded the fellowship", ChatMessageType.Fellowship);
                }
                else
                {
                    fellow.SendSystemMessage($"{leader.Name} disbanded the fellowship", ChatMessageType.Fellowship);
                }
                fellow.Fellowship = null;
            };
        }

        /// <summary>
        /// Assigns a new leader for the fellow
        /// </summary>
        /// <param name="newLeader">Player chosen as new leader</param>
        public void AssignNewLeader(Player newLeader)
        {
            if (Fellows.Contains(newLeader))
            {
                foreach (Player fellow in Fellows)
                {
                    fellow.SendSystemMessage($"{newLeader.Name} now leads the fellowship", ChatMessageType.Fellowship);
                };
            }
        }

        /// <summary>
        /// Open fellowships allow any member to invite.  Dismiss/Disband/Openness still leader options only
        /// </summary>
        /// <param name="isOpen">Boolean from client message</param>
        public void UpdateOpenness(bool isOpen)
        {
            Open = isOpen;
            string openness = Open ? "open" : "closed";
            Parallel.ForEach(Fellows, (member) =>
            {
                member.SendSystemMessage($"Fellowship is now {openness}", ChatMessageType.Fellowship);
            });
        }

        /// <summary>
        /// Called when a player spends points in stats.
        /// TODO: Add call to this method in the Player class SpendAbilityXp method
        /// </summary>
        /// <param name="playerUpdate"></param>
        public void SendStatUpdate(Player playerUpdate)
        {
            foreach (Player fellow in Fellows)
            {
                fellow.Session.Network.EnqueueSend(
                    new GameEventFellowshipUpdateFellow(fellow.Session, playerUpdate, ShareLoot, 2u));
                fellow.Session.Network.EnqueueSend(
                    new GameEventFellowshipUpdateDone(fellow.Session));
            };
        }

        /// <summary>
        /// Called when a players stats change
        /// TODO: Add call to this method in the Player class UpdateVitalInternal method
        /// </summary>
        /// <param name="playerUpdate">Player whose vitals are updating</param>
        public void SendVitalUpdate(Player playerUpdate)
        {
            foreach (Player fellow in Fellows)
            {
                fellow.Session.Network.EnqueueSend(
                    new GameEventFellowshipUpdateFellow(fellow.Session, playerUpdate, ShareLoot, 3u));
                fellow.Session.Network.EnqueueSend(
                    new GameEventFellowshipUpdateDone(fellow.Session));
            };
        }

        /// <summary>
        /// Push full payload of all members and all current stats to all players
        /// </summary>
        private void SendFullUpdateToMembers()
        {
            foreach (Player fellow in Fellows)
            {
                fellow.Session.Network.EnqueueSend(new GameMessageFellowshipFullUpdate(fellow.Session));
            }
        }

        /// <summary>
        /// Sends message to all members when a new player joins fellowship
        /// </summary>
        /// <param name="newMember">Player being added</param>
        private void SendAddNewMemberEvents(Player newMember)
        {
            foreach (Player fellow in Fellows)
            {
                if (fellow.Guid.Full != newMember.Guid.Full)
                {
                    fellow.Session.Network.EnqueueSend(
                        new GameEventFellowshipUpdateFellow(fellow.Session, newMember, ShareLoot, 1u));
                    fellow.Session.Network.EnqueueSend(
                        new GameEventFellowshipUpdateDone(fellow.Session));
                }
            }
        }

        /// <summary>
        /// Does the calculation on how whether fellow is distributed or even xp share. 
        /// </summary>
        private void CalculateEvenSplit()
        {
            if (Fellows.Count == 0)
                return;

            int countAboveFifty = Fellows.Select(s => s.Level >= 50).Count();
            if (countAboveFifty == Fellows.Count)
            {
                EvenXpShare = true;
                return;
            }

            var leaderLevel = Fellows.SingleOrDefault(s => s.Guid.Full == FellowshipLeaderGuid).Level;
            int withinFiveOfLeader = Fellows.Select(s => Math.Abs(leaderLevel - s.Level) <= 5).Count();
            if (withinFiveOfLeader == Fellows.Count)
            {
                EvenXpShare = true;
                return;
            }
            EvenXpShare = false;
        }

        /// <summary>
        /// Bonus based upon number of members.  Not given for Fellows with members under 50 and
        /// not all players are within 5 levels of leader
        /// </summary>
        /// <returns></returns>
        private float GetBonus()
        {
            switch (Fellows.Count)
            {
                case 1:
                    return 1.0f;
                case 2:
                    return .75f;
                case 3:
                    return .6f;
                case 4:
                    return .55f;
                case 5:
                    return .5f;
                case 6:
                    return .45f;
                case 7:
                    return .4f;
                case 8:
                    return .35f;
                default:
                    return .3f;
            }
        }

        /// <summary>
        /// Determines distance from player to Leader.
        /// Formula:
        ///     Less than or equal to 2.5 map coords then 100%
        ///     Between 2.5 and 5 map coords the percent drops off evenly from 100% at 2.5 to 0 at 5.0
        /// </summary>
        /// <param name="earner"></param>
        /// <returns></returns>
        private float GetDistanceFromEarner(Player earner, Player fellow)
        {
            Position fellowPos = fellow.Location;
            Position earnerPos = earner.Location;

            if (Math.Abs(fellowPos.SquaredXYDistanceTo(earnerPos)) <= 600 * 600)
            {
                return 1;
            }
            else
            {
                return 1 - ((Math.Abs(fellowPos.SquaredXYDistanceTo(earnerPos)) - (600 * 600)) / (600 * 600));
            }
        }

        /// <summary>
        /// Checks to see if player is indoors.  If so, no xp is earned and that portion of xp is lost
        /// </summary>
        /// <param name="player">Player to get location for</param>
        /// <returns></returns>
        private bool IsPlayerInside(Player player)
        {
            if (player.Location.Indoors)
                return true;
            return false;
        }

        /// <summary>
        /// Calculates the exact amount of xp when earned by any player how it is distributed to fellowship.
        /// If a player is inside, 0 xp is earned.
        /// If player distance is within 2.5, 100% with lowered from there to 0% at 5.0
        /// If leader is inside, all suffer 50% loss
        /// </summary>
        /// <param name="xp">Amount of experience earned</param>
        /// <param name="playerId">Guid of player earning xp</param>
        /// <param name="bonus">Whether to use fellow bonus or not</param>
        public void DistributeXp(ulong xp, Player earnerPlayer, bool bonus)
        {
            LandblockId earnerPos = earnerPlayer.Location.LandblockId;

            foreach (Player fm in Fellows)
            {
                ulong finalXpAward = 0;
                // Vapor Golem - TODO: commenting out Indoors check for now.  Will revisit later
                // if (IsPlayerInside(fm) && fm.Location.LandblockId.Landblock != earnerPos.Landblock)
                //    finalXpAward = 0;

                if (bonus) // Use calculated bonus amount based upon size and distribution of fellowship
                {
                    if (EvenXpShare)
                    {
                        finalXpAward = (ulong)(xp * GetBonus());
                    }
                    else
                    {
                        // Calc proportional
                        double percentPerPlayerLevel = Fellows.Sum(s => s.Level) / Fellows.Count;
                        finalXpAward = (ulong)(fm.Level * percentPerPlayerLevel);
                    }
                }
                else // raw even split
                { 
                    finalXpAward = (ulong)(xp / (ulong)Fellows.Count);
                }
                fm.GrantXp((ulong)(finalXpAward * GetDistanceFromEarner(earnerPlayer, fm)), false, false);
            }
        }
    }
}
