﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;

using DerethForever.Managers;

using log4net;

namespace DerethForever.Actors.Actions
{
    public class DelayManager : IActor
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private SortedSet<DelayAction> delayHeap = new SortedSet<DelayAction>();

        public void RunActions()
        {
            // While the minimum time of our delayHeap is > our current time, kick off actions
            bool checkNeeded = true;
            while (checkNeeded)
            {
                checkNeeded = false;
                List<DelayAction> toAct = new List<DelayAction>();

                // Actions may be added to delayHeap from network queue -- therefore this is needed
                lock (delayHeap)
                {
                    while (delayHeap.Count > 0)
                    {
                        // Find the next (O(1))
                        var min = delayHeap.Min();

                        // If they wanted to run before or at now
                        if (min.EndTime <= WorldManager.PortalYearTicks)
                        {
                            toAct.Add(min);

                            // O(log(n))
                            delayHeap.Remove(min);
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                foreach (var action in toAct)
                {
                    Tuple<IActor, IAction> next = action.Act();

                    if (next != null)
                    {
                        next.Item1.EnqueueAction(next.Item2);
                    }
                }
            }
        }

        public LinkedListNode<IAction> EnqueueAction(IAction action)
        {
            DelayAction delayAction = action as DelayAction;

            if (delayAction == null)
            {
                log.Error("Non DelayAction IAction added to DelayManager");
                return null;
            }

            delayAction.Start();

            lock (delayHeap)
            {
                delayHeap.Add(delayAction);
            }

            return null;
        }

        public void DequeueAction(LinkedListNode<IAction> action)
        {
            log.Error("DelayManager Doesn't support DequeueAction");
        }
    }
}
