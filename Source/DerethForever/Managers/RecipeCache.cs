/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using log4net;

using System.Collections.Generic;

using DerethForever.Entity;

namespace DerethForever.Managers
{
    public class RecipeCache
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private Dictionary<uint, Dictionary<uint, Recipe>> _cache = new Dictionary<uint, Dictionary<uint, Recipe>>();

        public RecipeCache(List<Recipe> sourceData)
        {
            foreach (Recipe recipe in sourceData)
            {
                if (recipe.SourceWcid == null || recipe.TargetWcid == null)
                    continue;

                if (!_cache.ContainsKey(recipe.SourceWcid.Value))
                    _cache.Add(recipe.SourceWcid.Value, new Dictionary<uint, Recipe>());

                if (!_cache[recipe.SourceWcid.Value].ContainsKey(recipe.TargetWcid.Value))
                    _cache[recipe.SourceWcid.Value].Add(recipe.TargetWcid.Value, recipe);
                else
                    log.Debug("duplicate/unusable recipe detected: " + recipe.RecipeGuid);
            }
        }

        public Recipe GetRecipe(uint source, uint target)
        {
            if (_cache.ContainsKey(source) && (_cache[source].ContainsKey(target)))
                return _cache[source][target];
            else
                // try and get a generic recipe (e.g. a "source" item can be used on a variety of targets, like a Mana Stone)
                return GetRecipeGeneric(source);
        }

        /// <summary>
        /// The "Generic" recipe is for recipes that have a specific WCID used on a wide variety of items.
        /// Examples include dyeing, mana stones, and mana charges.
        /// </summary>
        public Recipe GetRecipeGeneric(uint source)
        {
            // a target of 0 is used for generic items
            if (_cache.ContainsKey(source) && (_cache[source].ContainsKey(0)))
                return _cache[source][0];
            else
                return null;
        }
    }
}
