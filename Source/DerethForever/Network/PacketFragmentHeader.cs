﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.IO;
using System.Runtime.InteropServices;

namespace DerethForever.Network
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class PacketFragmentHeader
    {
        public static uint HeaderSize { get; } = 16u;

        public uint Sequence { get; set; }
        public uint Id { get; set; }
        public ushort Count { get; set; }
        public ushort Size { get; set; }
        public ushort Index { get; set; }
        public ushort Group { get; set; }

        public PacketFragmentHeader() { }

        public PacketFragmentHeader(BinaryReader payload)
        {
            Sequence = payload.ReadUInt32();
            Id = payload.ReadUInt32();
            Count = payload.ReadUInt16();
            Size = payload.ReadUInt16();
            Index = payload.ReadUInt16();
            Group = payload.ReadUInt16();
        }

        public byte[] GetRaw()
        {
            var headerHandle = GCHandle.Alloc(this, GCHandleType.Pinned);

            try
            {
                byte[] bytes = new byte[Marshal.SizeOf(typeof(PacketFragmentHeader))];
                Marshal.Copy(headerHandle.AddrOfPinnedObject(), bytes, 0, bytes.Length);
                return bytes;
            }
            finally
            {
                headerHandle.Free();
            }
        }
    }
}
