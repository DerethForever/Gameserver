﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;

namespace DerethForever.Network
{
    [Flags]
    public enum PacketHeaderFlags : uint
    {
        None              = 0x00000000,
        Retransmission    = 0x00000001,
        EncryptedChecksum = 0x00000002, // can't be paired with 0x00000001, see FlowQueue::DequeueAck
        BlobFragments     = 0x00000004,
        ServerSwitch      = 0x00000100,
        Referral          = 0x00000800,
        RequestRetransmit = 0x00001000,
        RejectRetransmit  = 0x00002000,
        AckSequence       = 0x00004000,
        Disconnect        = 0x00008000,
        LoginRequest      = 0x00010000,
        WorldLoginRequest = 0x00020000,
        ConnectRequest    = 0x00040000,
        ConnectResponse   = 0x00080000,
        CICMDCommand      = 0x00400000,
        TimeSynch         = 0x01000000,
        EchoRequest       = 0x02000000,
        EchoResponse      = 0x04000000,
        Flow              = 0x08000000
    }
}
