﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System;
using System.Text;

using DerethForever.Entity.Enum;

namespace DerethForever.Network.GameMessages.Messages
{
    public class GameMessageTurbineChat : GameMessage
    {
        public GameMessageTurbineChat(TurbineChatType turbineChatType, uint channel, string senderName, string message, uint senderID)
            : base(GameMessageOpcode.TurbineChat, GameMessageGroup.LoginQueue)
        {
            if (turbineChatType == TurbineChatType.InboundMessage)
            {
                var firstSizePos = Writer.BaseStream.Position;
                Writer.Write(0u); // Bytes to follow
                Writer.Write((uint)turbineChatType);
                Writer.Write(1u);
                Writer.Write(1u);
                Writer.Write(0x000B00B5); // Unique ID? Both ID's always match. These numbers change between 0x000B0000 - 0x000B00FF I think.
                Writer.Write(1u);
                Writer.Write(0x000B00B5); // Unique ID? Both ID's always match These numbers change between 0x000B0000 - 0x000B00FF I think.
                Writer.Write(0u);
                var secondSizePos = Writer.BaseStream.Position;
                Writer.Write(0u); // Bytes to follow

                Writer.Write(channel);

                Writer.Write((byte)senderName.Length);
                Writer.Write(Encoding.Unicode.GetBytes(senderName));

                Writer.Write((byte)message.Length);
                Writer.Write(Encoding.Unicode.GetBytes(message));

                Writer.Write(0x0Cu);
                Writer.Write(senderID);
                Writer.Write(0u);
                Writer.Write(1u);

                Writer.WritePosition((uint)(Writer.BaseStream.Position - firstSizePos + 4), firstSizePos);
                Writer.WritePosition((uint)(Writer.BaseStream.Position - secondSizePos + 4), secondSizePos);
            }
            else
                Console.WriteLine($"Unhandled GameMessageTurbineChat TurbineChatType: 0x{(uint)turbineChatType:X4}");
        }
    }
}
