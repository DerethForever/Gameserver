/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Entity;

namespace DerethForever.Network.GameAction.Actions
{
    /// <summary>
    /// This method processes the Game Action (F7B1) Inventory_StackableMerge (0x0054) and calls
    /// the HandleActionStackableMerge method on the player object. Og II
    /// </summary>
    public static class GameActionStackableMerge
    {
        [GameAction(GameActionType.StackableMerge)]
        public static void Handle(ClientMessage message, Session session)
        {
            uint mergeFromId = message.Payload.ReadUInt32();
            uint mergeToId = message.Payload.ReadUInt32();
            int amount = message.Payload.ReadInt32();
            session.Player.HandleActionStackableMerge(session, new ObjectGuid(mergeFromId), new ObjectGuid(mergeToId), amount);
        }
    }
}
