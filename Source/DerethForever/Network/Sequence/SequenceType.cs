﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
namespace DerethForever.Network.Sequence
{
    public enum SequenceType
    {
        ObjectPosition                        = 0,
        ObjectMovement                        = 1,
        ObjectState                           = 2,
        ObjectVector                          = 3,
        ObjectTeleport                        = 4,
        ObjectServerControl                   = 5,
        ObjectForcePosition                   = 6,
        ObjectVisualDesc                      = 7,
        ObjectInstance                        = 8,
        PrivateUpdateAttribute                = 9,
        PrivateUpdateAttribute2ndLevel        = 10,
        PrivateUpdateSkill                    = 11,
        PrivateUpdatePropertyInt64            = 12,
        PrivateUpdatePropertyInt              = 13,
        PrivateUpdatePropertyString           = 14,
        PrivateUpdatePropertyBool             = 15,
        PrivateUpdatePropertyDouble           = 16,
        Motion                                = 17,
        PrivateUpdatePropertyDataID           = 18,
        PrivateUpdateAttribute2ndLevelHealth  = 19,
        PrivateUpdateAttribute2ndLevelStamina = 20,
        PrivateUpdateAttribute2ndLevelMana    = 21,

        PublicUpdatePropertyInt               = 22,
        PublicUpdatePropertyInt64             = 23,
        PublicUpdatePropertyBool              = 24,
        PublicUpdatePropertyDouble            = 25,
        PublicUpdatePropertyDataID            = 26,
        PublicUpdatePropertyInstanceId        = 27,
        PublicUpdatePropertyString            = 28,

        SetStackSize                          = 29
    }
}
