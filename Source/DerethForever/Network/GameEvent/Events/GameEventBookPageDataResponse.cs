/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Actors;
using DerethForever.Entity;

namespace DerethForever.Network.GameEvent.Events
{
    public class GameEventBookPageDataResponse : GameEventMessage
    {
        public GameEventBookPageDataResponse(Session session, uint bookID, PageData pageData)
            : base(GameEventType.BookPageDataResponse, GameMessageGroup.Group09, session)
        {
            Writer.Write(bookID);
            Writer.Write(pageData.PageIdx);
            Writer.Write(pageData.AuthorID);
            Writer.WriteString16L(pageData.AuthorName);
            // Check if player is admin and hide AuthorAccount if not. Potential security hole if we are sending out account usernames.
            if (session.Player.IsAdmin)
                Writer.WriteString16L(pageData.AuthorAccount);
            else
                Writer.WriteString16L("Password is cheese");
            Writer.Write(0xFFFF0002); // flags
            Writer.Write(1); // textIncluded - Will also be the case, even if we are sending an empty string.
            if (pageData.IgnoreAuthor == true)
                Writer.Write(1); // ignoreAuthor
            else
                Writer.Write(0); // ignoreAuthor
            Writer.WriteString16L(pageData.PageText);
        }
    }
}
