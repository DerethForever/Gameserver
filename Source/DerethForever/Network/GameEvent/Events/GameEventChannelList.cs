﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

using DerethForever.Entity.Enum;
using DerethForever.Managers;

namespace DerethForever.Network.GameEvent.Events
{
    public class GameEventChannelList : GameEventMessage
    {
        public GameEventChannelList(Session session, GroupChatType chatChannel) : base(GameEventType.ChannelList, GameMessageGroup.Group09, session)
        {
            // TODO: This should send back to the client a correct count followed by name strings of the channel requested.
            //      Obviously this would be based on who was subscribed to the channel at the time

            // Writer.Write(1u);
            // Writer.WriteString16L("+Sentinel Nostromo");

            // For now, since everyone is subscribed and unable to alter, let's just list every character connected.
            uint numClientsConnected = 0;
            List<string> playerNames = new List<string>();
            foreach (var client in WorldManager.GetAll())
            {
                numClientsConnected++;
                playerNames.Add(client.Player.Name);
            }

            Writer.Write(numClientsConnected);
            foreach (var name in playerNames.ToArray())
                Writer.WriteString16L(name);
        }
    }
}