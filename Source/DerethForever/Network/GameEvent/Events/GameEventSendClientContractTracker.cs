﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using DerethForever.Actors;

namespace DerethForever.Network.GameEvent.Events
{
    public class GameEventSendClientContractTracker : GameEventMessage
    {
        /// <summary>
        /// This message is used to both add and remove quests in your quest panel.   The first use case, the add is stright forward
        /// and is sent in response to an onUse of a contract from your inventory. F7B1 0036 - Inventory_UseEvent
        /// The second use case is the abandon quest.   This sends a F7B1 0316  Social_AbandonContract in this case you send back the contract id
        /// you got in the message from the client and pass back a 1 in the deleteContract parameter. Og II
        /// </summary>
        /// <param name="session">Our player session used for getting message recipient guid and the correct message sequence.</param>
        /// <param name="contractTracker">This class contains all of the information we need to send the client about the contract. </param>

        public GameEventSendClientContractTracker(Session session, ContractTracker contractTracker)
                : base(GameEventType.SendClientContractTracker, GameMessageGroup.Group09, session)
        {
            Writer.Write(contractTracker.Version);
            Writer.Write(contractTracker.ContractId);
            Writer.Write(contractTracker.Stage);
            Writer.Write(contractTracker.TimeWhenDone);
            Writer.Write(contractTracker.TimeWhenRepeats);
            Writer.Write(contractTracker.DeleteContract);
            Writer.Write(contractTracker.SetAsDisplayContract);
        }
    }
}
