﻿/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using System.Collections.Generic;

using DerethForever.Actors;

namespace DerethForever.Network.GameEvent.Events
{
    public class GameEventSendClientContractTrackerTable : GameEventMessage
    {
        /// <summary>
        /// This message is used to send the list quests in your quest panel.
        /// This is sent as part of the login sequence - if we have any tracked quests, we will send them on to the client.
        /// The second use case is the abandon quest.   This sends a F7B1 0316  Social_AbandonContract in this case you send back the contract id
        /// you got in the message from the client and pass back a 1 in the deleteContract parameter. Og II
        /// </summary>
        /// <param name="session">Our player session used for getting message recipient guid and the correct message sequence.</param>
        /// <param name="contactTracker">This is a list of the contact class containing all of the information we need to send the client about the contracts. </param>

        public GameEventSendClientContractTrackerTable(Session session, List<ContractTracker> contactTracker)
                : base(GameEventType.SendClientContractTrackerTable, GameMessageGroup.Group09, session)
        {
            const ushort tableSize = 32;
            Writer.Write((ushort)contactTracker.Count);
            Writer.Write(tableSize);
            foreach (ContractTracker contract in contactTracker)
            {
                Writer.Write(contract.ContractId);
                Writer.Write(contract.Version);
                Writer.Write(contract.ContractId);
                Writer.Write(contract.Stage);
                Writer.Write(contract.TimeWhenDone);
                Writer.Write(contract.TimeWhenRepeats);
            }
       }
    }
}
