/************************************************************************

    Dereth Forever - http://www.derethforever.com
    Copyright (C) 2018 Dereth Forever Contributors
    
    ACEmulator - Asheron's Call server emulator
    Copyright (C) 2018 ACEmulator Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

In addition to and in accordance with section 7c of the terms of 
the GNU General Public License, any modifications of this work must 
retain the text of this header, including all copyright authors, dates, 
and descriptions.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
using Newtonsoft.Json;
using System.ComponentModel;

namespace DerethForever.Common
{
    public class DatabaseConfiguration
    {
        /// <summary>
        /// Allows for automatic database redeployment upon starting the application and on prepared statement load failure, if enabled (set true).
        /// </summary>
        [DefaultValue(false)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public bool EnableAutoRedeployment { get; set; }

        /// <summary>
        /// Data Import Threshold when splitting data for importing into database. Higher number means more memory usage.
        /// </summary>
        [DefaultValue(1999)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public int DataImportSpeed { get; set; }

        public MySqlConfiguration Authentication { get; set; }

        public MySqlConfiguration Shard { get; set; }

        public FolderConfiguration World { get; set; }
    }
}
