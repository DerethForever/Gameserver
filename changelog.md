# Forever Dereth Change Log - April 2018

### 2018-04-23
**Coral Golem**

* Killed WeenieObject.cs - Behemoth - please review - I moved the references to this to IWorldObject - I think
* that is what we want.
* Fixed a number of missing items from Physics Object.   Documented.
* Reviewed prior work and fixed a few obvious bugs.
* Animation Sequence Node - Added Get Starting Frame
* Motion Interp - added Stop Motion, Enter Default State, add to queue and Leave Ground - TODO Leave Ground Velocity
* Part Array - Added Initialize Motion Tables
* Physics Object - Added Initialize Motion Tables - TODO Set Local Velocity
* Raw Motion State - Added Remove Motion
* Sequence added Remove Link Animations
* Motion Table Manager Added Initialize State, Remove Redundant Links and Truncate Animation List
* Frame - add Global To Local Vector
* Motion Interp - add Get Max Speed, Get Jump Velocity Z, Get State Velocity and Get Leave Ground Velocity
* ObjectCell - add Update All Voyeur
* Position - add Get Global To Local Vector
* Collision Info - added Set Collision Normal and Normalize Check Small
* LandDef added Get Block Offset
* Position Local To Global
* Sphere - add Slide Sphere
* Sphere Path - add Add Offset To Check Position and Cache Global Sphere
* Added Classes Target Info and Targetted Voyeur Info
* Added enum Target Status
* Physics Object - added methods Get Motion Interp, Clear Target and Remove Voyeur
* Movement Manager - added methods Enter Default State and Get Motion Interp
* Move To Manager - added methods Clean up and Stop Motion
* Target Manager - added properties and documented - added methods Clear Target and Remove Voyeur
* Added Cell Structure
* Updated Clip Plane
* Updated Environment Cell
* Finished Calculate Clip Planes


### 2018-04-06
**Iron Golem**

* Added ability to override select NPC features when randomly generating appearance.

### 2018-04-05
**Coral Golem**

* Added Lost Cell
* Added Object Maintenance
* Added Adjust Offset to Constraint Manager
* Added Adjust Offset to Position Manager
* BspNode - added Point Intersects Solid
* Sphere - renamed property to Center to match client, added methods Intersects and step sphere up
* Documented and stubbed in other methods.
* Archived March 2018 log and started April
* Animation - implement unpack
* Object Cell - implement remove cell
* Physics Object - implement set position internal, change cell, leave cell
* Added Test for Animation Unpack
* Added Position - Heading
* Added Vector3 Extension Normalize Check Small
* Added Physics Test to verify extension method

### 2018-04-04
**Iron Golem**

* Adjusted logic for Creatures with ClothingBase set (Ulgrim)
* Fixed a bug when dropping equipped items

### 2018-04-02
**Behemoth**

* scenery now loads properly

### 2018-04-02
**Behemoth**

* refactored new physics viewer camera control to mimic player control
* added ability for physics viewer camera to track/follow a single object
